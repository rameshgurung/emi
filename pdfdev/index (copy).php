<?php require_once("dompdf_config.inc.php"); ?>
<script type="text/php">
          $font = Font_Metrics::get_font("helvetica", "bold");
          $pdf->page_text(72, 18, "Header: {PAGE_NUM} of {PAGE_COUNT}", $font, 6, array(0,0,0));
</script>
<?php
$name="ramesh";
$user=array('name'=>'username','email'=>'email','status'=>'status','type'=>'type');
$html =<<<FOOBAR
			<table  class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>S.no</th>
						<th>Username</th>
						<th>Email</th>
						<th>Status</th>
						<th>Type</th>
						<th>Last Login</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>{$name}</td>
						<td>Email1</td>
						<td>Status1</td>
						<td>Type1</td>
						<td>Last Login1</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Username2</td>
						<td>Email2</td>
						<td>Status2</td>
						<td>Type2</td>
						<td>Last Login2</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Username3</td>
						<td>Email3</td>
						<td>Status3</td>
						<td>Type3</td>
						<td>Last Login3</td>
					</tr>
				</tbody>
			</table>
FOOBAR;
 
$dompdf = new DOMPDF(); 
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream("sample.pdf");
?>