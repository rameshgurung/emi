<?php

namespace Transborder\REST;

interface Response
{
	public function toArray();
}