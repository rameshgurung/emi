<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class MtcnNotFoundErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_MTCN_NOT_FOUND);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}