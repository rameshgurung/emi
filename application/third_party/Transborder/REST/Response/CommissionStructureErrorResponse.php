<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class CommissionStructureErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_COMMISSION_SLAB_NOT_FOUND);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}