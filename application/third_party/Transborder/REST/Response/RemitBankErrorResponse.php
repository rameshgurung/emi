<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class RemitBankErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_REMIT_BANK_NOT_FOUND);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}