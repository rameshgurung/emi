<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class InvalidPaymentTypeErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_INVALID_PAYMENT_TYPE);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}