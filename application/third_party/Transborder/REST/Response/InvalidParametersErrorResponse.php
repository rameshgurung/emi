<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class InvalidParametersErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_INVALID_PARAMETERS);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}