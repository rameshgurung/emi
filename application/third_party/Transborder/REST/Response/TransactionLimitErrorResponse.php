<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class TransactionLimitErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_TXN_LIMIT_EXCEED);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}