<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class PayoutErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_PAYOUT);
		$this->setHttpResponseCode(HTTP_INTERNAL_SERVER_ERROR);
	}
}