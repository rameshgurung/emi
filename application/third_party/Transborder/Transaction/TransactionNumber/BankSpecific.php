<?php
namespace Transborder\Transaction\TransactionNumber;

use Transborder\Transaction\TransactionNumber\StrategyBase;

class BankSpecific implements StrategyBase {
	
	public function generateTrackingNumber(){
	
		$now    = \Options::get('last_seen_tracking_number', str_pad('0', 12, "0", STR_PAD_LEFT));
		$next   = str_pad($now+1, 12, "0", STR_PAD_LEFT);
		\Options::update('last_seen_tracking_number', $next);
		return $next;
	
	}
	
	public function generateControlNumber(){
	
		$generatedNumber = date('y').random_string('numeric', \Options::get('txn_mtcn_len', '10'));
	
		$exist = \CI::$APP->doctrine->em->getRepository('models\Transaction')
										->findBy( array('control_number' => md5($generatedNumber)) );
	
		if($exist) return $this->generateControlNumber();
		else return $generatedNumber;
	
	}
	
}