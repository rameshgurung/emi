<?php
namespace Transborder\Transaction\Remit\CTBS;

use models\Transaction;

use models\Transaction\WalletPayment;

use models\Transaction\BankPayment;

use Transborder\Transconnect\TransconnectConstants;

use Transborder\Transconnect\Transconnect;

use Transborder\Transaction\Remit;

class RemitAdapter implements Remit
{
	
	/**
	 * (non-PHPdoc)
	 * 
	 * 
	 * @see Transborder\Transaction.Remit::createTransaction()
	 */
	public function generateControlNumber(&$transaction){
	
		$transconnect = new Transconnect();
		
		$remitter = $transaction->getRemitter();
		
		$sourceCountry = $transaction->getRemittingAgent()->getCountry();
		
		$beneficiary = $transaction->getBeneficiary();
		
// 		$txnDetails = array(	'remitter_name'			=>	$remitter->getFirstName().' '.$remitter->getMiddleName().' '.$remitter->getLastName(),
// 								'remitter_phone'		=>	$remitter->getMobile(),
// 								'remitter_address'		=>	$remitter->getStreet(),
// 								'beneficiary_name'		=>	$beneficiary->getName(),
// 								'tracking_number'		=>	$transaction->getTrackingNumber(),
// 								'amount'				=>	$transaction->getTargetAmount(),
// 								'source_country'		=>	$sourceCountry->getIso_3(),
// 								'source_currency'		=> 	$sourceCountry->getCurrency()->getIsoCode(),
// 								'source_amount'			=>	$transaction->getRemittingAmount(),
// 								'money_source'			=> 	$transaction->getSource(),
// 								'currency'				=>	$transaction->getReceivingCurrency()->getIsoCode(),
// 								'type'					=>	Transaction::TYPE_ID_PAYMENT
// 		);

		$txnDetails['beneficiaryName'] = $beneficiary->getName();
		$txnDetails['beneficiaryAddress'] = $beneficiary->getAddressString();
		$txnDetails['beneficiaryContactDetail'] = '1231231230';
		$txnDetails['originatingAmount'] = $transaction->getRemittingAmount();
		$txnDetails['originatingCurrency'] = $sourceCountry->getCurrency()->getIsoCode();
		$txnDetails['originatingCountry'] = $sourceCountry->getIso_3();
		$txnDetails['receivingAmount']	=	$transaction->getTargetAmount();
		$txnDetails['receivingCurrency'] = $transaction->getReceivingCurrency()->getIsoCode();
		$txnDetails['senderAgentCode'] = '';
		$txnDetails['senderName'] = $remitter->getName();
		$txnDetails['senderContactDetail'] = $beneficiary->getMobile();
		$txnDetails['senderAddress'] = $remitter->getStreet();
		$txnDetails['type'] = Transaction::TYPE_ID_PAYMENT;
		$txnDetails['tracking_number'] = $transaction->getTrackingNumber();
		$txnDetails['agentUserId'] = 'JANATACTBS';
		
		//to be include in extra fields
		$txnDetails['issuerDocumentId'] = 'testdocumentid';
		$txnDetails['relation'] = 'testrelation';
		$txnDetails['sourceOfFund'] = 'testsourceoffund';
		
		
		if($remitter->getDocuments())
		{
			$senderID = $transaction->getRemitter()->getDocuments();
			$txnDetails['sender_id_type'] = $senderID{0}->getDocType()->getName();
			$txnDetails['sender_id_number'] = $senderID{0}->getDocNumber();
			$txnDetails['sender_id_issued_country'] = $senderID{0}->getIssuedCountry()->getIso_3();
			$txnDetails['sender_id_valid_until'] = "NR";
		}
		
		
		if($transaction instanceof BankPayment){
			$txnDetails['type'] = Transaction::TYPE_BANK_PAYMENT;
			$txnDetails['account_number'] = $transaction->getAccountNumber();
			$txnDetails['bank_bic'] = $transaction->getBankBranch()->getBank()->getSwiftCode();
		}elseif ($transaction instanceof WalletPayment){
			$txnDetails['type'] = Transaction::TYPE_WALLET_PAYMENT;
			$txnDetails['wallet_id'] = $transaction->getWalletAddress();
			$txnDetails['wallet_type'] = $transaction->getWalletType();
		}
		
		$response = $transconnect->getServiceInfoId($txnDetails);	
		
		log_message('info', 'Transconnect::RemitAdapter::CTBS::generateControlNumber:: '.json_encode($response));
		
		if($response->status == 'SUCCESS')
		{
						
			$serviceId = $response->ctbsTransactionId;
				
// 			$responseFields = $transconnect->getResponseFields();
			
			$transaction->setCtbsTxnID($serviceId);
			
			
			
// 			if(!$responseFields){
// 				return $transaction->getTrackingNumber();
// 			}
			
// 			if(array_key_exists(TransconnectConstants::MTCN, $responseFields))
// 				return $responseFields[TransconnectConstants::MTCN];
// 			else
// 				return $transaction->getTrackingNumber();
			if($response->mtcnDetail->mtcn){				
// 				die("found mtcn");
				return $response->mtcnDetail->mtcn;
			}
			else{
				return $transaction->getTrackingNumber();
			}
// 			die("Not Found");
			
		}
		else
		{
			throw new \Exception($transconnect->getResponseCode().' :: '.$transconnect->getResponseMessage());
		}
						
	}	
}