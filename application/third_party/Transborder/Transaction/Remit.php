<?php
namespace Transborder\Transaction;

interface Remit{
	
	/**
	 * 
	 * @param models\Transaction $transaction
	 */
	public function generateControlNumber(&$transaction);
	
}