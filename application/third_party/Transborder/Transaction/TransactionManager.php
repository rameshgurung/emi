<?php

namespace Transborder\Transaction;

use Transborder\Transconnect\Transconnect;

use models\Transaction\WalletPayment;

use models\CustomerIDocuments;

use Transborder\Common\Util\CommissionCalculator;

use models\Ledger;

use models\Transaction;

use models\LedgerPosting;

use models\Transaction\TransactionNote;

use Transborder\Common\Util\Calculator;

class TransactionManager{

	private $ci;
	
	private $pr_pool_account;
	
	private $commission_pool_account;

	public function __construct(){
		$this->ci = &\CI::$APP;
		$this->pr_pool_account = $this->ci->doctrine->em->find('models\Ledger', \Options::get('config_pool_account'));
		$this->commission_pool_account = $this->ci->doctrine->em->find('models\Ledger', \Options::get('config_commission_pool_account'));
	}

	public function approveTransaction(\models\Transaction $transaction, $note = NULL){
		
		log_message('info', 'UPADATE TRANSACTION TO APPROVED');
		
		$service_charge = $transaction->getNetComission();
				
		if(is_null($service_charge))
		{
			$this->ci->message->set('Commission not set for the remitter', 'alert', TRUE, 'critical');
			redirect('transaction/viewDetail/'.$transaction->getTrackingNumber());
		}
		
		$txnStatus = $transaction->getStatus();		
		
		$nextStatus = Transaction::STATUS_APPROVED;	
		
		$ctbsUpdateStatus = FALSE;
		
		$walletStatus = FALSE;
		
		$responseMessage = "";
		
		$date = new \DateTime();
		
		$currentUser = \Current_User::user();
		
		$currentAgent = $currentUser->getAgent();		
		
		
// 		\Events::trigger('change_status', array('transaction'=>$transaction, 'old_status'=>$txnStatus, 'new_status'=>$nextStatus));
		
		if(!is_null($transaction->getCtbsTxnID()) &&  \Options::get('use_ctbs', 0) == 1 && $txnStatus == Transaction::STATUS_PENDING)
		{
			
			
			
			$txnAdapter = new Transconnect();
			
			
			$ctbsUpdateStatus = $txnAdapter->createTransaction($transaction, $txnStatus, $nextStatus);
			
		
			
			$ctbsTxnDetails = $txnAdapter->getTransactionDetails();
			
			$responseMessage = $txnAdapter->getResponseMessage();		
						
			$ctbsTxnStatus = $ctbsTxnDetails->status; 
		
			
			log_message('info', 'CTBS Transaction Details Status :: '.$ctbsTxnStatus);
			
			log_message('info', 'CTBS Update status :: '.$ctbsUpdateStatus);
			
			if(!$ctbsUpdateStatus) return FALSE;
				
			
			if($transaction instanceof WalletPayment)
			{
			  
			
				
				if($ctbsTxnStatus == "COMPLETE")
				{
//					echo "COMPLETE"; exit;
					$nextStatus = Transaction::STATUS_PAID;
					$walletStatus = TRUE;
					$transaction->setPayoutAgent($currentAgent);
					$transaction->setPaidDate($date);
					$transaction->setUser($currentUser);
				}	
				elseif($ctbsTxnStatus == "PENDING")
				{
					
					$nextStatus = Transaction::STATUS_APPROVED;
					$walletStatus = TRUE;
				}	
				else 
				{
					
					$nextStatus = Transaction::STATUS_PENDING;
					$this->ci->message->set($responseMessage, 'error', TRUE, 'feedback');
					return FALSE;
				}		
			}
			
		}
		
		
		
		$remitting_agent = $transaction->getUser()->getAgent();
		
		$ra_c_account = $remitting_agent->getComAccount();
		
		$commission = new CommissionCalculator();
		
		$total_amount_in_tb = $commission->getAmountInTbCurrency($remitting_agent, $transaction->getRemittingAmount())->amount;
		
		$net_commission_in_tb = $commission->getAmountInTbCurrency($remitting_agent, $transaction->getNetCommission())->amount;
		
		$this->ledgerPosting($transaction, $this->pr_pool_account, $remitting_agent->getName(), $total_amount_in_tb, 'CREDIT', 'AMOUNT');
		
		$this->ledgerPosting($transaction, $this->commission_pool_account, $remitting_agent->getName(), $net_commission_in_tb, "CREDIT", "COMMISSION");
		
		$this->ledgerPosting($transaction, $remitting_agent->getAccount(), 'Payable/Receivable Pool Account', $transaction->getRemittingAmount(), "DEBIT", "AMOUNT");
		
		$this->ledgerPosting($transaction, $remitting_agent->getComAccount(), 'Commission Pool Account', $transaction->getNetComission(), "DEBIT", "COMMISSION");
		
		$this->createTransactionNote($transaction, $note.' (Transaction Approved)', $nextStatus);

		$transaction->setStatus($nextStatus);
		
		$this->ci->doctrine->em->persist($transaction);
		
		$this->ci->doctrine->em->flush();
		
		if($transaction->id()){
			
			if($transaction instanceof WalletPayment)
			{
				$this->ci->message->set($responseMessage, 'success', TRUE, 'feedback');
				
				if(!$walletStatus) 
				{
					return FALSE;
				}
				 
				log_message('info','Wallet Payment Response Message :: '.$responseMessage); 
			}
			else { log_message('info','Transaction approved by '.\Current_User::user()->getUsername().' for Tracking number - '.$transaction->getTrackingNumber()); }
			
			return TRUE;
		}
		else return FALSE;
	}
	
	
	
	
	
	public function createTransactionNote($transaction, $note, $nextStatus)
	{
	
		$transaction_note = new TransactionNote();
		$transaction_note->setNote($note);
		$transaction_note->setStatus($nextStatus);
		$transaction_note->setTransaction($transaction);
		$transaction_note->setUser(\Current_User::user());
		$this->ci->doctrine->em->persist($transaction_note);
	}
	
	public function ledgerPosting($transaction, $account_number, $to_from, $amount, $debit_credit = "DEBIT", $amount_commission = "AMOUNT") 
	{
		$amtDesc = (strtoupper($amount_commission) == "AMOUNT")? 'amount' : 'commission';
		
		if(strtoupper($debit_credit) == "DEBIT")
		{
			$drcrDesc = 'paid to';
			$d_c = LedgerPosting::LEDGER_POSTING_DEBIT;
		}
		else 
		{
			$drcrDesc = 'received from';
			$d_c = LedgerPosting::LEDGER_POSTING_CREDIT;
		}
		
		$ledger = new LedgerPosting();
		$ledger->setAccount($account_number);
		$ledger->setAmount($amount);
		$ledger->setDebitCredit($d_c);
		$ledger->setParticular('Being '.$amtDesc.' '.$drcrDesc.' '.$to_from);
		$ledger->setTransaction($transaction);
		
		$this->ci->doctrine->em->persist($ledger);
	}

	public function pay(\models\Transaction $transaction, $note = NULL, $beneficiaryDocuments = NULL){
		
		$data = unserialize($this->ci->session->userdata('pay_data'));
		
		$old_status = $transaction->getStatus();
		
		if($transaction->getRemittanceType() == Transaction::TYPE_ID_PAYMENT){
			$payout_agent_id = (isset($data['sub_agent']))? $data['sub_agent'] : '';
			$payoutAgent = ($payout_agent_id != '')? $this->ci->doctrine->em->find('models\Agent',$payout_agent_id) : \Current_User::user()->getAgent() ;
			
			if(is_array($beneficiaryDocuments) && array_key_exists("id_type",$beneficiaryDocuments)){
				
				$beneficiary=$transaction->getBeneficiary();
				$id_type = $this->ci->doctrine->em->find('models\Common\IdentificationDocument',$beneficiaryDocuments['id_type']);
								
				$bid = new CustomerIDocuments();
				$bid->setCustomer($beneficiary);
				$bid->setDocNumber(trim($beneficiaryDocuments['doc_no']));
				$bid->setDocType($id_type);
				$bid->setIssuedDate(new \DateTime($beneficiaryDocuments['issued_date']));
				
				$bid->setIssuedCountry($this->ci->doctrine->em->find('models\Common\Country',$beneficiaryDocuments['issued_country']));
				
				if (isset($beneficiaryDocuments['issued_date']) && $beneficiaryDocuments['issued_date']!=''){
						$bid->setIssuedDate(new \DateTime($beneficiaryDocuments['issued_date']));
				}
				
				if (isset($beneficiaryDocuments['expired_date']) && $beneficiaryDocuments['issued_date']!=''){
						$bid->setExpiryDate(new \DateTime($beneficiaryDocuments['expired_date']));
				}
				
				if (isset($beneficiaryDocuments['issued_state']) && $beneficiaryDocuments['issued_state']!=''){
					$bid->setIssuedState($this->ci->doctrine->em->find('models\Common\State',$beneficiaryDocuments['issued_state']));
				}
				
				if (isset($beneficiaryDocuments['issued_city']) && $beneficiaryDocuments['issued_city']!=''){
					$bid->setIssuedCity($this->ci->doctrine->em->find('models\Common\City',$beneficiaryDocuments['issued_city']));
					
				}
				
				$this->ci->doctrine->em->persist($bid);
				$transaction->setBeneficiaryDocument($bid);
				$this->ci->doctrine->em->persist($beneficiary);
			}
		}
		else{
			$payoutAgent = \Current_User::user()->getAgent();
			
		}
		
		$remitting_currency = $transaction->getRemittingAgent()->getCountry()->getCurrency();
		$receiving_currency = $payoutAgent->getCountry()->getCurrency();
		$amt_in_words = $receiving_currency->getIsoCode().' '.$transaction->getAmountInWord();
		
		if(is_null($transaction->getReceivingCurrency())){
			$transaction->setReceivingCurrency($receiving_currency);
			$transaction->setAmountInWord($amt_in_words);		
		}
		
		if(is_null($transaction->getRemittingCurrency()))
			$transaction->setRemittingCurrency($remitting_currency);
		
		$transaction->setStatus(Transaction::STATUS_PAID);
		$transaction->setPaidDate(new \DateTime());
		$transaction->setPayoutAgent($payoutAgent);
		$transaction->setDestinationCountry($payoutAgent->getCountry());
		$transaction->setAmlStatus($this->ci->session->userdata('aml_status'));
		$this->ci->doctrine->em->persist($transaction);
		
		$this->createTransactionNote($transaction, $note.' (Transaction Paid)', Transaction::STATUS_PAID);
		
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			\Events::trigger('change_status', array('transaction'=>$transaction,'old_status'=>$old_status,'new_status'=>Transaction::STATUS_PAID));
			log_message('info','Transaction paid by '.\Current_User::user()->getUsername().' for tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function unblock(\models\Transaction $transaction,$note = NULL){
		
		$this->createTransactionNote($transaction, $note.' (Transaction Unblocked)', 'unblocked');
		
		$transaction->setStatus(Transaction::STATUS_APPROVED);
		$this->ci->doctrine->em->persist($transaction);
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			log_message('info','Transaction unblocked by '.\Current_User::user()->getUsername().' for Tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else return FALSE;

	}

	public function block(\models\Transaction $transaction, $note = NULL, $auto = FALSE){

		$this->createTransactionNote($transaction, $note.' (Transaction Blocked)', Transaction::STATUS_BLOCKED);
		
		$transaction->setStatus(Transaction::STATUS_BLOCKED);
		$this->ci->doctrine->em->persist($transaction);
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			$logmsg = ($auto) 
						? 'Transaction with Tracking number - ' .$transaction->getTrackingNumber(). ' unpaid for '.\Options::get('unpaid_txn_block_after', 0).' days is automatically blocked' 
						: 'Transaction with Tracking number - ' .$transaction->getTrackingNumber(). ' is blocked by '.\Current_User::user()->getUsername();
			log_message('info', $logmsg);
			return TRUE;
		}
		else return FALSE;

	}
	
	public function  settle(\models\Transaction $transaction, $note = NULL)
	{
		$payout_agent = $transaction->getPayoutAgent();
		
		$payout_pr_account = $payout_agent->getAccount();
		
		$payout_commission_account = $payout_agent->getComAccount();
		
		$calculateCommission = new CommissionCalculator();
		
		$pa_commission = $calculateCommission->calculatePayoutCommission($transaction);
		
		$total_amount_in_tb = $calculateCommission->getAmountInTbCurrency($transaction->getRemittingAgent(), $transaction->getRemittingAmount())->amount;
		
		$target_amount_in_tb = $calculateCommission->getAmountInTbCurrency($payout_agent, $transaction->getTargetAmount())->amount;

		$this->ledgerPosting($transaction, $this->pr_pool_account, $payout_agent->getName(), $total_amount_in_tb, "DEBIT", "AMOUNT");
		
		$this->ledgerPosting($transaction, $payout_pr_account, 'Payable/Receivable Pool Account', $target_amount_in_tb, "CREDIT", "AMOUNT");
		
		$this->createTransactionNote($transaction, $note.' (Transaction Settled)', Transaction::STATUS_SETTLED); 
		
		$transaction->setStatus(Transaction::STATUS_SETTLED);
		
		$this->ci->doctrine->em->persist($transaction);
		
		$this->ci->doctrine->em->flush();
		
		if($transaction->id()){
			log_message('info','Transaction settled by '.\Current_User::user()->getUsername().' for tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else{
			return FALSE;
		}	
		
	}
	
	public function  revert(\models\Transaction $transaction, $note = NULL){
		
		$this->createTransactionNote($transaction, $note.' (Transaction Reverted)', 'reverted');
		
		$transaction->setStatus(Transaction::STATUS_APPROVED);
		
		$transaction->setIsLocked(FALSE);
		
		$transaction->setPaidDate(NULL);
		
		$transaction->setPayoutAgent(NULL);
		
		$this->ci->doctrine->em->persist($transaction);
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			log_message('info','Transaction reverted by '.\Current_User::user()->getUsername().' for Tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else return FALSE;
		}
		
	public function cancel(\models\Transaction $transaction, $note = NULL){
		
		$calculator = new CommissionCalculator();
		
		$total_amount_in_tb = $calculator->getAmountInTbCurrency($transaction->getRemittingAgent(), $transaction->getRemittingAmount())->amount;
		
		$this->ledgerPosting($transaction, $this->pr_pool_account, $transaction->getRemittingAgent()->getName(), $total_amount_in_tb, "DEBIT", "AMOUNT");
				
		$this->createTransactionNote($transaction, $note.' (Transaction Cancelled)', Transaction::STATUS_CANCELLED);
		
		$transaction->setStatus(Transaction::STATUS_CANCELLED);
		$this->ci->doctrine->em->persist($transaction);
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			log_message('info','Transaction cancelled by '.\Current_User::user()->getUsername().' for Tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else return FALSE;
	}
	
	public function delete(\models\Transaction $transaction, $note = NULL){
		
		$calculator = new CommissionCalculator();
		
		$total_amount_in_tb = $calculator->getAmountInTbCurrency($transaction->getRemittingAgent(), $transaction->getRemittingAmount())->amount;
		
		$net_commission_in_tb = $calculator->getAmountInTbCurrency($transaction->getRemittingAgent(), $transaction->getNetCommission())->amount;
		
		$this->ledgerPosting($transaction, $this->pr_pool_account, $transaction->getRemittingAgent()->getName(), $total_amount_in_tb, "DEBIT", "AMOUNT");
		
		$this->ledgerPosting($transaction, $this->commission_pool_account, $transaction->getRemittingAgent()->getName(), $net_commission_in_tb, "DEBIT", "AMOUNT");
		
		$this->createTransactionNote($transaction, $note.' (Transaction Deleted)', Transaction::STATUS_DELETED);
		
		$transaction->setStatus(Transaction::STATUS_DELETED);
		$this->ci->doctrine->em->persist($transaction);
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
			log_message('info','Transaction deleted by '.\Current_User::user()->getUsername().' for Tracking number - '.$transaction->getTrackingNumber());
			return TRUE;
		}
		else return FALSE;
	}

}