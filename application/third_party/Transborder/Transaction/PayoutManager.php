<?php

namespace Transborder\Transaction;


class PayoutManager{
	
	private $ci;
	
	public function __construct(){
		$this->ci = &\CI::$APP;
	}
	
	/**
	 * 
	 * @param unknown_type $mtcnNumber
	 * @param unknown_type $forceCTBS
	 */
	public function getPayoutAdapter($mtcnNumber, $forceCTBS = FALSE){
		
		if( (substr($mtcnNumber, 0, strlen('ESW')) == 'ESW' or $forceCTBS) and \Options::get('use_ctbs','0') == '1' ) 
			
			
			return new \Transborder\Transaction\Payout\Adapters\CTBS\PayoutAdapter($mtcnNumber);
		
		return new \Transborder\Transaction\Payout\Adapters\Native\PayoutAdapter($mtcnNumber);
		
// 		if(substr($mtcnNumber, 0, strlen('ESW')) == 'ESW') $forceCTBS = TRUE;
		
// 		$native = FALSE;
// 		if($forceCTBS and \Options::get('use_ctbs','0') == '1')
// 			return new \Transborder\Transaction\Payout\Adapters\CTBS\PayoutAdapter($mtcnNumber);
		
// 		$cRepo = $this->ci->doctrine->em->getRepository('models\Common\Country');
// 		$countries = $cRepo->getCountryList();
// 		foreach($countries as $c){
// 			$fragment = substr($mtcnNumber, 0,strlen($c['mtcn']));
// 			if($c['mtcn'] == $fragment){$native = TRUE; break;}
// 		}
		
// 		if($native or \Options::get('use_ctbs','0') == '0') return new \Transborder\Transaction\Payout\Adapters\Native\PayoutAdapter($mtcnNumber);
		
// 		return new \Transborder\Transaction\Payout\Adapters\CTBS\PayoutAdapter($mtcnNumber);
		
	}
	
	
	
}