<?php

namespace Transborder\Transaction;

interface Payout 
{
	public function getDetails();
	
	public function pay();
	
	public function cancel();
	
}