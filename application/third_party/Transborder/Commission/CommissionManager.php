<?php
namespace Transborder\Commission;

class CommissionManager{
	
	
	public function __construct(){
		
	}
	
	public function findCommission($identifier,$version = NULL){
		$ent = \CI::$APP->doctrine->em->find('models\Commission',$identifier);
		
		if(is_null($version))	
			return $ent;
		
		/* @var $repo \models\Commission\CommissionLogRepository */
		$repo = \CI::$APP->doctrine->em->getRepository('models\Commission\CommissionLog');
		$log = $repo->getLogAtVersion($ent,$version);
		
		return $log;
	}
}