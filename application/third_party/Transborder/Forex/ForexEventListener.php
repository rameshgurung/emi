<?php
namespace Transborder\Forex;

use models\Common\ExchangeRate;

use Doctrine\Common\EventArgs;

use Doctrine\Common\EventSubscriber;


class ForexEventListener implements EventSubscriber{
	public function __construct(){
		
	}
	
	public function getSubscribedEvents(){
		return array('onFlush');
	}
	
	public function onFlush(EventArgs $args){
		
		/* @var $em \Doctrine\ORM\EntityManager
		 * 
		 * */
		
// 		$em = $args->getEntityManager();
// 		$uow = $em->getUnitOfWork();
		
// 		foreach ($uow->getScheduledEntityInsertions() AS $entity) {
// 			if($entity instanceof ExchangeRate){
				
// 				/* @var $frepo \models\Repository\Common\ForexRepository */
// 				$frepo = $em->getRepository('models\Common\ExchangeRate');
				
// 				$oldExchangeRate = $frepo->getActiveGlobalExchangeRates($entity->getCurrency()->id());
				
// 				if($oldExchangeRate->getCrossRate() !== $entity->getCrossRate()){
// 					$oldExchangeRate->deActivate();
				
// 					$uow->computeChangeSet($em->getClassMetaData(get_class($oldExchangeRate)),$oldExchangeRate);
// 				}else{
// 					$em->detach($entity);
// 				}
// 			}
// 		}
	}
}