<?php

namespace Transborder\AMLParser\AML\Adapters\LOCAL;

use models\AML;

class AMLAdapter implements \Transborder\AMLParser\AML{
	
	private $filepath;
	private $doctrine;
	
	public function __construct($file_path){
		$this->doctrine = & \CI::$APP->doctrine;
		$this->filepath = $file_path;
	}
	
	public function import($fileOrder=NULL){
		
		require_once APPPATH."third_party/Excel/excel_reader2.php";
		$data = new \Spreadsheet_Excel_Reader($this->filepath);
		$data->setOutputEncoding('CP1251');
		$data = $data->sheets[0]['cells'];
		$data1= array_shift($data);
		$arrdata = array();
		$groupID = array();
		
		foreach ($data as $e){
			
			$firstname		=	isset($e[1]) ? $e[1]:'';
			$middlename		=	isset($e[2]) ? $e[2]:'';
			$lastname		=	isset($e[3]) ? $e[3]:'';
			$title			=	isset($e[4]) ? $e[4]:'';
			$dob			=	isset($e[5]) ? $e[5]:'';
			$place_O_B		=	isset($e[6]) ? $e[6]:'';
			$country_O_B	=	isset($e[7]) ? $e[7]:'';
			$nationality	=	isset($e[8]) ? $e[8]:'';
			$postion 		= 	isset($e[9]) ? $e[9]:'';
			$address		=	isset($e[10]) ? $e[10]:'';
			$zip			=	isset($e[11]) ? $e[11]:'';
			$country		=	isset($e[12]) ? $e[12]:'';
			$description	=	isset($e[13]) ? $e[13]:'';
			$title			=	isset($e[14]) ? $e[14]:'';
			$details		=	isset($e[15]) ? $e[15]:'';
			$issued_country	=	isset($e[16]) ? $e[16]:'';
			$otherInfo		=	isset($e[17]) ? $e[17]:'';
			$group_type		=	isset($e[18]) ? $e[18]:'';
			$program		=	isset($e[19]) ? $e[19]:'';
			$listed_on		=	isset($e[20]) ? $e[20]:'';
			$group_id		=	isset($e[21]) ? $e[21]:'';
			
			if(in_array($group_id,$groupID)){
				foreach($arrdata as $k => $v){
					if($arrdata[$k]['group_id'] == $group_id){
						
							$fullname	= $firstname.' '.$middlename.' '.$lastname;
							
					 		$alias  		= array('alias_type'=>'AKA', 'alias_name'=>$fullname, 'remarks'=>$otherInfo);
							$address		= array('description'=>$description,'address'=>$address,'country'=>$country,'remarks'=>$zip);
							$documents		= array('title'=>$title,'description'=>$details,'remarks'=>'','issued_country'=>$issued_country);
							$arrdata[$k]['alias'][] 	= $alias;
							$arrdata[$k]['document'][]	= $documents;
							$arrdata[$k]['address'][]	= $address;
						
					} 
				}
			
			}
			
			else{
				$groupID[] = $group_id;
				if(($firstname && $group_id && $title) != ''){
				
					$alias  		= array('alias_type'=>'', 'alias_name'=>'', 'remarks'=>'');
					$address		= array('description'=>$description,'address'=>$address,'country'=>$country,'remarks'=>$zip);
					$documents		= array('title'=>$title,'description'=>$details,'remarks'=>'','issued_country'=>$issued_country);
					
					$arrdata[] = array(
										'aml_type'				=> AML::AML_TYPE_LOCAL,
										'group_id' 				=> $group_id,
										'group_type'			=> $group_type,
										'reference_number'		=> '',
										'title'					=> $title,
										'first_name'			=> $firstname,
										'middle_name'			=> $middlename,
										'last_name'				=> $lastname,
										'name_original_script' 	=> '',
										'dob_type'				=> '',
										'dob'					=> $dob,
										'dob_place'				=> $place_O_B,
										'country'				=> $country_O_B,
										'designation'			=> $postion,
										'nationality'			=> $nationality,
										'document' 				=> array($documents),
										'address'				=> array($address),
										'alias'					=> array($alias),
										'program'				=> $program,
										'listed_on'				=> $listed_on,
										'sort_key'				=> '',
										'remarks'				=> $otherInfo,
											);
				}
			}
			
		}
		return $arrdata;
	}
	
	
	
}