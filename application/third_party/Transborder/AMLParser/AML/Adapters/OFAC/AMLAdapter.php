<?php

namespace Transborder\AMLParser\AML\Adapters\OFAC;

use models\AML;

class AMLAdapter implements \Transborder\AMLParser\AML{
	
	private $filePath;
	private $filePath2;
	private $filePath3;
	private $doctrine;
	
	
	public function __construct($filePath, $filePath2, $filePath3){
		$this->doctrine = & \CI::$APP->doctrine;
		$this->filePath = $filePath;
		$this->filePath2 = $filePath2;
		$this->filePath3 = $filePath3;
	}
	
	public function import($fileOrder = NULL){
		
		$sdnHandle = fopen($this->filePath, 'r');
		$amlDataArray = array();
		$aliasArray[] = array('alias_type'=>'', 'alias_name'=>'', 'remarks'=>'');
		$addressArray[] = array('description'=>'','address'=>'','country'=>'','remarks'=>'');
		$documentArray[] = array('title'=>'', 'description'=>'', 'issued_country'=>'', 'remarks'=>'');
		$group_id_arr = array();
		
		while(($data = fgetcsv($sdnHandle, "", ","))){
			
			$group_id = trim($data[0]);
			$first_name = 	(isset($data[1]) && trim($data[1]) != "-0-")? $data[1] : '';
			$group_type = 	(isset($data[2]) && trim($data[2]) != "-0-")? $data[2] : '';
			$program = 		(isset($data[3]) && trim($data[3]) != "-0-")? $data[3] : '';
			$designation =	(isset($data[4]) && trim($data[4]) != "-0-")? $data[4] : '';
			if($group_type != 'vessel' && $group_id!='' && $first_name!='' && $group_type !=''){
				$group_id_arr[] = $group_id;
				$amlDataArray[]= array(
						'group_id'=> trim($group_id),
						'first_name' => trim($first_name),
						'middle_name'=> '',
						'last_name' => '',
						'reference_number' => '',
						'listed_on' => '',
						'name_original_script' => '',
						'designation' => $designation,
						'title' => '',
						'nationality' => '',
						'alias' => $aliasArray,
						'address' => $addressArray,
						'dob_type' => '',
						'dob' => '',
						'dob_place' => '',
						'country' => '',
						'program' => trim($program),
						'document' => $documentArray,
						'sort_key' => '',
						'group_type' => trim($group_type),
						'aml_type'=> AML::AML_TYPE_OFAC,
						'remarks' =>  '',
				);
			}
		}
		
		
		$sdnHandle2 = fopen($this->filePath2, 'r');
		while($addData = fgetcsv($sdnHandle2, "", ",")){
			$group_id1 = (isset($addData[0]) && trim($addData[0]) != "-0-")? trim($addData[0]) : '';
			$add_u_id = (isset($addData[1]) && trim($addData[1]) != "-0-")? trim($addData[1]) : '';
			$address = (isset($addData[2]) && trim($addData[2]) != "-0-")? trim($addData[2]) : '';
			$add_desc = (isset($addData[3]) && trim($addData[3]) != "-0-")? trim($addData[3]) : '';
			$country = (isset($addData[4]) && trim($addData[4]) != "-0-")? trim($addData[4]) : '';
			$add_remarks = (isset($addData[5]) && trim($addData[5]) != "-0-")? trim($addData[5]) : '';
			
			if(in_array($group_id1, $group_id_arr)){
				if($group_id1!='' && $add_u_id!=''){
					
					foreach($amlDataArray as $k=>$v){
						if($amlDataArray[$k]['group_id'] == $group_id1){
							$amlDataArray[$k]['address'][] = array('description'=>$add_desc,'address'=>$address,'country'=>$country,'remarks'=>$add_remarks);
						}		
					}
				}
			}
			
		}
		
		$sdnHandle3 = fopen($this->filePath3, 'r');
		while($aliasData = fgetcsv($sdnHandle3,"",",")){
			$group_id2 = (isset($aliasData[0]) && trim($aliasData[0]) != "-0-")? trim($aliasData[0]) : '';
			$alias_id = (isset($aliasData[1]) && trim($aliasData[1]) != "-0-")? trim($aliasData[1]) : '';
			$alias_type = (isset($aliasData[2]) && trim($aliasData[2]) != "-0-")? trim($aliasData[2]) : '';
			$alias_name = (isset($aliasData[3]) && trim($aliasData[3]) != "-0-")? trim($aliasData[3]) : '';
			$remarks = (isset($aliasData[4]) && trim($aliasData[4]) != "-0-")? trim($aliasData[4]) : '';
			
			if(in_array($group_id2, $group_id_arr)){
				if($group_id2!='' && $alias_name!=''){
					foreach($amlDataArray as $k=>$v){
						if($amlDataArray[$k]['group_id'] == $group_id2)
							$amlDataArray[$k]['alias'][] = array('alias_type'=>$alias_type, 'alias_name'=>$alias_name, 'remarks'=>$remarks);
					}
					
				}
			}
			
			
		}
		return $amlDataArray;
		
	}
	
}