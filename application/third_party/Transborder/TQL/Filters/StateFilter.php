<?php
namespace Transborder\TQL\Filters;
use Transborder\TQL\Lexer;
use models\Agent;

use Transborder\TQL\TQLFilter;

class StateFilter extends TQLFilter
{
	
	private $columnName;
	
	private $state = NULL;
	
	private $country = NULL;
	
	private $state_id = 0;
	
	public function parse(\Transborder\TQL\Parser $parser){
		
		$_lexer = $parser->getLexer();
		$parser->match(Lexer::T_IDENTIFIER);
		$this->columnName = $_lexer->token['value'];
	}
	
	public function getSql(){

		if (isset($_REQUEST['filter:state:'.$this->columnName]) && !empty($_REQUEST['filter:state:'.$this->columnName])) {
				
			$state = $_REQUEST['filter:state:' . $this->columnName];
			$this->state = $state;
			return  "= '".$state."'";
		}
		$this->state = 'IS NOT NULL';
		return 'IS NOT NULL';
		
	}
	
	public function getFilterLabel(){
		return trim(preg_replace(array('/([a-z]+)([A-Z]+)/', '/([A-Z]+)([A-Z])/'), array('$1 $2', '$1 $2'), $this->columnName));
	}
	
	public function getFilterElement()
	{
		$ret = "";
		
		$ret .= '<div style="float:left; margin-right:10px;">
					<span>Country</span>
					<select name="filter:country:'.$this->columnName.'" id="country_filter">
							<option value=""> Any Country </option>';
		
		\CI::$APP->load->helper('country/country');
		$operatingCountries = getOperatingCountries();
		
		foreach($operatingCountries as $id => $name)
		{
			$sel = ($this->country == $id) ? 'selected="selected"' : '';
			$ret .= '<option value="'.$id.'" '.$sel.' > '.$name.' </option>';
		}
		
		$ret .= '</select></div>';
		
		$ret .= '<div style="float:left; margin-right:10px;">
					<span>State</span>
					<select name="filter:state:' . $this->columnName . '" id="states_filter"><option value=""> Any State </option>';
		$ret .= '</select></div>';
		
		$ret .= $this->getFilterScript();
		
		return $ret;
	}
	
	public function getFilterValue(){
		if ( ! is_numeric($this->state) ) return 'Any State';
		return \CI::$APP->doctrine->em->find('models\Common\State', $this->state)->getName();
	} 
	
	public function getFilterScript()
	{
			
		$filterScript = "<script>$(function() { ";
	
	
		$filterScript .= "$('#country_filter').live('change',function(){
	
				var _obj = $(this);
				var sel = _obj.val();
	
				if(sel !== '') {
			
					$.ajax({
						type : 'GET',
						url : Transborder.config.base_url+'country/ajax/getStates/'+sel+'/".$this->state."',
						success : function(res){
							$('#states_filter').html(res);
							}
						});
	
					} else {
						$('#states_filter').html('<option value=\"\"> Any State </option>');
					}
				});
		
				$('.filter-agent-country-".$this->columnName."').trigger('change');
			";
	
		$filterScript .= "$('#country_filter').val(".$this->country.").trigger('change')";
			
		$filterScript .= "});</script>";
	
		return $filterScript;
	}
	
}