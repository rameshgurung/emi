<?php
namespace Transborder\TQL\Filters;
use Transborder\TQL\Lexer;
use models\Agent;

use Transborder\TQL\TQLFilter;

class CountryFilter extends TQLFilter
{
	
	private $columnName;
	
	private $country = NULL;
	
	public function parse(\Transborder\TQL\Parser $parser){
		
		$_lexer = $parser->getLexer();
		$parser->match(Lexer::T_IDENTIFIER);
		$this->columnName = $_lexer->token['value'];
	}
	
	public function getSql(){
		
		if (isset($_REQUEST['filter:country:'.$this->columnName]) && !empty($_REQUEST['filter:country:'.$this->columnName])) {

			$country = $_REQUEST['filter:country:' . $this->columnName];
			$this->country = $country;
			return "= '".$country."'";
			
		}
		
		$this->country = 'IS NOT NULL';
		return 'IS NOT NULL';
	}
	
	public function getFilterLabel(){
		return trim(preg_replace(array('/([a-z]+)([A-Z]+)/', '/([A-Z]+)([A-Z])/'), array('$1 $2', '$1 $2'), $this->columnName));
	}
	
	public function getFilterElement(){
		
		$operatingCountries = \Options::get('config_operating_countries');
		
		$ret = '<div style="float:left; margin-right:10px;">
				<span>'.$this->getFilterLabel().'</span>
				<select name="filter:country:' . $this->columnName . '">
					<option value=""> Any Country </option>';
		
		foreach ($operatingCountries as $c) {
			
			$country = \CI::$APP->doctrine->em->find('models\Common\Country', $c);
			$selectString = ($c == $this->country) ? 'selected="selected"' : ''; 
			if ($country)
				$ret .= "<option value='{$c}' {$selectString}> {$country->getName()}</option>";	
		}
		
		$ret .= '</select></div>';

		return $ret;
	}
	
	public function getFilterValue(){
		if ( ! is_numeric($this->country) ) return 'Any Country';
		return \CI::$APP->doctrine->em->find('models\Common\Country', $this->country)->getName();
	} 
}