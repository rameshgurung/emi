<?php

namespace Transborder\Common\Util;
use models\User\Group;

use \models\Transaction;

use Doctrine\ORM\Tools\Console\ConsoleRunner;

class CommissionCalculator{
	private $ci;
	
	public function __construct(){
		$this->ci = & \CI::$APP;
	}
	
	private function formatMoney($amount){
		return number_format($amount,2,'.','');
	}
	
	private function calculateTotalWithCommission($sourceAmount, $commissionSlab){
		$totalWithFlatCom = $sourceAmount + $commissionSlab->getOverallCommissionFlat();
		$totalWithPercentage = $sourceAmount + ((($commissionSlab->getOverallCommissionPercent())/100)*$sourceAmount);
	
		return ($totalWithFlatCom > $totalWithPercentage) ? $totalWithFlatCom : $totalWithPercentage;
	}
	
	private function getEquivalentUSD($amount, $currency, $rate){
		$result = (1/$rate->getBuyingRate()) * $amount;
		return $result;
	}
	
	private function getTargetAmountFromUSD($amount, $currency, $rate){
		$result = $amount * $rate->getBuyingRate();
		return $result;
	}
	
	
	private function getApplicableCommissionSlab(\models\Transaction $transaction){
		$rcrepo = $this->ci->doctrine->em->getRepository('models\RemitCommission');
		$commissionSlabs = $rcrepo->getCommissionSlab($transaction);
		$sendingAmount = $transaction->getRemittingAmount();
		if(is_null($commissionSlabs)){
			return NULL;
		}
		else{
			$applicableCommissionSlab = NULL;
			foreach($commissionSlabs as $cs){
				if($sendingAmount >= $cs->getLowerLimit() && $sendingAmount <= $cs->getUpperLimit()){
					$applicableCommissionSlab = $cs->id();
					continue;
				}
			}
			return $rcrepo->findOneBy(array('id'=>$applicableCommissionSlab));
		}
		return NULL;
	}
	
	public function calculateTotalAmount($sourceAmount,$sourceCurrency,$targetCurrency,$agent, $destCountry, $payoutAgent, $paymentType, $bank){
		
		$frepo = $this->ci->doctrine->em->getRepository('models\Common\ExchangeRate');
		$curepo = $this->ci->doctrine->em->getRepository('models\Common\Currency');
		$rcrepo = $this->ci->doctrine->em->getRepository('models\RemitCommission');
		
		$target_cur = $targetCurrency->getIsoCode();
		
		$lcyToUSDRate = $frepo->getCurrentForexRates($agent,$sourceCurrency);
		$USDToTargetCurrencyRate = $frepo->getCurrentForexRates($agent,$targetCurrency);
		
		if($sourceCurrency->getIsoCode()  == $targetCurrency->getIsoCode()) $sourceToDestinationExchangeRate = '1';
		elseif($sourceCurrency->getIsoCode()  == 'USD') $sourceToDestinationExchangeRate = $USDToTargetCurrencyRate->getBuyingRate();
		elseif($targetCurrency->getIsoCode() == 'USD') $sourceToDestinationExchangeRate = $lcyToUSDRate->getBuyingRate();
		else $sourceToDestinationExchangeRate = $USDToTargetCurrencyRate->getBuyingRate()/$lcyToUSDRate->getBuyingRate();
		
		$remitting_exchange_id = $lcyToUSDRate->getId();
		$receiving_exchange_id = $USDToTargetCurrencyRate->getId();
		$amountInUSD = $this->getEquivalentUSD($sourceAmount, $sourceCurrency, $lcyToUSDRate);

		$targetAmount = ($sourceCurrency !== $targetCurrency)
						? $this->getTargetAmountFromUSD($amountInUSD, $targetCurrency, $USDToTargetCurrencyRate)
						: $sourceAmount;

		$subAgent = ($payoutAgent == 0)? NULL : $this->ci->doctrine->em->find('models\Agent',$payoutAgent);
		$paAgent = (is_null($subAgent))? NULL : $subAgent->getParentAgent();
		
		if((is_null($paAgent ))){
			 $paAgent=$subAgent;
		}
		
		$applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($sourceAmount, $agent, $destCountry, $paymentType, $paAgent, $bank);
		
		if(is_null($applicableCommissionSlab))
			$applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($sourceAmount, $agent, $destCountry, $paymentType, NULL, $bank);
		
		if(is_null($applicableCommissionSlab)) throw new \Exception('Commission slab not found');
		
		$total = $this->calculateTotalWithCommission($sourceAmount, $applicableCommissionSlab);
		$convert = new ConvertNumberToWord();
		$amount_in_words = $convert->numberToWord($targetAmount);
		
		$return = new \stdClass();
		$return->sourceAmount = $this->formatMoney($sourceAmount);
		$return->totalAmount = $this->formatMoney($total);
		$return->serviceCharge = $this->formatMoney($total - $sourceAmount);
		$return->targetAmount = $this->formatMoney($targetAmount);
		$return->amount_in_words = $target_cur.' '.$amount_in_words;
		$return->remitting_exchange_id = $remitting_exchange_id;
		$return->receiving_exchange_id = $receiving_exchange_id;
		$return->amountInUSD = $amountInUSD;
		$return->sdExchangeRate = number_format($sourceToDestinationExchangeRate,4,'.','');
		$return->commission_slab_id = $applicableCommissionSlab->id();
		return $return;
	}
	
	
	public function calculateSendingAmount($totalAmount, $sourceCurrency, $targetCurrency, $destCountry, $agent, $paymentType, $payoutAgent, $bank){
		
		$frepo = $this->ci->doctrine->em->getRepository('models\Common\ExchangeRate');
		$curepo = $this->ci->doctrine->em->getRepository('models\Common\Currency');
		$rcrepo = $this->ci->doctrine->em->getRepository('models\RemitCommission');
		
		$target_cur = $targetCurrency->getIsoCode();
			
		$lcyToUSDRate = $frepo->getCurrentForexRates($agent,$sourceCurrency);
		$remitting_exchange_id = $lcyToUSDRate->getId();
			
		$USDToTargetCurrencyRate = $frepo->getCurrentForexRates($agent,$targetCurrency);
		$receiving_exchange_id = $USDToTargetCurrencyRate->getId();
		
		if($sourceCurrency->getIsoCode()  == $targetCurrency->getIsoCode()) $sourceToDestinationExchangeRate = '1';
		elseif($sourceCurrency->getIsoCode()  == 'USD') $sourceToDestinationExchangeRate = $USDToTargetCurrencyRate->getBuyingRate();
		elseif($targetCurrency->getIsoCode() == 'USD') $sourceToDestinationExchangeRate = $lcyToUSDRate->getBuyingRate();
		else $sourceToDestinationExchangeRate = $USDToTargetCurrencyRate->getBuyingRate()/$lcyToUSDRate->getBuyingRate();
		
		$subAgent = ($payoutAgent == 0)? NULL : $this->ci->doctrine->em->find('models\Agent',$payoutAgent);

		$paAgent = (is_null($subAgent))? NULL : $subAgent->getParentAgent();
		
		if((is_null($paAgent ))){
			$paAgent=$subAgent;
		}
		
		$commissions = $rcrepo->getCommissions($agent, $destCountry, $paymentType, $paAgent, $bank);
		
		if(is_null($commissions)) $commissions = $rcrepo->getCommissions($agent, $destCountry, $paymentType, NULL, $bank);
		
		if(is_null($commissions)) throw new \Exception('Commission slab not found');
		
		$commissionFlat = 0;
		$commissionOnPercent = 0;
		$applicableCommission = 0;
		$commission_slab_id = NULL;
			
		for($i=0; $i<sizeof($commissions); $i++){
		
		
			if($i == 0 ) $newLowerLimit = $commissions[$i]->getLowerLimit();
			else{
				$j = $i-1;
				$newLowerLimit = $commissions[$i]->getLowerLimit()+$commissions[$j]->getOverallCommissionFlat();
			}
			$newUpperLimit = $commissions[$i]->getUpperLimit() + $commissions[$i]->getOverallCommissionFlat();
			$commission = $commissions[$i]->getOverallCommissionFlat();
			if($totalAmount >= $newLowerLimit && $totalAmount <= $newUpperLimit){
				$commissionFlat = $commissions[$i]->getOverallCommissionFlat();
				$commissionOnPercent = ($totalAmount * $commissions[$i]->getOverallCommissionPercent())/100;
				$applicableCommission = ($commissionFlat > $commissionOnPercent) ? $commissionFlat : $commissionOnPercent;
				$commission_slab_id = $commissions[$i]->id();
				continue;
			}
		}
		
		if(is_null($commission_slab_id)) throw new \Exception('Appropriate commission slab not found');
			
		$sendingAmount = $totalAmount - $applicableCommission;
		$targetAmountEqualToUSD = $this->getEquivalentUSD($sendingAmount, $sourceCurrency, $lcyToUSDRate);
		if($sourceCurrency !== $targetCurrency){
			$targetAmount = $this->getTargetAmountFromUSD($targetAmountEqualToUSD, $targetCurrency, $USDToTargetCurrencyRate);
		}
		else{
			$targetAmount = $sendingAmount;
		}
		$convert = new ConvertNumberToWord();
		$amount_in_words = $convert->numberToWord($targetAmount);
			
		$return = new \stdClass();
		$return->totalAmount = $this->formatMoney($totalAmount);
		$return->sendingAmount = $this->formatMoney($sendingAmount);
		$return->totalCommission = $applicableCommission;
		$return->targetAmount = $this->formatMoney($targetAmount);
		$return->amount_in_words = $target_cur.' '.$amount_in_words;
		$return->remitting_exchange_id = $remitting_exchange_id;
		$return->receiving_exchange_id = $receiving_exchange_id;
		$return->amountInUSD = $targetAmountEqualToUSD;
		$return->sdExchangeRate = number_format($sourceToDestinationExchangeRate,2,'.','');
			
		return $return;
	}
	
	public function calculateRemitterCommission(\models\Transaction $transaction){
		
		$amount = $transaction->getRemittingAmount();
		$remitAgent = $transaction->getRemittingAgent();
		$payoutAgent = is_null($transaction->getPayoutAgent())? NULL : $transaction->getPayoutAgent();
		$paymentType = $transaction->getRemittanceType();
		$destCountry = 	($transaction->getRemittanceType() == \models\Transaction::TYPE_BANK_PAYMENT)
				? $transaction->getBankBranch()->getBank()->getCity()->getState()->getCountry()
				: ((!is_null($transaction->getBeneficiary()->getCity()))? $transaction->getBeneficiary()->getCity()->getState()->getCountry() : $transaction->getPayoutAgent()->getCity()->getCountry());
		$rcrepo = $this->ci->doctrine->em->getRepository('models\RemitCommission');
		
		if($paymentType == \models\Transaction::TYPE_BANK_PAYMENT && !is_null($transaction->getBankBranch())){
			$bank_id = $transaction->getBankBranch()->getBank()->id();
		}
		else $bank_id = 0;
		
		$applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($amount,$remitAgent, $destCountry, $paymentType, $payoutAgent, $bank_id);
		if(is_null($applicableCommissionSlab)) $applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($amount,$remitAgent, $destCountry, $paymentType, NULL, $bank_id);
		if(is_null($applicableCommissionSlab)) return NULL;
		
		$remitterCommissionFlat = $applicableCommissionSlab->getRaCommissionFlat();
		$remitterCommissionPercent = ($applicableCommissionSlab->getRaCommissionPercent() * $transaction->getNetCommission())/100;
		$remitterCommission = ($remitterCommissionFlat > $remitterCommissionPercent) ? $remitterCommissionFlat : $remitterCommissionPercent;

		$rcom = new \stdClass();
		
		$parent_agent = (!is_null($remitAgent->getParentAgent()))? $remitAgent->getParentAgent() : $remitAgent;
		
		if($remitAgent->isTaxable() && $parent_agent->isTaxable()){
			$tds = $this->getTDS($remitAgent);
			$rcom->tds_per = $tds;
			$rcom->gross = $remitterCommission;
			$rcom->tds = ($tds * $remitterCommission)/100;
			$raComNet = $remitterCommission - ($remitterCommission * $tds / 100);
			$rcom->net = $raComNet;
		}
		else{
			$rcom->tds_per = 0;
			$rcom->gross = $remitterCommission;
			$rcom->tds = 0;
			$rcom->net = $remitterCommission;
		}
		return $rcom;	
	}
	
	public function calculatePayoutCommission(\models\Transaction $transaction){
		
		$amount = $transaction->getRemittingAmount();
		$remitAgent = $transaction->getRemittingAgent();
		$payoutAgent = is_null($transaction->getPayoutAgent())? NULL : $transaction->getPayoutAgent();
		$paymentType = $transaction->getRemittanceType();
		$destCountry = 	($transaction->getRemittanceType() == \models\Transaction::TYPE_BANK_PAYMENT)
			? $transaction->getBankBranch()->getBank()->getCity()->getState()->getCountry()
			: ((!is_null($transaction->getBeneficiary()->getCity()))? $transaction->getBeneficiary()->getCity()->getState()->getCountry() : $transaction->getPayoutAgent()->getCity()->getCountry());
		$rcrepo = $this->ci->doctrine->em->getRepository('models\RemitCommission');
		
		if($paymentType == \models\Transaction::TYPE_BANK_PAYMENT && !is_null($transaction->getBankBranch())){
			$bank_id = $transaction->getBankBranch()->getBank()->id();
		}
		else $bank_id = 0;
		
		$applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($amount,$remitAgent, $destCountry, $paymentType, $payoutAgent, $bank_id);
		if(is_null($applicableCommissionSlab)) $applicableCommissionSlab = $rcrepo->getApplicableCommissionSlab($amount,$remitAgent, $destCountry, $paymentType, NULL, $bank_id);
		if(is_null($applicableCommissionSlab)) return NULL;
		
		$sourceCurrency = $transaction->getRemittingAgent()->getCountry()->getCurrency();
		$destCurrency = $transaction->getPayoutAgent()->getCountry()->getCurrency();
		
		$frepo = $this->ci->doctrine->em->getRepository('models\Common\ExchangeRate');
		$raExRate = $frepo->getCurrentForexRates($transaction->getRemittingAgent(), $sourceCurrency);
		$paExRate = $frepo->getCurrentForexRates($transaction->getPayoutAgent(), $destCurrency);
		
		$p_com_flat_p_lcy = $applicableCommissionSlab->getPaCommissionFlat();
		$net_com_usd = $this->getEquivalentUSD($transaction->getNetCommission(), $sourceCurrency, $raExRate);
		$net_com_p_lcy = $this->getTargetAmountFromUSD($net_com_usd, $destCurrency, $paExRate);
		$p_com_per_p_lcy = ($net_com_p_lcy * $applicableCommissionSlab->getPaCommissionPercent())/100;
		$payoutCommission = ($p_com_flat_p_lcy > $p_com_per_p_lcy)? $p_com_flat_p_lcy : $p_com_per_p_lcy;
		
		$pcom = new \stdClass();
		
		$parent_agent = (!is_null($payoutAgent->getParentAgent()))? $payoutAgent->getParentAgent() : $payoutAgent;
		
		if($payoutAgent->isTaxable() && $parent_agent->isTaxable()){
			$tds = $this->getTDS($payoutAgent);
			$pcom->tds_per = $tds;
			$pcom->gross = $payoutCommission;
			$pcom->tds = ($tds * $payoutCommission)/100;
			$paComNet = $payoutCommission - ($payoutCommission * $tds / 100);
			$pcom->net = $paComNet;
		}
		else{
			$pcom->tds_per = 0;
			$pcom->gross = $payoutCommission;
			$pcom->tds = 0;
			$pcom->net = $payoutCommission;
		}
		return $pcom;	
	}
	
	
	public function calculateBankCommission(\models\Transaction $transaction){
		
		$remitAgent = $transaction->getRemittingAgent();
		$payoutAgent = is_null($transaction->getPayoutAgent())? NULL : $transaction->getPayoutAgent();
		
		$sourceCurrency = $transaction->getRemittingAgent()->getCountry()->getCurrency();
		$destCurrency = $transaction->getPayoutAgent()->getCountry()->getCurrency();
		
		$adminrepo = $this->ci->doctrine->em->getRepository('models\User');
		$superadminUser = $adminrepo->findOneBy(array('groups'=>Group::SUPER_ADMIN));
		$superadminAgent = $superadminUser->getAgent();
		$superAdminParentAgent = (!is_null($superadminAgent->getParentAgent()))? $superadminAgent->getParentAgent() : $superadminAgent;
		$superadminCurrency = $superAdminParentAgent->getCountry()->getCurrency();
		
		
		$frepo = $this->ci->doctrine->em->getRepository('models\Common\ExchangeRate');
		$raExRate = $frepo->getCurrentForexRates($remitAgent, $sourceCurrency);
		$paExRate = $frepo->getCurrentForexRates($payoutAgent, $destCurrency);
		
		$netComUSD = $this->getEquivalentUSD($transaction->getNetCommission(), $sourceCurrency, $raExRate);
		
		$paCom = $this->calculatePayoutCommission($transaction);
		$paCommission = (!is_null($paCom))? $paCom->net : 0;
		
		
		$raCom = $this->calculateRemitterCommission($transaction);
		$raCommission = (!is_null($raCom))? $raCom->net : 0;
		
		$paComUSD = $this->getEquivalentUSD($paCommission, $destCurrency, $paExRate);
		$raComUSD = $this->getEquivalentUSD($raCommission, $sourceCurrency, $raExRate);
		
		$bankComUSD = $netComUSD - ($paComUSD + $raComUSD);
		
		$superadminExRate = $frepo->getCurrentForexRates($superAdminParentAgent, $superadminCurrency);
		
		$bankComLCY = $this->getTargetAmountFromUSD($bankComUSD, $superadminCurrency, $superadminExRate);
		
		$bcom = new \stdClass();
		$bcom->currency = $superadminCurrency->getIsoCode();
		$bcom->commission = $bankComLCY;
		return $bcom;
	}
	
	
	public function getTDS($agent){
		$stdsrepo = $this->ci->doctrine->em->getRepository('models\StateTDS');
		$ctdsrepo = $this->ci->doctrine->em->getRepository('models\CountryTDS');
		$state = $agent->getCity()->getState();
		$country = $agent->getCountry();
			
		$stateTds = $stdsrepo->findOneBy(array('state'=>$state->id(),'active'=>TRUE));
		if(is_null($stateTds)){
			$country_tds = $ctdsrepo->findOneBy(array('country'=>$country->id(),'active'=>TRUE));
			$tds = (is_null($country_tds))? 0 : $country_tds->getTds();
		}
		else $tds = $stateTds->getTds();
		
		return $tds;
	}
	
	public function getTbCurrency(){
		$adminrepo = $this->ci->doctrine->em->getRepository('models\User');
		$superadminUser = $adminrepo->findOneBy(array('groups'=>Group::SUPER_ADMIN));
		$superadminAgent = $superadminUser->getAgent();
		$superAdminParentAgent = (!is_null($superadminAgent->getParentAgent()))? $superadminAgent->getParentAgent() : $superadminAgent;
		$superadminCurrency = $superAdminParentAgent->getCountry()->getCurrency();
		return $superadminCurrency;
	}
	
	public function getAmountInTbCurrency(\models\Agent $agent, $amount){
		
		$super_agent = (!is_null($agent->getParentAgent())) ? $agent->getParentAgent() : $agent;
		
		$agent_currency = $super_agent->getCountry()->getCurrency();
		
		$adminrepo = $this->ci->doctrine->em->getRepository('models\User');
		
		$superadminUser = $adminrepo->findOneBy(array('groups'=>Group::SUPER_ADMIN));
		
		$superadminAgent = $superadminUser->getAgent();
		
		$superAdminParentAgent = (!is_null($superadminAgent->getParentAgent()))? $superadminAgent->getParentAgent() : $superadminAgent;
		
		$tb_currency = $superAdminParentAgent->getCountry()->getCurrency();
		
		$frepo = $this->ci->doctrine->em->getRepository('models\Common\ExchangeRate');
		
		$agent_exchange_rate = $frepo->getCurrentForexRates($super_agent, $agent_currency);
		
		$tb_exchange_rate = $frepo->getCurrentForexRates($superAdminParentAgent, $tb_currency);
		
		$agent_currency_to_usd = $this->getEquivalentUSD($amount, $agent_currency, $agent_exchange_rate);
		
		$usd_to_tb_currency = $this->getTargetAmountFromUSD($agent_currency_to_usd, $tb_currency, $tb_exchange_rate);
		
		$return = new \stdClass();
		
		$return->amount = $usd_to_tb_currency;
		
		$return->currency = $tb_currency;
		
		$return->currency_code = $tb_currency->getIsoCode();
		
		return $return;
		
	}

}