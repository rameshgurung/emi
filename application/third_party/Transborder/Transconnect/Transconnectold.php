<?php

namespace Transborder\Transconnect;

use models\Transaction;

use models\Transaction\WalletPayment;

use Transborder\Transconnect\AlertService\TransconnectListener;

class Transconnect
{
	
	private $_client;
	
	private $_serviceID;
	
	private $_response;
	
	private $_transactionDetails;
	
	private $_params = array();
	
	public function __construct(){
		
		
		try {
						
				$this->_client = @new \SoapClient(\Options::get('ctbs_url', 'http://ctbs.f1soft.com.np/transconnect/tcws/transconnectservice.wsdl'), array('exception'=>0));
				$this->_client->__setLocation(\Options::get('ctbs_url'));
				$this->_params = array(	'username'	=>	\Options::get('ctbs_username'),
										'password'	=>	\Options::get('ctbs_password')
										);
				//client for connecting transconnect
				$this->_client2 = @new \SoapClient(\Options::get('transconnect_url', 'http://ctbs.f1soft.com.np/transconnect/tcws/transconnectservice.wsdl'));
				$this->_client2->__setLocation(\Options::get('transconnect_url'));
				$this->_params = array(	'username'	=>	\Options::get('transconnect_username'),
						'password'	=>	\Options::get('transconnect_password')
				);
				
// 			if(isset(\CI::$APP->config->item('proxy_ips')))
// 			{
// 				$this->_params['proxy_host'] = \CI::$APP->config->item('proxy_ips'); 
// 			}

		} catch (\Exception $e) {
			\CI::$APP->message->set($e->getMessage(), 'error', TRUE, 'feedback');
			redirect('');
		}
		
	}
	
	
	public function getServiceInfoId($data)
	{
		
		if ($data['type'] == Transaction::TYPE_ID_PAYMENT){
			$this->_params['requestCode'] = 'NEW_REMIT_REQ';
// 			$this->_params['transactionURI'] = $data['beneficiary_name'];
		}
		elseif ($data['type'] == Transaction::TYPE_WALLET_PAYMENT){
// 			$this->_params['transactionURI'] = $data['wallet_id'];
				
			if($data['wallet_type'] == WalletPayment::TYPE_MWALLET)
				$this->_params['requestCode'] = 'MWALLET';
			else
				$this->_params['requestCode'] = 'EWALLET';
		}
		else {
			$this->_params['requestCode'] = 'BANK';
// 			$this->_params['transactionURI'] = $data['bank_bic'].";".$data['account_number'];
		}
		
		$this->_params['uniqueId'] = $data['tracking_number'];
		$this->_params['agentUserId'] = $data['agentUserId'];
		$this->_params['mtcnDetail'] = array(
								'status'=>'',
								'beneficiaryName'	=>	$data['beneficiaryName'],
								'beneficiaryAddress'	=>	$data['beneficiaryAddress'],
								'beneficiaryContactDetail'	=>	$data['beneficiaryContactDetail'],
								'originatingAmount'	=>	$data['originatingAmount'],
								'originatingCurrency'	=>	$data['originatingCurrency'],
								'originatingCountry'	=>	$data['originatingCountry'],
								'receivingAmount'	=>	$data['receivingAmount'],
								'receivingCurrency'	=>	$data['receivingCurrency'],
								'senderAgentCode'	=>	$data['senderAgentCode'],
								'senderName'	=>	$data['senderName'],
								'senderContactDetail'	=>	$data['senderContactDetail'],
								'senderAddress'	=>	$data['senderAddress'],
					);
		$this->_params['requiredFields'] = array(
							array(	'key'=>'issuerDocumentId', 		'value' => $data['issuerDocumentId']),
							array(	'key'=>'relation', 	'value' => $data['relation']),
							array(  'key'=>'sourceOfFund', 'value' =>	$data['sourceOfFund']),		
				);
		
// 		$this->_params['amountInformation'] = array(	
// 														'amount'	=>	$data['amount'],
// 														'currency'	=>	$data['currency']
// 													);
// 			$this->_params['originatingAmount'] = $data['amount'];
// 			$this->_params['originatingCurrency'] = $data['currency'];
// 		$this->_params['senderInformation'] = array(
// 													'name'			=>	$data['remitter_name'],
// 													'contactDetail'	=>	$data['remitter_phone'],
// 													'address'		=>	$data['remitter_address']
// 												);
		
// 		$this->_params['requestFields'] = array(
// 					array(	'key'=>TransconnectConstants::ORIGINATING_COUNTRY, 		'value' => $data['source_country']),
// 					array(	'key'=>TransconnectConstants::ORIGINATING_CURRENCY, 	'value' => $data['source_currency']),
// 					array(	'key'=>TransconnectConstants::ORIGINATING_AMOUNT, 		'value' => $data['source_amount']),
// 					array(	'key'=>TransconnectConstants::SENDER_ID_TYPE, 			'value' => $data['sender_id_type']),
// 					array(	'key'=>TransconnectConstants::SENDER_ID_ISSUED_COUNTRY, 'value' => $data['sender_id_issued_country']),
// 					array(	'key'=>TransconnectConstants::SENDER_ID_NUMBER,		 	'value' => $data['sender_id_number']),
// 					array(	'key'=>TransconnectConstants::SENDER_ID_VALID_UNTIL, 	'value' => $data['sender_id_valid_until']),
// 					array(	'key'=>TransconnectConstants::SOURCE_OF_TRANSACTION, 	'value' => $data['money_source']),
// 				);
		
		
		log_message('info','Transconnect::BookingService::Params :: '.json_encode($this->_params));
		$response = $this->requestService();

		log_message('info','Transconnect::BookingService::Response :: '.json_encode($this->_response));
		
		return $response;
	}
	
	public function createTransaction($transaction, $old_status = NULL, $new_status = NULL)
	{
	
		
		$this->_params['originatingUniqueId'] = $transaction->getTrackingNumber();
		
		$this->_params['serviceInfoId'] = $transaction->getCtbsTxnID();
		
	
		log_message('info','Transconnect::createTransaction => '.json_encode($this->_params));
		
		$response = $this->_client2->Transfer($this->_params);
		
		//die(show_pre($response));
		
				
		log_message('info', 'Transfer Response :: '. json_encode($response));
		
		if($response){
			
			$this->_response = $response;
		
			return $this->checkResponse();
		}
		else 
		{
			return FALSE;
		}
		
	}
	
	private function checkResponse(){
		if($this->_response->status == 'SUCCESS')
		{
			return TRUE;
		}
		return FALSE;
	}


	private function requestService(){
		$response = $this->_client->CTBSService($this->_params);
		
		if($response){
// 			die(show_pre($response));
			
			$this->_response = $response;
			if($this->checkResponse()){
// 				$this->_serviceID = $response->serviceInfoId;
				return $response;
			}
			return FALSE;
		}
			
	}
	
	public function getResponseFields(){
		
		if(!isset($this->_response->responseFields))
			return FALSE;
		
		$fields = array();
		
		if(is_array($this->_response->responseFields)){
			foreach($this->_response->responseFields as $field){
				$fields[$field->key] = $field->value; 		
			}	
		}else{
			$fields[$this->_response->responseFields->key] = $this->_response->responseFields->value; 
		}
		return $fields;
	}
	
	public function getServiceId()
	{
		return (isset($this->_response->serviceInfoId))?
			$this->_response->serviceInfoId : FALSE;		
	}
	
	public function getTransactionDetails(){
		return (isset($this->_response->transactionDetail)) ? 
			$this->_response->transactionDetail:FALSE;
	}
	
	public function getResponseMessage(){
		return $this->_response->genericMessage;
	}
	
	public function getResponseCode(){
		return $this->_response->responseCode;
	}
		
}