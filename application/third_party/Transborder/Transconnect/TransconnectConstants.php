<?php

namespace Transborder\Transconnect;

final class TransconnectConstants
{
	
	const ORIGINATING_CURRENCY 	= "TCMAP0001";
	const ORIGINATING_COUNTRY 	= "TCMAP0002";
	const BENEFICIARY_ADDRESS 	= "TCMAP0003";
	const MTCN 					= "TCMAP0004";
	const ALERT_MOBILE_NO 		= "TCMAP0005";
	const LOCAL_USD_EXCHANGE_RATE = "TCMAP0006";
	const NPR_USD_EXCHANGE_RATE = "TCMAP0007";
	
	const ORIGINATING_AMOUNT	= "TCMAP0008";
	
	const SENDER_ID_TYPE 			= "TCMAP0030";
	const SENDER_ID_ISSUED_COUNTRY 	= "TCMAP0031";
	const SENDER_ID_NUMBER			= "TCMAP0032";
	const SENDER_ID_VALID_UNTIL 	= "TCMAP0033"; //YYYY-MM-DD
	const SOURCE_OF_TRANSACTION		= "TCMAP0034";
	 	
	const PAYING_AGENT_CODE = "TCMAP0009";
	const PAYING_AGENT_NAME = "TCMAP0010";
	const BENEFICIARY_CONTACT_DETAILS = "TCMAP0029";

}
/*
TCMAP0001=Originating Amount
TCMAP0002=Originating Country
TCMAP0003=Beneficiary Address
TCMAP0004=TCMAP0004
TCMAP0005=Alert Mobile No.
TCMAP0006=Local USD Exchange Rate
TCMAP0007=NPR USD Exchange Rate
TCMAP0008=Beneficiary Contact Detail
TCMAP0009=CTBS Payer Agent Code
TCMAP0010=CTBS Payer Agent Name
TCMAP0011=FonePay Payment Code
TCMAP0012=Extra Amount
TCMAP0013=Extra Amount Type
TCMAP0014=Start Date
TCMAP0015=Course Code
TCMAP0016=Roll No.
TCMAP0017=Semester
TCMAP0018=Grade
TCMAP0019=Section
TCMAP0020=Prepaid No.
TCMAP0021=Subscriber Id
TCMAP0022=Customer Id
TCMAP0023=Area Id
TCMAP0024=Bill Month
TCMAP0025=Customer No.
TCMAP0026=Month
TCMAP0027=Product Name
TCMAP0028=Beneficiary Name
TCMAP0029=Beneficiary Contact Detail
TCMAP0030=SenderID Type
TCMAP0031=SenderID Issued Country Code
TCMAP0032=SenderID No.
TCMAP0033=SenderID Valid Until
TCMAP0034=Source Of Transaction
*/