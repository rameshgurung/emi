<?php

namespace Transborder\Transconnect\AlertService;

use models\Transaction;

use Transborder\Transconnect\AlertService\Exceptions\TcAlertException;

class TransconnectListener
{
	
	private $ci;
	
	private $doctrine;
	
	private static $username;
	
	private static $password;
	
	public function TransconnectListener($sender){
		$this->ci = &\CI::get_instance();
	}
	
	function NotifyStatus() {
		
		$this->doctrine = &\CI::get_instance()->doctrine;
		
		self::$username = \Options::get('ctbs_alert_username','alerter');
		
		self::$password = \Options::get('ctbs_alert_password','alertersecret');
		
		log_message('info',"TransconnectListener :: Alert Service Request for NotifyStatus");

		$validParameters = array(
				"(Transborder\Transconnect\AlertService\NotifyStatusRequest)",
		);
		$args = func_get_args();
		log_message('info',"TransconnectListener :: Arguments ".json_encode($args));
		
		
		$response = new NotifyStatusResponse();
		
		try {
			
			$this->_checkArguments($args, $validParameters);
			
			$arg = $args[0];
			
			$txnStatus = $arg->transactionDetail->status;
			
			$ctbsTxnId = $arg->transactionDetail->tctransactionId;
			
			$trepo = $this->doctrine->em->getRepository('models\Transaction');
			
			$transaction = $trepo->findOneBy(array('ctbsTxnID'=>$ctbsTxnId));
			
			if($txnStatus == 'COMPLETE' && !is_null($transaction) && ($transaction->getStatus() == Transaction::STATUS_PENDING || $transaction->getStatus() == Transaction::STATUS_APPROVED)){

				if(isset($arg->requestFields)){
					
					$fields = array();
					
					if(is_array($arg->requestFields)){
						
						foreach($arg->requestFields as $field){
							
							$fields[$field->key] = $field->value;
							
						}
						
					}
					else{
						$fields[$arg->requestFields->key] = $arg->requestFields->value;
					}
					
					if(key_exists('TCMAP0009',$fields) && $fields['TCMAP0009']!=''){
						
						$arepo = $this->doctrine->em->getRepository('models\Agent');
						
						$payout_agent = $arepo->findOneBy(array('branch_code'=>$fields['TCMAP0009']));
						
						if(!is_null($payout_agent)){
							
							log_message('info','Payout agent name :: '.$payout_agent->getName());
							
							$transaction->setPayoutAgent($payout_agent);
						} 
						else{ 
							
							log_message('info','Payout agent not found with code :: '.$fields['TCMAP0009']);

							$response->responseCode = 'RES0012';
							$response->status = 'FAILURE';
							$response->responseFields = array();
							
							log_message('info','TransconnectListener::NotifyStatus Response :: '.json_encode($response));
							return $response;
						
						}
					}
					else{ 
						
						log_message('info','Payout Agent Code is missing'); 
						
						$response->responseCode = 'RES0002';
						$response->status = 'FAILURE';
						$response->responseFields = array();
						
						log_message('info','TransconnectListener::NotifyStatus Response :: '.json_encode($response));
						return $response;
						
					}
					
				}
				else{
					
					log_message('info','requestFields does not exists');
					
					$response->responseCode = 'RES0002';
					$response->status = 'FAILURE';
					$response->responseFields = array();
					
					log_message('info','TransconnectListener::NotifyStatus Response :: '.json_encode($response));
					return $response;
				}
				
				$transaction->setStatus(Transaction::STATUS_PAID);
				$transaction->setPaidDate(new \DateTime());
				$this->doctrine->em->persist($transaction);
				
				try {
					$this->doctrine->em->flush();
					if($transaction->id()){
						$response->responseCode = 'RES0000';
						$response->status = 'SUCCESS';
						$response->responseFields = array();
					}
					else{
						$response->responseCode = 'RES0013';
						$response->status = 'FAILURE';
						$response->responseFields = array();
					}
				} catch (TcAlertException $e) {
					$response->responseCode = $e->getTcExceptionCode();
					$response->status = 'FAILURE';
					$response->responseFields = array();
				}
			}
			else{
				$response->responseCode = 'RES0015';
				$response->status = 'FAILURE';
				$response->responseFields = array();
			}
			
			
		} catch (TcAlertException $e) {
			$response->responseCode = $e->getTcExceptionCode();
			$response->status = 'FAILURE';
			$response->responseFields = array();
		}
		
		log_message('info','TransconnectListener::NotifyStatus Response :: '.json_encode($response));
		return $response;
	}
	
	/**
	 * Checks if an argument list matches against a valid argument type list
	 * @param array $arguments The argument list to check
	 * @param array $validParameters A list of valid argument types
	 * @return boolean true if arguments match against validParameters
	 * @throws Exception invalid function signature message
	 */
	public function _checkArguments($arguments, $validParameters) {
		log_message('info',"TransconnectListener::_checkArguments Validating passed parameters.");
		$variables = "";
		foreach ($arguments as $arg) {
			$type = gettype($arg);
			if ($type == "object") {
				$type = get_class($arg);
			}
			$variables .= "(".$type.")";
		}
		
		if (!in_array($variables, $validParameters)) {
			log_message('info',"TransconnectListener::_checkArguments Invalid Parameters :: Invalid parameter types: ".str_replace(")(", ", ", $variables));
			throw new TcAlertException("Invalid parameter types: ".str_replace(")(", ", ", $variables),'RES0012');
		}
		
		$argument = $arguments[0];
		
		$username = $argument->username;
		$password = $argument->password;
		
		if($username !== self::$username
				&& $password !== self::$password){
			log_message('info','Wrong Credentials');
			throw new TcAlertException("Wrong credentials",'RES0001');
		}
		
		return true;
	}
}