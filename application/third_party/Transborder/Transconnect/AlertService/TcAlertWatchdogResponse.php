<?php

namespace Transborder\Transconnect\AlertService;

class TcAlertWatchdogResponse {
	/**
	 * @access public
	 * @var string
	 */
	public $responseCode;
	/**
	 * @access public
	 * @var TransconnectAlertMapEntry[]
	 */
	public $resonseFields;
}