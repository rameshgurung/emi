<?php

namespace Transborder\Transconnect\AlertService;

class NotifyStatusRequest {
	/**
	 * @access public
	 * @var string
	 */
	public $username;
	/**
	 * @access public
	 * @var string
	 */
	public $password;
	/**
	 * @access public
	 * @var TransconnectTransactionAlertDetail
	 */
	public $transactionDetail;
	/**
	 * @access public
	 * @var TransconnectAlertMapEntry[]
	 */
	public $requestFields;
}