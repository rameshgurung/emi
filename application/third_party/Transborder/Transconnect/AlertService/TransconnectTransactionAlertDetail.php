<?php

namespace Transborder\Transconnect\AlertService;

class TransconnectTransactionAlertDetail {
	/**
	 * @access public
	 * @var tnsTransconnectTransactionAlertStatus
	 */
	public $status;
	/**
	 * @access public
	 * @var string
	 */
	public $tctransactionId;
	/**
	 * @access public
	 * @var string
	 */
	public $originatingUniqueId;
}