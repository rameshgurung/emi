<?php

namespace Transborder\Transconnect\AlertService;

class TransconnectAlertMapEntry {
	
	
	
	/**
	 * @access public
	 * @var string
	 */
	public $key;
	/**
	 * @access public
	 * @var string
	 */
	public $value;
	
	public function __construct($key, $value){
		$this->key = $key;
		$this->value = $value;
	}
}