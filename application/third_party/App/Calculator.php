<?php
/**
 * Created by PhpStorm.
 * User: broncha
 * Date: 6/22/15
 * Time: 5:40 PM
 */

namespace App;

class Calculator
{
    /**
     * @param $amount
     * @param $term
     * @param $interest
     * @param $startDate \DateTime
     *
     * @return array
     */
    public function calculate($amount, $term, $interest, $startDate = null,$emiStartDate=null)
    {
        $installments = [];

        $totalMonths = $term * 12;

        $rate = $interest/100;
        $monthlyRate = $rate/12;

        $finalReimbursement = 0.4 * $amount;

        $partialReimbursement = 0;

        $k = 0;
        while($k < $term) {
            $k++;
            $partialReimbursement += 1/pow(( 1 + $monthlyRate), $k * 12) ;
        }

        $num = $amount * (1 - (0.04 * $partialReimbursement) ) - ($finalReimbursement / pow(( 1 + $monthlyRate ), $totalMonths) );
        $den = 1 - pow(1+ $monthlyRate, -$totalMonths );

        // this gives approximate (calculating interest for the interest due carryover)
        $compoundInterest = ($monthlyRate * $num)/$den;

        $installmentAmount = $compoundInterest;

        $outstanding = $this->adjustInstallmentAmount($installmentAmount, $amount,$startDate,$emiStartDate,$totalMonths,$rate);
        $newOutstanding = $outstanding;

        // printf("Total outstanding %.2f :: Trying with installment amount %.2f\n", $outstanding, $installmentAmount);
        
        $lastDecrement = 10;
        $finalInstallment = $installmentAmount;

        while(abs(round($newOutstanding,2)) != 0) {

            $finalInstallment = $installmentAmount - $lastDecrement;

            $newOutstanding = $this->adjustInstallmentAmount($finalInstallment, $amount,$startDate,$emiStartDate,$totalMonths,$rate);
            
            // printf("Total outstanding %.2f after decreasing by %f\n", $newOutstanding, $lastDecrement);

            $difference = $outstanding - $newOutstanding;

            $decrement = ($lastDecrement * $outstanding) / $difference;

            $lastDecrement = $decrement;
        }

        return $finalInstallment;
    }

    private function adjustInstallmentAmount($installment, $amount, $startDate, $emiStartDate, $totalMonths, $rate)
    {
        $outstandingPrincipal = $amount;
        $dueInterest = 0;
        $dueDate = clone $startDate;
        $years = $totalMonths/12;

        for($i = 1; $i <= $totalMonths; $i++) {

            if($i==1){
                $thisDate = clone $emiStartDate;
            }
            else{
                $thisDate = clone $dueDate;

                $thisDate->add(new \DateInterval("P1M"));
            }

            $interval = $dueDate->diff($thisDate, true)->days;
            $dueDate = $thisDate;

            $actualInterest = (($outstandingPrincipal * $rate)/365 ) * $interval;
            $dueInterest += $actualInterest;

            $payment = $installment;

            if($i % 12 == 0) {
                $payment += round(0.04 * $amount, 2);
            }

            if($i == $totalMonths) {
                $payment += round((1 - (0.04 * $years )) * $amount,2);
            }

            if($payment < $dueInterest) {
                $dueInterest -= $payment;
                $interestRecovered = $payment;
            }else{
                $principal = $payment - $dueInterest;
                $outstandingPrincipal -= $principal;
                $dueInterest = 0;
                $interestRecovered = $actualInterest;
            }
        }

        return $outstandingPrincipal;
    }
}