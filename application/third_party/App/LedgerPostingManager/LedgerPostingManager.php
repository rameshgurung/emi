<?php
namespace App\LedgerPostingManager;
use models\User;
class LedgerPostingManager{
	public $ci;

	public function __construct(){
		$this->ci = \CI::$APP;
	}

	/*
	*add ledger posting
	*@param array $data 
	*@elements object ledger
	*@elements string amount
	*@elements string debit_credit
	*@elements string particular
	*@elements string type
	*/
	public function addPosting($data){
		// print_r($data);
		// exit();

			$accountNumber = $data['ledger'];
			$amount = $data['amount'];
			$dr_cr = $data['dr_cr'];
			$particular = $data['particular'];
			$type = $data['type'];
			$transaction = $data['transaction'];


			$ledgerRepo = $this->ci->doctrine->em->getRepository('models\Ledger');
			$ledger = $ledgerRepo->findOneBy(array('account_number'=> $accountNumber));

			
			
			$ledgerPosting = new \models\LedgerPosting;
			$ledgerPosting->setAccount($ledger);
			$ledgerPosting->setAmount($amount);
			$ledgerPosting->setDebitCredit($dr_cr);
			$ledgerPosting->setParticular($particular);
			$ledgerPosting->setType($type);
			$ledgerPosting->setTransaction($transaction);

			$this->ci->doctrine->em->persist($ledgerPosting);


			$cib_account_num = \Options::get('cib_account');
			$cibLedger = $ledgerRepo->findOneBy(array('account_number'=> $cib_account_num));  

			$ledgerPostingCib = new \models\LedgerPosting;
			$ledgerPostingCib->setAccount($cibLedger);
			$ledgerPostingCib->setAmount($amount);
			

			switch ($dr_cr) {
				case \models\LedgerPosting::LEDGER_POSTING_DEBIT:
					$dr_cr = \models\LedgerPosting::LEDGER_POSTING_CREDIT;
					break;
				case \models\LedgerPosting::LEDGER_POSTING_CREDIT:
					$dr_cr = \models\LedgerPosting::LEDGER_POSTING_DEBIT;
					break;	
				default:
					exit("NOT MENTIONED DEBIT OR CREDIT");
					break;
			}

			$ledgerPostingCib->setDebitCredit($dr_cr);
			$ledgerPostingCib->setParticular($particular);
			$ledgerPostingCib->setType($type);
			$ledgerPostingCib->setTransaction($transaction);

			$this->ci->doctrine->em->persist($ledgerPostingCib);	

			try{
				$response['status'] = "success";
				$response['message'] = "Ledger Posted";
				$response['posting'] = $ledgerPosting;
				$response['postingPool'] = $ledgerPostingCib;

			}catch(Exception $ex){
				$response['status'] = "error";
				$response['message'] = $ex->getMessage();
			}

			return $response;

	}

	
}