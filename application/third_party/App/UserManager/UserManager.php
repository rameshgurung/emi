<?php
namespace App\UserManager;
use models\User;
class UserManager{
	public $ci;

	public function __construct(){
		$this->ci = \CI::$APP;
	}

	public function createUser($createLedger = true){

		$user = new \models\User;

		$firstname = $this->ci->input->post("first_name");
		$middlename = $this->ci->input->post("middle_name");
		$lastname = $this->ci->input->post("last_name");

		$user->setFirstname($firstname);
		$user->setMiddlename($middlename);
		$user->setLastname($lastname);
		$city = $this->ci->doctrine->em->find('\models\Common\City',$this->ci->input->post("district"));
		$user->setCity($city);

		$user->setEmail($this->ci->input->post("email"));
		$user->setPhone($this->ci->input->post("phone"));

		if($this->ci->input->post('group')){
			$groups = $this->ci->doctrine->em->find('\models\User\Group',$this->ci->input->post("group"));
			$user->setGroup($groups);	
		}

		$user->setUsername($this->ci->input->post("username"));
		$user->setPassword(md5($this->ci->input->post("password")));
		$user->setAccName($this->ci->input->post('acc_name'));
		$acc_type = $this->ci->doctrine->em->find('models\AccountType',$this->ci->input->post('acc_type'));
		if($acc_type){
			$user->setAccType($acc_type);
		}
		$user->setLocalGov($this->ci->input->post('local_gov'));
		$user->setAddress($this->ci->input->post('address'));

		$user->setFax($this->ci->input->post('fax'));
		$accountNumber = $rand =  ( new \App\NumberManager\NumberContext("ACCOUNT") )->getNumber();
		$user->setAccNumber($accountNumber);

		if(\Current_User::user()){
			$user->setCreatedBy(\Current_User::user(\Current_User::user()));
		}

		$country = $this->ci->doctrine->em->find('models\Common\Country',$this->ci->input->post('country'));
		if($country){
			$user->setCountry($country);		
		}

		if($this->ci->input->post('bank')){

			$bank = $this->ci->doctrine->em->find('\models\Common\Bank',$this->ci->input->post("bank"));
			$user->setBankBranch($bank->getMainBranch());

		}

		$user->setSecurityAdminister($user);

		$user->markFirstLogin();
		$user->setPwdLastChangedOn();
		$user->setLastLogged();

		$user->setStatus(\models\User::STATUS_PENDING);


		$this->ci->doctrine->em->persist($user);
		$this->ci->doctrine->em->flush();
		log_message('info',"User Created. username => ".$user->getUsername());

		if($createLedger){
					//if user is of client type or govenment entity create ledger

			if($user->getAccType()->getType() == "CLIENT" || 
				$user->getAccType()->getType() == "GOVERNMENT_ENTITY" || 
				$user->getAccType()->getType() == "REGISTRY_ADMIN")
			{

				$repo = $this->ci->doctrine->em->getRepository('\models\AccountType');
				$clientAccountType = $repo->findOneBy(array('type'=> $user->getAccType()->getType() ));

				$ledger = new \models\Ledger();

				$ledger->setIsUsed(FALSE);

				$ledger->setAccountType($clientAccountType);
				$ledger->setAccountNumber($accountNumber);
				$ledger->setBalance("0.00");
				$ledger->setAccountHead(strip_tags($firstname).' - Payable/Receivable Account');

				$this->ci->doctrine->em->persist($ledger);

				$user->setLedger($ledger);

				$this->ci->doctrine->em->persist($ledger);

				$user->setLedger($ledger);
				try{
					$this->ci->doctrine->em->persist($user);								
					$to=$user->getEmail();
								// $subject=" Your account has been created ";
								// $msg=" Welcome to CIBN ";
								// $this->sentMail($to,$subject,$msg);
					$response['status'] = "success";
					$response['message'] = "User Added Successfully";
					$response['user'] = $user;


					return $response;
				}
				catch(Exception $ex){
					$response['status'] = 'error';
					$response['message'] = 'Could not ledger ';
					return $response;
				}
			}
		}
	}
	
	public function editUser(&$user)
	{

		//show_pre($this->ci->input->post());
		if($this->ci->input->post("first_name")){
			$user->setFirstname($this->ci->input->post("first_name"));	
		}
		if($this->ci->input->post("middle_name")){
			$user->setMiddlename($this->ci->input->post("middle_name"));	
		}
		if($this->ci->input->post("last_name")){
			$user->setLastname($this->ci->input->post("last_name"));	
		}
		if($this->ci->input->post("phone")){
			$user->setPhone($this->ci->input->post("phone"));		
		}
		if($this->ci->input->post("mobile")){
			$user->setMobile($this->ci->input->post("mobile"));
		}
		if($this->ci->input->post("fax")){
			$user->setFax($this->ci->input->post("fax"));
		}
		
		
		//update secured admin accname and address
		$securityAdmin=getSecurityAdminOfUser($user);
		if($this->ci->input->post('acc_name')){
			$securityAdmin->setAccName($this->ci->input->post('acc_name'));	
		}
		if($this->ci->input->post('local_gov')){
			$securityAdmin->setLocalGov($this->ci->input->post('local_gov'));	
		}
		if($this->ci->input->post('address'))
			$securityAdmin->setAddress($this->ci->input->post('address'));
		$country = $this->ci->doctrine->em->find('models\Common\Country',$this->ci->input->post('country'));
		if($country) $securityAdmin->setCountry($country);				
		$city = $this->ci->doctrine->em->find('\models\Common\City',$this->ci->input->post("district"));
		if($city) $securityAdmin->setCity($city);

		


		if($this->ci->input->post('email')){
			$user->setEmail($this->ci->input->post("email"));
		}
		if($this->ci->input->post('username')){
			$user->setUsername($this->ci->input->post("username"));	
		}

		
		if($status = $this->ci->input->post('status')){
			$user->setStatus($status);
		}


		if($this->ci->input->post('newpwd')){
			$user->setPassword(md5(trim($this->ci->input->post('newpwd'))));
			$user->setPwdLastChangedOn();
		}



		
		$group = $this->ci->doctrine->em->find('\models\User\Group',$this->ci->input->post("group"));
		if($group) $user->setGroup($group);
		try{
			$this->ci->doctrine->em->persist($user);		
			$this->ci->doctrine->em->persist($securityAdmin);								
			$response['status'] = "success";
			$response['message'] = "User edited successfully";
			$response['user'] = $user;	
			return $response;
		}
		catch(Exception $ex){
			$response['status'] = 'error';
			$response['message'] = 'Could not edit User';
			return $response;
		}
	}


	public function sentMail($to=NULL,$subject=NULL,$msg=NULL){
		if(!is_null($to) AND !is_null($subject) AND !is_null($msg)){
			$this->ci->load->library('email');
			$this->ci->email->from(\Options::get('config_adminemail', 'madhab@f1soft.com'), 'CIBN');
			$this->ci->email->to($to); 
			$this->ci->email->subject($subject);
			$this->ci->email->message($msg);	

			if(!$this->ci->email->send()) 
				return false;
			//echo $this->ci->email->print_debugger();
		}
	}
}
