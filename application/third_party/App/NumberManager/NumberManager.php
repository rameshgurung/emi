<?php
namespace App\NumberManager;

class NumberContext{

	private $strategy = null;

	public function __construct($strategy){

		// echo $strategy;
		// exit;

		switch ($strategy){
			case "ACCOUNT":
				$this->strategy =  new AccountNumberGenerator();
				break;
			case "TRANSACTION":
				$this->strategy = new TransactionNumberGenerator();
				break;
			case "WEBSESSION":
				$this->strategy = new WebsessionNumberGenerator();
				break;
			case "NOTICE":
				$this->strategy = new NoticeNumberGenerator();
				break;
			case "NOTICE_CHANGE":
				$this->strategy = new NoticeChangeNumberGenerator();
				break;
			case "CERTIFIED_SEARCH":
				$this->strategy = new CertifiedSearchNumberGenerator();
				break;
			case "RESET_KEY":
				$this->strategy = new ResetNumberGenerator();
				break;
			case "RECEIPT":
				$this->strategy = new ReceiptNumberGenerator();
				break;
			default:
				$this->strategy = new DefaultNumberGenerator();
				break;
		}
	}

	public function getNumber(){
		return $this->strategy->getNumber();
	}

}

interface NumberStrategy{
	public function getNumber();
}

class ReceiptNumberGenerator implements NumberStrategy{

	protected $length = 7;

	protected $type = 'numeric';

	public function getNumber(){
		$account_name =  \Current_User::user()->getSecurityAdminister()->getAccName();
		
		$short_name = strtoupper(substr($account_name, 0, 3));

		$ci = & get_instance();
		
		$number = $short_name . random_string($this->type, $this->length);
		
		$lbRepo = $ci->doctrine->em->getRepository('\models\LoadBalance');
		
		$lb_number = $lbRepo->findOneBy(array('receipt_number'=>$number));
		
		if($lb_number){
			return $this->getNumber();
		}
		return $number;
	}
}


class AccountNumberGenerator implements NumberStrategy{
	
	protected $length = 10;

	public function getNumber(){

		//computing using single digit check number

		$ci = & get_instance(); $rand_num = null;  $sum = null; $digit = null; $weightedDigit = null; $number = null;
		
		for($i = 0 ; $i < $this->length ; $i++){

			$weight = ( ($i % 2) == 0 ) ? 1 : 2;
		
			$digit = rand(0,9);
			
			$rand_num .= $digit;
			
			$weightedDigit = $digit * $weight;
			
			$sum += $weightedDigit;
		
		}

		$reminder = $sum % $this->length; 
		
		$acc_number = $rand_num."-".$reminder;
		
		$user_repo = $ci->doctrine->em->getRepository('\models\Ledger');
		
		$number = $user_repo->findOneBy(array('account_number'=>$acc_number));


		
		if($number) return $this->getNumber();

		return $acc_number;
	}
}

class TransactionNumberGenerator implements NumberStrategy{
	protected $length = 10;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$transRepo = $ci->doctrine->em->getRepository('\models\Transaction');
		
		$transNumber = $transRepo->findOneBy(array('transactionNumber'=>$number));
		if($transNumber){
			return $this->getNumber();
		}
		return $number;
	}
}

class CertifiedSearchNumberGenerator implements NumberStrategy{
	protected $length = 6;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$certRepo = $ci->doctrine->em->getRepository('\models\CertifiedSearch');
		
		$certNumber = $certRepo->findOneBy(array('certifiedNumber'=>$number));
		if($certNumber){
			return $this->getNumber();
		}
		return $number;
	}
}

class WebsessionNumberGenerator implements NumberStrategy{
	protected $length = 10;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$transRepo = $ci->doctrine->em->getRepository('\models\Transaction');
		
		if($transRepo->findOneBy(array('transactionNumber'=>$number))){
			return $this->getNumber();
		}
		return $number;
	}	
}

class NoticeNumberGenerator implements NumberStrategy{
	protected $length = 10;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$noticeRepo = $ci->doctrine->em->getRepository('\models\ca\Notice');
		
		if($noticeRepo->findOneBy(array('fileNumber'=>$number))){
			return $this->getNumber();
		}
		return $number;
	}	
}

class NoticeChangeNumberGenerator implements NumberStrategy{
	protected $length = 8;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$noticeRepo = $ci->doctrine->em->getRepository('\models\ca\Notice');
		
		if($noticeRepo->findOneBy(array('fileNumber'=>$number))){
			return $this->getNumber();
		}
		return $number;
	}	
}

class DefaultNumberGenerator implements NumberStrategy{
	
	public function getNumber(){
		return rand();
	}
}

class ResetNumberGenerator implements NumberStrategy{
	
	protected $length = 10;

	protected $type = 'numeric';

	public function getNumber(){
		$ci = & get_instance();
		
		$number = random_string($this->type, $this->length);
		
		$userRepo = $ci->doctrine->em->getRepository('\models\User');
		
		if($userRepo->findOneBy(array('reset_password_key'=>$number))){
			return $this->getNumber();
		}
		return $number;
	}	

}