<?php
namespace App;

class FileGenerator
{

    //loan part
    const HEAD='LD,SRBL.SRSGK.LOAN/I/PROCESS,';

    
    private $_customerUsername;
    private $_customerPassword;

    private $_branchCode;

    private $_customerId;
    private $_currency;
    private $_amount;
    private $_valueDate;
    private $_finMatDate;
    
    const INTEREST_BASIS='S';
    const INT_RATE_TYPE=1;
    const INT_PAYMT_METHOD=1;
    const CAPITALISATION='NO';
    
    private $_intersetRate;
    private $_drawDownAccount;
    private $_prinLiqAcct;
    private $_intLiqAcct;
    
    const AUTO_SCHEDS='NO';
    const DEFINE_SCHEDS='YES';
    const LIQUIDATION_MODE='SEMI-AUTOMATIC';
    const LIM_REV='Y';

    //loan schedule part
    const FORWARD_BACKWARD=4;
    const BASE_DATE_KEY=3;

    function __construct(){
    }

    function prepareloanPart($_branchCode,$_customerUsername,$_customerPassword,$_customerId,$_currency,
        $_amount,$_valueDate,$_finMatDate,$_intersetRate,$_drawDownAccount,$_prinLiqAcct,$_intLiqAcct){
        try {
            $this->_branchCode = $_branchCode;
            $this->_customerUsername = $_customerUsername;
            $this->_customerPassword = $_customerPassword;
            $this->_customerId = $_customerId;
            $this->_currency = $_currency;
            $this->_amount = $_amount;
            $this->_valueDate = $_valueDate;
            $this->_finMatDate = $_finMatDate;
            $this->_intersetRate = $_intersetRate;
            $this->_drawDownAccount = $_drawDownAccount;
            $this->_prinLiqAcct = $_prinLiqAcct;
            $this->_intLiqAcct = $_intLiqAcct;            
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    function getloanPart(){
        try {


            $loanPart=array();
            $loanPart['no_head']=self::HEAD;
            $loanPart['no_username']=$this->_customerUsername;
            $loanPart['no_password']="/".$this->_customerPassword;
            $loanPart['no_branch']="/".$this->_branchCode.",,";
            $loanPart['CUSTOMER.ID']=$this->_customerId;
            $loanPart['CURRENCY']=$this->_currency;
            $loanPart['AMOUNT'] = $this->_amount;
            $loanPart['VALUE.DATE'] = $this->_valueDate;
            $loanPart['FIN.MAT.DATE'] = $this->_finMatDate;


            $loanPart['INTEREST.BASIS'] = self::INTEREST_BASIS;
            $loanPart['INT.RATE.TYPE'] = self::INT_RATE_TYPE;
            $loanPart['INT.PAYMT.METHOD'] = self::INT_PAYMT_METHOD;
            $loanPart['CAPITALISATION'] = self::CAPITALISATION;

            $loanPart['INTEREST.RATE'] = $this->_intersetRate;
            $loanPart['DRAWDOWN.ACCOUNT'] = $this->_drawDownAccount;
            $loanPart['PRIN.LIQ.ACCT'] = $this->_prinLiqAcct;
            $loanPart['INT.LIQ.ACCT'] = $this->_intLiqAcct;

            $loanPart['AUTO.SCHEDS'] = self::AUTO_SCHEDS;
            $loanPart['DEFINE.SCHEDS'] = self::DEFINE_SCHEDS;
            $loanPart['LIQUIDATION.MODE'] = self::LIQUIDATION_MODE;
            $loanPart['LIM.REV'] = self::LIM_REV.'//';

            $loanPart['FORWARD.BACKWARD'] = self::FORWARD_BACKWARD;
            $loanPart['BASE.DATE.KEY'] = self::BASE_DATE_KEY;

            return $loanPart;  

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


}
