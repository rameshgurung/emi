<?php
namespace App;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

class Mailer {
	
	/*
	*@args $from['from_name'] = 'madhab';
	*      $from['from_email'] = 'madhab@f1soft.com';
	*@args $to['to_name'] = 'madhab';
	*      $to['to_email'] = 'madhab@f1soft.com';
	*/

	public static function sendMail($from = array(), $to = array(), $subject, $message){

		if(\Options::get('send_mail') != '1'){
			return true;
		}

		$ci  = & get_instance();

		$smtp_host = $ci->config->item('smtp_host');
		$smtp_port = $ci->config->item('smtp_port');
		$smtp_username = $ci->config->item('smtp_username');
		$smtp_password = $ci->config->item('smtp_password');
		$smtp_type = $ci->config->item('smtp_type');

		// exit($smtp_username);
		

		$transport = Swift_SmtpTransport::newInstance($smtp_host, $smtp_port, $smtp_type)
		->setUsername($smtp_username)
		->setPassword($smtp_password);

		$mailer = Swift_Mailer::newInstance($transport);

		$data['message'] = $message;
		$data['logo_url'] = theme_url()."images/mail/logo.gif";

		$html = \CI::$APP->load->theme('email/index', $data, TRUE);

		$mail = Swift_Message::newInstance($subject)
		->setFrom(array($from['from_email'] => $from['from_name']))
		->setTo(array($to['to_email'] => $to['to_name']))
		->setBody($html);

		$mail->setContentType("text/html");


		return  $mailer->send($mail);
	}
}