<?php defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'production';
$active_record = TRUE;

$db['production']['hostname'] = '10.13.212.16';
$db['production']['username'] = 'cib';
$db['production']['password'] = 'cib@123';
$db['production']['database'] = 'cib_prod';
$db['production']['dbdriver'] = 'mysqli';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = TRUE;
$db['production']['db_debug'] = TRUE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;

/* End of file database.php */
