<?php 

use Doctrine\ORM\Query\AST\Functions\SubstringFunction;

use models\Smsrecord;

use Transborder\REST\Annotations as API;



class SMS_Controller extends TBREST_Controller{

	private $APIURL = 'http://localhost/transborder/SMS?apiusername=superadmin&apipwd=123456&appkey=J84ftW2zxisnfhEasc3yV29u45Te5mBZ&';


	public function __construct(){
			
		parent::__construct();
	}

	/**
	 * @API\Arg(name="mobilenumber", rules="trim|required|alphanumeric")
	 * @API\Arg(name="message", rules="trim|required|text")
	 */

	public function  index_get()
	{
			
		$mobilenumber 		= $this->get('mobilenumber');
		$code  = substr($mobilenumber, 0, 3);
		$number  			= substr($mobilenumber, 3, 13);
		$message 			= $this->get('message');
		//insert smslog
		$insData['message'] = $message;
		$insData['phoneNumber'] = $number;
		$insData['status']   =  Smsrecord::SMS_STATUS_FALSE;
		//$insData['status'] = (stripos($res, 'Automessage : +OK'))?Smslog::SMS_STATUS_TRUE : Smslog::SMS_STATUS_FALSE;

		$this->insertSmslog($insData);

		//$this->getSmsDetails($mobilenumber, $message);


		$this->sendsms($code, $number, $message);

		$this->response(array('status' => 'SUCCESS', 'responseCode' => '0000', 'responseMessage' => $mobilenumber), HTTP_OK);
	}


	
	public function insertSmslog($insData){
	$ci = & get_instance();
	$sms = new Smsrecord();
	$sms->setCreatedBy(Current_User::user());
	$sms->setPhoneNumber($insData['phoneNumber']);
	$sms->setMessage($insData['message']);
	$sms->setStatus($insData['status']);
			$ci->doctrine->em->persist($sms);

					try {
					$ci->doctrine->em->flush();
	} catch (Exception $e) {
	log_message('info','Coulnot save sms log :: '.$ex->getMessage());
			return FALSE;
		}
		return TRUE;
	}

	public function sendsms($code, $mobilenumber, $msg)
	{

	$numOK = $numErr = array();
	$conRepo = $this->doctrine->em->getRepository('models\Common\Country');

	$country = $conRepo->findOneBy(array('dialing_code' => $code));

			$num = $code."-".$mobilenumber;

			if ($country) {
			$code = $country->getIso_2();

			$sent = $this->_sendsms_($code, $mobilenumber, $msg);

			($sent) ? $numOK[] = $num : $numErr[] = $num;

			} else {
			$numErr[] = $num;
			}

			$type = 'success';

			if (count($numOK) == 0) {
			$type  = 'error';
			$flashmsg = 'SMS not sent to any numbers.';
			}

		if (count($numOK) > 0 and count($numErr) > 0)
			$flashmsg = 'SMS sent to some numbers, skipping the invalid ones.';

			if (count($numErr) == 0)
			$flashmsg = 'SMS sent to all the numbers.';

			if (count($numOK) > 0)
				log_message('info', 'Bulk SMS: { '. str_replace(array("\r\n", "\n", "\r"), " ", $msg) .' } sent to numbers: { '. implode(', ', $numOK) . ' }');

				$this->message->set($flashmsg, $type, TRUE, 'feedback');

	}


	public function _sendsms_($code = '', $number = '', $message = ''){

	if ($code and $number and $message) {
		
	/* $data = array('apiusername'	=>	'superadmin',
	'apipwd'		=>	'123456',
	'appkey'		=>	'J84ftW2zxisnfhEasc3yV29u45Te5mBZ'
	); */

	$data = array(	'guid'			=>	'9ea78085-ecd4-4a01-a226-a50451ac9c40',
			'username'		=>	'info@nmc.org.np',
			'password'		=>	'nmc@123'
										/* 'username'		=>	'kblremit@kbl.com.np',
												'password'		=>	'kbl@123' */
														);
			

			$data['mobilenumber']	=	$number;
			 $data['countryCode']	=	$code;
			$data['message'] 		= 	$message;
		
	$url = $this->APIURL.http_build_query($data, '', '&');

			$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
	$res = curl_exec($ch);
		
			return stripos($res, 'Automessage : +OK') !== FALSE;

	}
	}


	public function index_post()
	{

	}

	}






