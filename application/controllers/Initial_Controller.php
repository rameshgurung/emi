<?php

use Transborder\TQL\TQLExecuter;



class Initial_Controller extends MY_Controller{
	const PR_POOL_ACCOUNT = '001234';
	const COMMISSION_POOL_ACCOUNT = 'test';
	const BALANCE = 100.0;
	const BANK_COMM_CURRENCY = 'NPR';
	const ADMIN_EMAIL = 'sita@f1soft.com';
	const TECHNICAL_EMAIL = 'sita@f1soft.com';
	const BANK_NAME = 'Test Bank';
	const SWIFT_CODE = 'test1234';
	const BRANCH_CODE = 'test12';
	
	public function __construct(){
		
		parent::__construct();
		
		//$res = $this->db->query("select * from f1_users");
		
		//if (count($res) > 0) redirect('dashboard');
		
		$query ='set foreign_key_checks = 0';
		$this->db->query($query);
		
		$this->InsertCountries();
		$this->InsertCurrencies();
 		$this->InsertStates();
		$this->InsertCities();
		$this->InsertTimeZones();
		$this->InsertPrincipalAgents();
		$this->InsertUsers(); 
		$this->InsertGroups(); 
		$this->InsertLedgers();
		$this->InsertDocumentTypes();
		$this->InsertOptions();
		$this->InsertBanks();
		$this->InsertBankBranch();
		$query = 'set foreign_key_checks = 1';
		$this->db->query($query);
		
		redirect('dashboard');
		
	}
	
	
	
	public function InsertCountries(){
		
	$query = "INSERT INTO `f1_countries` (`id`, `currency_id`, `name`, `iso_2`, `iso_3`, `mtcn`, `dialing_code`) VALUES
	
	(19, 4, 'Bangladesh', 'BD', 'BGD', 'BGD', '880'),
	(103, 2, 'India', 'IN', 'IND', 'IND', '91'),
	(155, 1, 'Nepal', 'NP', 'NPL', '22', '977'),
	(180, 3, 'Qatar', 'QA', 'QAT', 'QAT', '974')";
		
	$this->db->query($query);
		
	}
	
	public function InsertCurrencies(){
		
		$query = "INSERT INTO `f1_currencies` (`id`, `name`, `iso_code`, `symbol`) VALUES
					(1, 'Nepali Rupee', 'NPR', 'NPR'),
					(2, 'Indian Rupee', 'INR', 'INR'),
					(3, 'Qatari Riyal', 'QAR', 'QAR'),
					(4, 'Bangladesh Taka', 'BDT', 'BDT')";

		$this->db->query($query);
	}
	
	public function InsertStates(){
	
		$query = "INSERT INTO `f1_states` (`id`, `country_id`, `name`) VALUES
				(1, 155, 'Mechi'),
				(2, 155, 'Rapti'),
				(3, 155, 'Bagmati'),
				(4, 155, 'Karnali'),
				(5, 155, 'Sagarmatha'),
				(6, 155, 'Koshi'),
				(7, 155, 'Narayani'),
				(8, 155, 'Mahakali'),
				(9, 155, 'Gandaki'),
				(10, 155, 'Janakpur'),
				(11, 155, 'Lumbini'),
				(12, 155, 'Seti'),
				(13, 155, 'Bheri'),
				(14, 155, 'Dhawalagiri')";
	
		$this->db->query($query);
	
	}
	
	public function InsertCities(){
	
		$query = "INSERT INTO `f1_cities` (`id`, `state_id`, `name`) VALUES
				(1, 1, 'Ilam'),
				(2, 1, 'Jhapa'),
				(3, 1, 'Panchthar'),
				(4, 1, 'Taplejung'),
				(5, 2, 'Dang'),
				(6, 2, 'Pyuthan'),
				(7, 2, 'Rolpa'),
				(8, 2, 'Rukum'),
				(9, 2, 'Salyan'),
				(10, 3, 'Bhaktapur'),
				(11, 3, 'Dhading'),
				(12, 3, 'Kathmandu'),
				(13, 3, 'Kavrepalanchok'),
				(14, 3, 'Lalitpur'),
				(15, 3, 'Nuwakot'),
				(16, 3, 'Rasuwa'),
				(17, 3, 'Sindhupalchok'),
				(18, 4, 'Dolpa'),
				(19, 4, 'Humla'),
				(20, 4, 'Jumla'),
				(21, 4, 'Kalikot'),
				(22, 4, 'Mugu'),
				(23, 5, 'Khotang'),
				(24, 5, 'Okhaldhunga'),
				(25, 5, 'Saptari'),
				(26, 5, 'Siraha'),
				(27, 5, 'Solukhumbu'),
				(28, 5, 'Udayapur'),
				(29, 6, 'Bhojpur'),
				(30, 6, 'Dhankuta'),
				(31, 6, 'Morang'),
				(32, 6, 'Sankhuwasabha'),
				(33, 6, 'Sunsari'),
				(34, 6, 'Terhathum'),
				(35, 7, 'Bara'),
				(36, 7, 'Chitwan'),
				(37, 7, 'Makwanpur'),
				(38, 7, 'Parsa'),
				(39, 7, 'Rautahat'),
				(40, 8, 'Baitadi'),
				(41, 8, 'Dadeldhura'),
				(42, 8, 'Darchula'),
				(43, 8, 'Kanchanpur'),
				(44, 9, 'Gorkha'),
				(45, 9, 'Kaski'),
				(46, 9, 'Lamjung'),
				(47, 9, 'Manang'),
				(48, 9, 'Syangja'),
				(49, 9, 'Tanahu'),
				(50, 10, 'Dhanusa'),
				(51, 10, 'Dolakha'),
				(52, 10, 'Mahottari'),
				(53, 10, 'Ramechhap'),
				(54, 10, 'Sarlahi'),
				(55, 10, 'Sindhuli'),
				(56, 11, 'Arghakhanchi'),
				(57, 11, 'Gulmi'),
				(58, 11, 'Kapilvastu'),
				(59, 11, 'Nawalparasi'),
				(60, 11, 'Palpa'),
				(61, 11, 'Rupandehi'),
				(62, 12, 'Achham'),
				(63, 12, 'Bajhang'),
				(64, 12, 'Bajura'),
				(65, 12, 'Doti'),
				(66, 12, 'Kailali'),
				(67, 13, 'Banke'),
				(68, 13, 'Bardiya'),
				(69, 13, 'Dailekh'),
				(70, 13, 'Jajarkot'),
				(71, 13, 'Surkhet'),
				(72, 14, 'Baglung'),
				(73, 14, 'Mustang'),
				(74, 14, 'Myagdi'),
				(75, 14, 'Parbat')";
		$this->db->query($query);
	
	}
	
	public function InsertTimeZones(){
		$query = "INSERT INTO `f1_timezones` (`id`, `name`, `code`, `gmt_offset`, `dst_offset`) VALUES
			(65, ' [UTC - 12] Baker Island Time', ' [UTC - 12] Baker Island Time', -12.0000, -12.0000),
			(66, ' [UTC - 11] Niue Time, Samoa Standard Time', ' [UTC - 11] Niue Time, Samoa Standard Time', -11.0000, -11.0000),
			(67, ' [UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time', ' [UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time', -10.0000, -10.0000),
			(68, ' [UTC - 9:30] Marquesas Islands Time', ' [UTC - 9:30] Marquesas Islands Time', -9.5000, -9.5000),
			(69, ' [UTC - 9] Alaska Standard Time, Gambier Island Time', ' [UTC - 9] Alaska Standard Time, Gambier Island Time', -9.0000, -9.0000),
			(70, ' [UTC - 8] Pacific Standard Time', ' [UTC - 8] Pacific Standard Time', -8.0000, -8.0000),
			(71, ' [UTC - 7] Mountain Standard Time', ' [UTC - 7] Mountain Standard Time', -7.0000, -7.0000),
			(72, ' [UTC - 6] Central Standard Time', ' [UTC - 6] Central Standard Time', -6.0000, -6.0000),
			(73, ' [UTC - 5] Eastern Standard Time', ' [UTC - 5] Eastern Standard Time', -5.0000, -5.0000),
			(74, ' [UTC - 4:30] Venezuelan Standard Time', ' [UTC - 4:30] Venezuelan Standard Time', -4.5000, -4.5000),
			(75, ' [UTC - 4] Atlantic Standard Time', ' [UTC - 4] Atlantic Standard Time', -4.0000, -4.0000),
			(76, ' [UTC - 3:30] Newfoundland Standard Time', ' [UTC - 3:30] Newfoundland Standard Time', -3.5000, -3.5000),
			(77, ' [UTC - 3] Amazon Standard Time, Central Greenland Time', ' [UTC - 3] Amazon Standard Time, Central Greenland Time', -3.0000, -3.0000),
			(78, ' [UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time', ' [UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time', -2.0000, -2.0000),
			(79, ' [UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time', ' [UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time', -1.0000, -1.0000),
			(80, ' [UTC] Western European Time, Greenwich Mean Time', ' [UTC] Western European Time, Greenwich Mean Time', 0.0000, 0.0000),
			(81, ' [UTC + 1] Central European Time, West African Time', ' [UTC + 1] Central European Time, West African Time', 1.0000, 1.0000),
			(82, ' [UTC + 2] Eastern European Time, Central African Time', ' [UTC + 2] Eastern European Time, Central African Time', 2.0000, 2.0000),
			(83, ' [UTC + 3] Moscow Standard Time, Eastern African Time', ' [UTC + 3] Moscow Standard Time, Eastern African Time', 3.0000, 3.0000),
			(84, ' [UTC + 3:30] Iran Standard Time', ' [UTC + 3:30] Iran Standard Time', 3.5000, 3.5000),
			(85, ' [UTC + 4] Gulf Standard Time, Samara Standard Time', ' [UTC + 4] Gulf Standard Time, Samara Standard Time', 4.0000, 4.0000),
			(86, ' [UTC + 4:30] Afghanistan Time', ' [UTC + 4:30] Afghanistan Time', 4.5000, 4.5000),
			(87, ' [UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time', ' [UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time', 5.0000, 5.0000),
			(88, ' [UTC + 5:30] Indian Standard Time, Sri Lanka Time', ' [UTC + 5:30] Indian Standard Time, Sri Lanka Time', 5.5000, 5.5000),
			(89, ' [UTC + 5:45] Nepal Time', ' [UTC + 5:45] Nepal Time', 5.7500, 5.7500),
			(90, ' [UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time', ' [UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time', 6.0000, 6.0000),
			(91, ' [UTC + 6:30] Cocos Islands Time, Myanmar Time', ' [UTC + 6:30] Cocos Islands Time, Myanmar Time', 6.5000, 6.5000),
			(92, ' [UTC + 7] Indochina Time, Krasnoyarsk Standard Time', ' [UTC + 7] Indochina Time, Krasnoyarsk Standard Time', 7.0000, 7.0000),
			(93, ' [UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time', ' [UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time', 8.0000, 8.0000),
			(94, ' [UTC + 8:45] Southeastern Western Australia Standard Time', ' [UTC + 8:45] Southeastern Western Australia Standard Time', 8.7500, 8.7500),
			(95, ' [UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time', ' [UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time', 9.0000, 9.0000),
			(96, ' [UTC + 9:30] AInsertGroupPermissionsustralian Central Standard Time', ' [UTC + 9:30] Australian Central Standard Time', 9.5000, 9.5000),
			(97, ' [UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time', ' [UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time', 10.0000, 10.0000),
			(98, ' [UTC + 10:30] Lord Howe Standard Time', ' [UTC + 10:30] Lord Howe Standard Time', 10.5000, 10.5000),
			(99, ' [UTC + 11] Solomon Island Time, Magadan Standard Time', ' [UTC + 11] Solomon Island Time, Magadan Standard Time', 11.0000, 11.0000),
			(100, ' [UTC + 11:30] Norfolk Island Time', ' [UTC + 11:30] Norfolk Island Time', 11.5000, 11.5000),
			(101, ' [UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time', ' [UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time', 12.0000, 12.0000),
			(102, ' [UTC + 12:45] Chatham Islands Time', ' [UTC + 12:45] Chatham Islands Time', 12.7500, 12.7500),
			(103, ' [UTC + 13] Tonga Time, Phoenix Islands Time', ' [UTC + 13] Tonga Time, Phoenix Islands Time', 13.0000, 13.0000),
			(104, ' [UTC + 14] Line Island Time', ' [UTC + 14] Line Island Time', 14.0000, 14.0000)";
		$this->db->query($query);
	}
	
	public function InsertPrincipalAgents()
	{
	$query = "INSERT INTO `f1_agents` (`id`, `account_id`, `commission_account_id`, `city_id`, `timezone_id`, `name`, `slug`, `description`, `branch_code`, `address`, `phone`, `email`, `fax`, `registration_number`, `contact_person`, `dailyTxnLimit`, `monthlyTxnLimit`, `perTxnLimit`, `taxable`, `active`, `deleted`, `remit_destinations`, `payout_banks`, `reason`, `parentAgent_id`, `forexBatch_id`) VALUES
	(1,1,1,12,89,'Pashupati Remit','pashupati-remit','','001','Ktm','123456','sita@f1soft.com','123456','123456n','sita@f1soft.com',10000000,100000000,1000000,1,1,0,'a:3:{i:0;s:2:\"19\";i:1;s:3:\"103\";i:2;s:3:\"155\";}','a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";i:3;s:1:\"5\";}',NULL,NULL,9),
	(2,1,1,12,89,'Pashupati Remit','pashupati-remit-1','','001','Ktm','123456','sita@f1soft.com','123456','123456n','sita@f1soft.com',10000000,100000000,1000000,1,1,0,'a:3:{i:0;s:2:\"19\";i:1;s:3:\"103\";i:2;s:3:\"155\";}','a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"4\";i:3;s:1:\"5\";}',NULL,1,9)";
	$this->db->query($query);
	}
	
	public  function InsertUsers(){
	
		$query = "INSERT INTO `f1_users` (`id`, `city_id`, `id_type_id`, `groups_id`, `agent_id`, `username`, `password`, `firstname`, `middlename`, `lastname`, `usercode`, `address`, `phone`, `mobile`, `email`, `id_no`, `teller_id`, `active`, `created`, `deleted`, `api_key`, `first_login`, `pwd_change_on`, `last_logged`, `userlevel`) VALUES
		(1, 12, NULL, 1, 1, 'superadmin', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', '', 'admin', '001', 'ktm', NULL, '123456', 'sita@f1soft.com', 'N/A', NULL, 1, '2013-06-04 17:16:41', 0, 'X6nKrz', 0, '2013-06-04 17:16:41', '2014-03-13 09:48:00', NULL)";
		$this->db->query($query);
		
	}
	public function InsertGroups(){
				$query= "INSERT INTO `f1_groups` (`id`, `name`, `description`, `active`, `created`) VALUES
						(1, 'Super Admin', 'The Super Admin', 1, '2012-09-25 12:48:20'),
						(2, 'Principal Agent Admin', 'The admin of the Principal Agent', 1, '2012-09-25 12:50:33'),
						(3, 'Checker', 'The Subagent Admin/Checker', 1, '2012-09-25 12:52:10'),
						(4, 'Maker', 'The maker group', 1, '2012-09-25 12:54:26')";
				$this->db->query($query);
							
	}

	public function InsertLedgers(){
		       $pr_pool_ac  = self::PR_POOL_ACCOUNT;
		       $com_pool_ac = self::COMMISSION_POOL_ACCOUNT;
		       $balance     = self::BALANCE;
		        $query= "INSERT INTO `f1_ledgers` (`id`, `account_number`, `account_head`, `balance`) VALUES
						(1, '$pr_pool_ac', '$com_pool_ac', $balance)";
				$this->db->query($query);	
			
	}
		
	public function InsertDocumentTypes(){
		$query= "INSERT INTO `f1_idocuments` (`id`, `name`, `description`, `created`) VALUES
				(1, 'Citizenship', 'National Identification Card', '2013-06-04 17:07:50'),
				(2, 'Passport', 'International Identification Card', '2013-06-04 17:07:50')";
		$this->db->query($query);			
	}
	
	public function InsertOptions(){
		$admin_email = self::ADMIN_EMAIL;
		$technical_email = self::TECHNICAL_EMAIL;
		$query= "INSERT INTO `f1_options` (`id`, `option_name`, `option_value`, `autoload`) VALUES
				(1, 'config_adminemail', '$admin_email', 1),
				(2, 'config_technicalemail', '$technical_email', 1)";
		$this->db->query($query);
			
	}
	
	public function InsertBanks(){
		$bank_name = self::BANK_NAME;
		$swiftcode = self::SWIFT_CODE;
		$query= "INSERT INTO `f1_banks` (`id`, `city_id`, `name`, `swiftCode`, `address`) VALUES
					(1, 12, '$bank_name', '$swiftcode', 'test address, Kathmandu')";
		$this->db->query($query);			
	}
	
	public function InsertBankBranch(){
		$name = self::BANK_NAME;
		$branchcode = self::BRANCH_CODE;
		$query= "INSERT INTO `f1_bankbranches` (`id`, `bank_id`, `city_id`, `name`, `branchCode`, `address`, `active`) VALUES
		(1, 1, 12, '$name', '$branchcode', 'test address, Kathmandu', 1)";
		$this->db->query($query);
	}
	
}