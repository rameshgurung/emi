<?php

use Transborder\Transaction\RemitManager;

use Transborder\AMLParser\AMLManager;

use Transborder\Transaction\TransactionManager;

use Transborder\Transaction\PayoutManager;

use Transborder\Common\Util\TransborderEventListener;

use Transborder\Forex\ForexManager;

use Transborder\Commission\CommissionManager;

use Transborder\Transaction\TransactionNumberManager;

class Transborder{
	
	/**
	 * 
	 * The instance to Remit Manager
	 * @var $rm Transborder\Transaction\RemitManager
	 */
	public $rm;
	
	/**
	 * 
	 * The instance to the Payout Manager
	 * @var $pm Transborder\Transaction\PayoutManager
	 */
	public $pm;
	
	/**
	 * 
	 * @var Transborder\Commission\CommissionManager
	 */
	public $cm;
	
	/**
	 * 
	 * @var Transborder\Forex\ForexManager
	 */
	public $fm;
	
	/**
	 *
	 * @var Transborder\Transaction\TransactionManager
	 */
	public $tm;
	
	/**
	 * 
	 * @var Tranborder\AMLParser\AMLManager
	 */
	public $am;
	
	/**
	 *
	 * @var Transborder\Transaction\TransactionNumberManager
	 */
	public $nm;
	
	public function __construct(){
		$this->cm = new CommissionManager();
		
		$this->fm = new ForexManager();
		
		$this->pm = new PayoutManager();
		
		$this->rm = new RemitManager();
				
		$this->tm = new TransactionManager();
		
		$this->am = new AMLManager();
		
		$this->nm = new TransactionNumberManager();
		
		//forex update listener
		$forexUpdateListener = new Transborder\Forex\ForexEventListener();
		\CI::$APP->doctrine->em->getEventManager()->addEventSubscriber($forexUpdateListener);
		
		//agent update listener
		$agentEventListener = new Transborder\Agent\AgentEventListener();
		\CI::$APP->doctrine->em->getEventManager()->addEventSubscriber($agentEventListener);
		
		
		//transborder general event listener
		$tbeventlistener = new TransborderEventListener();
		\CI::$APP->doctrine->em->getEventManager()->addEventSubscriber($tbeventlistener);
		
		
		
		log_message('debug','Transborder Class initialized');
	}
}