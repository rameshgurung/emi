<?php

class MY_Log extends CI_Log{
	
	protected $_levels	= array('ERROR' => '1', 'INFO' => '2',  'DEBUG' => '3', 'ALL' => '4');
	
	public function __construct(){
		$config =& get_config();

		$this->_log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';

		if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path))
		{
			$this->_enabled = FALSE;
		}

		if (is_numeric($config['log_threshold']))
		{
			$this->_threshold = $config['log_threshold'];
		}

		if ($config['log_date_format'] != '')
		{
			$this->_date_fmt = $config['log_date_format'];
		}
	}
	
	
	public function write_log($level = 'error', $msg, $php_error = FALSE)
	{
		
		
		if ($this->_enabled === FALSE)
		{
			return FALSE;
		}
	
		$level = strtoupper($level);
	
		if ( ! isset($this->_levels[$level]) OR ($this->_levels[$level] > $this->_threshold))
		{
			return FALSE;
		}
	
		$filepath = $this->_log_path.'log-'.date('Y-m-d').EXT;
		$message  = '';
	
		if ( ! file_exists($filepath))
		{
			$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}
	
		if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
		{
			return FALSE;
		}
	
		// if(Current_User::user()){
			
		// 	CI::$APP->load->library('session');

		// 	if (is_numeric($main_user = CI::$APP->session->userdata('main_user')) and $main_user > 0)
		// 		$main_user = CI::$APP->doctrine->em->find('models\User', $main_user);
			
		// 	if ($main_user and !$main_user->isDeleted() and $main_user->isActive() 
		// 		and Current_User::user()->getUsername() != $main_user->getUsername()) {
					
		// 		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' ['.CI::$APP->input->ip_address().'] ['. $main_user->getUsername() .' as ' . Current_User::user()->getUsername() . '] '.' --> '.$msg."\n";
			
		// 	} else 
		// 		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' ['.CI::$APP->input->ip_address().'] ['.Current_User::user()->getUsername().'] '.' --> '.$msg."\n";
	
		// } else
		// 	$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.date($this->_date_fmt). ' ['.CI::$APP->input->ip_address().'] [Not Logged In] '.' --> '.$msg."\n";
	
		flock($fp, LOCK_EX);
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);
	
// 		error_log($message,3,'php://stdin');
		
// 		$data = json_encode(array(	'action'	=>	'pushlog',
// 									'data'		=>	$message));
		
// 		$client = new WebsocketClient();
// 		$client->connect('192.168.2.19', 8500, '/push','foo.lh');
// 		$client->sendData($data);
		
		@chmod($filepath, FILE_WRITE_MODE);
		return TRUE;
	}
}