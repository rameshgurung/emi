<?php
namespace models\Repository;
 

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use content\models\Content,
	F1soft\DoctrineExtensions\Paginate\Paginate,
	Doctrine\ORM\Query;
 
class RateRepository extends EntityRepository{
	
	
	public function getMin($param){
		$column=$param['column'];
		$qb = $this->_em->createQueryBuilder();
		$qb->select("min(r.$column)")
			->from('models\Rate', 'r')
			->where("r.rate!=''");
		$result=$qb->getQuery()->getScalarResult();
		return $ids = array_map('current', $result);
	}

	public function getMax($param){
		$column=$param['column'];
		$qb = $this->_em->createQueryBuilder();
		$qb->select("max(r.$column)")
			->from('models\Rate', 'r')
			->where("r.rate!=''");
		$result=$qb->getQuery()->getScalarResult();
		return $ids = array_map('current', $result);
	}

	
}