<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo,
Doctrine\Common\Collections\ArrayCollection; 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @ORM\Entity()
 * @ORM\Table(name="f1_installments")
 */
class Installment{
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
	private $id;

    /**
     * @ORM\ManyToOne(targetEntity="models\Discount")
     * @ORM\JoinColumn(name="premium_payment_mode_id", referencedColumnName="id", nullable=false)
     */
    private $premiumPaymentMode;


	/**
     * @ORM\Column(type="integer",nullable=false)
     */
	private $amount;

	/**
	 * @ORM\Column(type="decimal", precision=5, scale=2,nullable=false)
	 */
	private $interest;

	/**
     * @ORM\Column(type="integer",nullable=false)
	 */
	private $tenture;

	/**
     * @ORM\Column(type="integer",nullable=false)
	 */
	private $age;

   /**
    *@ORM\Column(name="loan_disbursement_date", type="datetime",nullable=false)
    */
   private $loanDispbursementDate;

   /**
    *@ORM\Column(name="emi_start_date", type="datetime",nullable=false)
    */
   private $emiStarDate;

	/**
	 * @ORM\Column(name="accidental_death_benefit",type="boolean")
	 */
	private $AccidentalDeathBenefit=0;
	
	/**
	 * @ORM\Column(name="total_permanent_disability_benefit",type="boolean")
	 */
	private $TotalPermanentDisabilityBenefit=0;


	/**
	 * @ORM\Column(name="premium_rate",type="decimal", precision=5, scale=2,nullable=false)
	 */
	private $premiumRate;

	/**
	 * @ORM\Column(name="large_sum_discount",type="decimal", precision=5, scale=2,nullable=false)
	 */
	private $largeSumDiscount;

	/**
	 * @ORM\Column(name="after_discount",type="decimal", precision=5, scale=2,nullable=false)
	 */
	private $afterDiscount;

	/**
	 * @ORM\Column(name="annual_premium",type="decimal", precision=20, scale=2,nullable=false)
	 */
	private $annualPremium;

    /**
     * @ORM\Column(name="discount_on_annual_payment",type="decimal", precision=20, scale=2,nullable=false)
     */
    private $discountOnAnnualPayment;

	/**
	 * @ORM\Column(name="premium_after_discount",type="decimal", precision=20, scale=2,nullable=false)
	 */
	private $permiumAfterDiscount;

    /**
     * @ORM\Column(name="less_corporate_discount",type="decimal", precision=20, scale=2,nullable=false)
     */
    private $lessCorporateDiscount;

	/**
	 * @ORM\Column(name="adb_amount",type="decimal", precision=20, scale=2)
	 */
	private $adbAmount;
    /**
     * @ORM\Column(name="tpdpw_amount",type="decimal", precision=20, scale=2)
     */
    private $tpdpwAmount;


    /**
     * @ORM\Column(name="monthly_premium_amount",type="decimal", precision=20, scale=2,nullable=false)
     */
    private $monthlyPremiumAmount;


    /**
     * @ORM\Column(name="t24_username",type="string", nullable=false)
     */
    private $t24Username;    

    /**
     * @ORM\Column(name="t24_password",type="string", nullable=false)
     */
    private $t24Password;    

    /**
     * @ORM\Column(name="branch_code",type="string", nullable=false)
     */
    private $branchCode;    
    /**
     * @ORM\Column(name="customer_indentification_number",type="string", nullable=false)
     */
    private $customerIdentificationNumber;    
    /**
     * @ORM\Column(name="loan_disbursement_account",type="string", nullable=false)
     */
    private $loanDisbursementAccount;    
    /**
     * @ORM\Column(name="principal_liquidation_account",type="string", nullable=false)
     */
    private $principalLiquidationAccount;    
    /**
     * @ORM\Column(name="interest_liquidation_account",type="string", nullable=false)
     */
    private $interestLiquidationAccount;    

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $installments;    

    /**
     * @ORM\Column(name="api_request_data",type="text", nullable=false)
     */
    private $apiRequestData;    

    /**
     * @ORM\Column(name="api_response_data",type="text", nullable=false)
     */
    private $apiResponseData;    

    /**
     * @ORM\Column(name="api_customer_no",type="string", nullable=false)
     */
    private $apiCustomerNo;    

   /**
    *@Gedmo\Timestampable(on="create")
    *@ORM\Column(name="created_at", type="datetime")
    */
   private $createdAt;

    /**
    **@Gedmo\Timestampable(on="update")
    *@ORM\Column(name="updated_at", type="datetime")
    */
    private $updatedAt;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function setPremiumPaymentMode(\models\Discount $premiumPaymentMode){

        $this->premiumPaymentMode = $premiumPaymentMode;
        return $this;
    }

    public function getPremiumPaymentMode(){

        return $this->premiumPaymentMode;
    }
    /**
     * Set amount
     *
     * @param integer $amount
     * @return Installment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set interest
     *
     * @param string $interest
     * @return Installment
     */
    public function setInterest($interest)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Get interest
     *
     * @return string 
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * Set tenture
     *
     * @param integer $tenture
     * @return Installment
     */
    public function setTenture($tenture)
    {
        $this->tenture = $tenture;

        return $this;
    }

    /**
     * Get tenture
     *
     * @return integer 
     */
    public function getTenture()
    {
        return $this->tenture;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return Installment
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set loanDispbursementDate
     *
     * @param \DateTime $loanDispbursementDate
     * @return Installment
     */
    public function setLoanDispbursementDate($loanDispbursementDate)
    {
        $this->loanDispbursementDate = $loanDispbursementDate;

        return $this;
    }

    /**
     * Get loanDispbursementDate
     *
     * @return \DateTime 
     */
    public function getLoanDispbursementDate()
    {
        return $this->loanDispbursementDate;
    }

    /**
     * Set emiStarDate
     *
     * @param \DateTime $emiStarDate
     * @return Installment
     */
    public function setEmiStarDate($emiStarDate)
    {
        $this->emiStarDate = $emiStarDate;

        return $this;
    }

    /**
     * Get emiStarDate
     *
     * @return \DateTime 
     */
    public function getEmiStarDate()
    {
        return $this->emiStarDate;
    }

    /**
     * Set AccidentalDeathBenefit
     *
     * @param boolean $accidentalDeathBenefit
     * @return Installment
     */
    public function setAccidentalDeathBenefit($accidentalDeathBenefit)
    {
        $this->AccidentalDeathBenefit = $accidentalDeathBenefit;

        return $this;
    }

    /**
     * Get AccidentalDeathBenefit
     *
     * @return boolean 
     */
    public function getAccidentalDeathBenefit()
    {
        return $this->AccidentalDeathBenefit;
    }

    /**
     * Set TotalPermanentDisabilityBenefit
     *
     * @param boolean $totalPermanentDisabilityBenefit
     * @return Installment
     */
    public function setTotalPermanentDisabilityBenefit($totalPermanentDisabilityBenefit)
    {
        $this->TotalPermanentDisabilityBenefit = $totalPermanentDisabilityBenefit;

        return $this;
    }

    /**
     * Get TotalPermanentDisabilityBenefit
     *
     * @return boolean 
     */
    public function getTotalPermanentDisabilityBenefit()
    {
        return $this->TotalPermanentDisabilityBenefit;
    }

    /**
     * Set premiumRate
     *
     * @param string $premiumRate
     * @return Installment
     */
    public function setPremiumRate($premiumRate)
    {
        $this->premiumRate = $premiumRate;

        return $this;
    }

    /**
     * Get premiumRate
     *
     * @return string 
     */
    public function getPremiumRate()
    {
        return $this->premiumRate;
    }

    /**
     * Set largeSumDiscount
     *
     * @param string $largeSumDiscount
     * @return Installment
     */
    public function setLargeSumDiscount($largeSumDiscount)
    {
        $this->largeSumDiscount = $largeSumDiscount;

        return $this;
    }

    /**
     * Get largeSumDiscount
     *
     * @return string 
     */
    public function getLargeSumDiscount()
    {
        return $this->largeSumDiscount;
    }

    /**
     * Set afterDiscount
     *
     * @param string $afterDiscount
     * @return Installment
     */
    public function setAfterDiscount($afterDiscount)
    {
        $this->afterDiscount = $afterDiscount;

        return $this;
    }

    /**
     * Get afterDiscount
     *
     * @return string 
     */
    public function getAfterDiscount()
    {
        return $this->afterDiscount;
    }

    /**
     * Set annualPremium
     *
     * @param string $annualPremium
     * @return Installment
     */
    public function setAnnualPremium($annualPremium)
    {
        $this->annualPremium = $annualPremium;

        return $this;
    }

    /**
     * Get annualPremium
     *
     * @return string 
     */
    public function getAnnualPremium()
    {
        return $this->annualPremium;
    }

    /**
     * Set discountOnAnnualPayment
     *
     * @param string $discountOnAnnualPayment
     * @return Installment
     */
    public function setDiscountOnAnnualPayment($discountOnAnnualPayment)
    {
        $this->discountOnAnnualPayment = $discountOnAnnualPayment;

        return $this;
    }

    /**
     * Get discountOnAnnualPayment
     *
     * @return string 
     */
    public function getDiscountOnAnnualPayment()
    {
        return $this->discountOnAnnualPayment;
    }

    /**
     * Set permiumAfterDiscount
     *
     * @param string $permiumAfterDiscount
     * @return Installment
     */
    public function setPermiumAfterDiscount($permiumAfterDiscount)
    {
        $this->permiumAfterDiscount = $permiumAfterDiscount;

        return $this;
    }

    /**
     * Get permiumAfterDiscount
     *
     * @return string 
     */
    public function getPermiumAfterDiscount()
    {
        return $this->permiumAfterDiscount;
    }

    /**
     * Set lessCorporateDiscount
     *
     * @param string $lessCorporateDiscount
     * @return Installment
     */
    public function setLessCorporateDiscount($lessCorporateDiscount)
    {
        $this->lessCorporateDiscount = $lessCorporateDiscount;

        return $this;
    }

    /**
     * Get lessCorporateDiscount
     *
     * @return string 
     */
    public function getLessCorporateDiscount()
    {
        return $this->lessCorporateDiscount;
    }

    /**
     * Set adbAmount
     *
     * @param string $adbAmount
     * @return Installment
     */
    public function setAdbAmount($adbAmount)
    {
        $this->adbAmount = $adbAmount;

        return $this;
    }

    /**
     * Get adbAmount
     *
     * @return string 
     */
    public function getAdbAmount()
    {
        return $this->adbAmount;
    }

    /**
     * Set tpdpwAmount
     *
     * @param string $tpdpwAmount
     * @return Installment
     */
    public function setTpdpwAmount($tpdpwAmount)
    {
        $this->tpdpwAmount = $tpdpwAmount;

        return $this;
    }

    /**
     * Get tpdpwAmount
     *
     * @return string 
     */
    public function getTpdpwAmount()
    {
        return $this->tpdpwAmount;
    }

    /**
     * Set monthlyPremiumAmount
     *
     * @param string $monthlyPremiumAmount
     * @return Installment
     */
    public function setMonthlyPremiumAmount($monthlyPremiumAmount)
    {
        $this->monthlyPremiumAmount = $monthlyPremiumAmount;

        return $this;
    }

    /**
     * Get monthlyPremiumAmount
     *
     * @return string 
     */
    public function getMonthlyPremiumAmount()
    {
        return $this->monthlyPremiumAmount;
    }

    /**
     * Set t24Username
     *
     * @param string $t24Username
     * @return Installment
     */
    public function setT24Username($t24Username)
    {
        $this->t24Username = $t24Username;

        return $this;
    }

    /**
     * Get t24Username
     *
     * @return string 
     */
    public function getT24Username()
    {
        return $this->t24Username;
    }

    /**
     * Set t24Password
     *
     * @param string $t24Password
     * @return Installment
     */
    public function setT24Password($t24Password)
    {
        $this->t24Password = $t24Password;

        return $this;
    }

    /**
     * Get t24Password
     *
     * @return string 
     */
    public function getT24Password()
    {
        return $this->t24Password;
    }

    /**
     * Set branchCode
     *
     * @param string $branchCode
     * @return Installment
     */
    public function setBranchCode($branchCode)
    {
        $this->branchCode = $branchCode;

        return $this;
    }

    /**
     * Get branchCode
     *
     * @return string 
     */
    public function getBranchCode()
    {
        return $this->branchCode;
    }

    /**
     * Set customerIdentificationNumber
     *
     * @param string $customerIdentificationNumber
     * @return Installment
     */
    public function setCustomerIdentificationNumber($customerIdentificationNumber)
    {
        $this->customerIdentificationNumber = $customerIdentificationNumber;

        return $this;
    }

    /**
     * Get customerIdentificationNumber
     *
     * @return string 
     */
    public function getCustomerIdentificationNumber()
    {
        return $this->customerIdentificationNumber;
    }

    /**
     * Set loanDisbursementAccount
     *
     * @param string $loanDisbursementAccount
     * @return Installment
     */
    public function setLoanDisbursementAccount($loanDisbursementAccount)
    {
        $this->loanDisbursementAccount = $loanDisbursementAccount;

        return $this;
    }

    /**
     * Get loanDisbursementAccount
     *
     * @return string 
     */
    public function getLoanDisbursementAccount()
    {
        return $this->loanDisbursementAccount;
    }



    public function setPrincipalLiquidationAccount($principalLiquidationAccount)
    {
        $this->principalLiquidationAccount = $principalLiquidationAccount;

        return $this;
    }

    public function getPrincipalLiquidationAccount()
    {
        return $this->principalLiquidationAccount;
    }
    /**
     * Set interestLiquidationAccount
     *
     * @param string $interestLiquidationAccount
     * @return Installment
     */
    public function setInterestLiquidationAccount($interestLiquidationAccount)
    {
        $this->interestLiquidationAccount = $interestLiquidationAccount;

        return $this;
    }

    /**
     * Get interestLiquidationAccount
     *
     * @return string 
     */
    public function getInterestLiquidationAccount()
    {
        return $this->interestLiquidationAccount;
    }

    /**
     * Set installments
     *
     * @param string $installments
     * @return Installment
     */
    public function setInstallments($installments)
    {
        $this->installments = serialize($installments);

        return $this;
    }

    /**
     * Get installments
     *
     * @return string 
     */
    public function getInstallments()
    {
        return $this->installments;
    }


    public function setApiRequestData($apiRequestData)
    {
        $this->apiRequestData = serialize($apiRequestData);
        return $this;
    }
    public function getApiRequestData()
    {
        return $this->apiRequestData;
    }
    public function setApiResponseData($apiResponseData)
    {
        $this->apiResponseData = serialize($apiResponseData);
        return $this;
    }
    public function getApiResponseData()
    {
        return $this->apiResponseData;
    }

    public function setApiCustomerNo($apiCustomerNo)
    {
        $this->apiCustomerNo = $apiCustomerNo;
        return $this;
    }
    public function getApiCustomerNo()
    {
        return $this->apiCustomerNo;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Installment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Installment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
