<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo,
	Doctrine\Common\Collections\ArrayCollection; 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @ORM\Entity(repositoryClass="models\Repository\RateRepository")
 * @ORM\Table(name="f1_rate")
 */
class Rate{
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $age;    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rate;


	public function id()
	{
	    return $this->id;
	}

	public function getAge()
	{
	    return $this->age;
	}

	public function setAge($age)
	{
	    $this->age = $age;
	}

	public function getYear()
	{
	    return $this->year;
	}

	public function setYear($year)
	{
	    $this->year = $year;
	}

	public function getRate()
	{
	    return $this->rate;
	}

	public function setRate($rate)
	{
	    $this->rate = $rate;
	}

}