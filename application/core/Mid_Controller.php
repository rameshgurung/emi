<?php
class Mid_Controller extends My_Controller{

	protected $templatedata = array();
	protected $_t = [];

	public function __construct(){
		parent::__construct();

		System::init();
		
		$this->load->library('breadcrumb');

		$this->load->library('session');

		$this->templatedata['_CONFIG']	= $this->_CONFIG;
		$this->templatedata['flashdata'] = $this->session->flashdata('feedback');
		
		$this->templatedata['scripts'] = array();
		$this->templatedata['stylesheets'] = array();

			if(\Current_User::user() == NULL and \Current_User::nonClient() == NULL )
			{
				if(!$this->session->userdata('search_parameters'))
					redirect('public');
			}

		//check for any critical messages
		$_critical_messages = $this->message->get('alert','critical');

		if(count($_critical_messages) > 0 ){
				$this->templatedata['critical_alerts'] = $_critical_messages;
		}
		
		$_feedbacks = $this->message->get(FALSE,'feedback');


		if(count($_feedbacks) > 0){
			$this->templatedata['feedback'] = $_feedbacks;
		}

	}

	public function index(){
		
	}



}