<?php
class Logger {

	public function __construct(){

	}
	/*
	*@param 
	*@args => $logBy = log creator option client object or non client object
	*@args => $logKey = log type eg notice added, notice updated
	*@arg => $logValue = description of log
	*/
	public function log($logBy, $logKey, $logValue){
		
		try{
			$logger  = new models\ActivityLog();
			
			if($logBy instanceof \models\User){
				$logger->setUser($logBy);
			}
			if($logBy instanceOf \models\NonClient){
				$logger->setNonClient($logBy);
			}
			
			$logger->setKey($logKey);

			$logger->setValue($logValue);

			$this->doctrine->em->persist($logger);
			$this->doctrine->em->flush();

		}catch(Exception $ex){
			throw new Exception($ex->getMessage(), 1);
		}
	}

	public function get($logKey){
		$result =  $this->doctrine->em->getRepositoty('models\ActivityLog')
								 	  ->findBy(array('logKey'=>$logKey));
		return $result;
	}
}