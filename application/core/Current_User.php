<?php

use models\User\Group;

use models\User,
Doctrine\Common\Util\Debug;

class Current_User {
	
	/**
	 * 
	 * @var models\User
	 */
	private static $user;
	
	private static $permissions = array();
	
	private static $remitCountry;

	private $nc;

	private function __construct() {}

	public static function user() {
		
		if(!isset(self::$user)) {
			$CI =& get_instance();
			$CI->load->library('session');
			
			if (!$user_id = $CI->session->userdata('user_id')) {
				return FALSE;
			}
			
			$user = \CI::$APP->doctrine->em->find('models\User',$user_id);

			if(!$user)
				return FALSE;
			
			self::$user =& $user;
			
		}

		return self::$user;
	}

	public static function nonClient(){
		
		$CI =& get_instance();
		$CI->load->library('session'); 

		$non_client_id = $CI->session->userdata('non_client_id');
		if(!$non_client_id) return false;
		
		$nonClientRepo = $CI->doctrine->em->getRepository('\models\NonClient');
		$nonClient = $nonClientRepo->findOneBy(array('id'=>$non_client_id));
		
		if(!$nonClient) return false;
		return $nonClient;
	}

	public static function login($username, $password) {


		
		$CI =& get_instance();
		
		$query = $CI->doctrine->em->createQuery("SELECT u FROM models\User u WHERE u.username = :username");
		$query->setParameter(':username', $username);
		$user = $query->getResult();
		
		
		if($user)
		{
			$user = $user[0];
			switch ($user->getStatus()) {
					case \models\User::STATUS_PENDING:{
						$CI ->message->set("The client application is not approved yet, Please contact Registry Administrator","error", true, 'feedback' );
						redirect('public');
						break;
					}					
					default:{
						break;
					}
				}
			if($user->isDeleted() || $user->isActive()==FALSE || $user->getStatus() != models\User::STATUS_ACTIVE){
				return FALSE;
			}
			/*if($user->getLedger()){
				if( $user->getLedger()->getStatus() != \models\Ledger::STATUS_ACTIVE )
					return false;
			}*/
			
			$accoutType = $user->getAccType()->getType();
			
			//if user is of subuser type his parent should be ok for him to let logged in	
			switch ($accoutType) {
				case 'SUB_USER':
				$parent = $user->getCreatedBy();
				if($parent != ''){
					if($parent->getStatus() != \models\User::STATUS_ACTIVE ){
						return false;
					}
				}

				if($user->getLedger()){
					if( $user->getLedger()->getStatus() != \models\Ledger::STATUS_ACTIVE )
						return false;
				}
				break;
				
				default:
					# code...
				break;
			}

			if($user->getPassword() == md5($password))
			{
				
				$CI->load->library('session');
				$CI->session->set_userdata('user_id',$user->id());

				$CI->session->set_userdata('web_session', ( new \App\NumberManager\NumberContext("WEBSESSION") )->getNumber() ) ;
				self::$user = $user;

				// if($user->getLedger()){
				// 	if($user->getLedger()->getIsUsed() != TRUE){
				// 		$user->getLedger()->setIsUsed(TRUE);
				// 		$CI->doctrine->em->persist($user);
				// 		$CI->doctrine->em->flush();
				// 		return true;
				// 	}
				// }

				//call the post_user_login hook
				\Events::trigger('post_user_login',self::$user);
				
				return TRUE;
				
			}/**/
		}
		
		return FALSE;

	}
	
	public function switchto($user_id) {

		$CI =& get_instance();
		$CI->load->library('session');

		if (is_numeric($main_user = $CI->session->userdata('main_user')) and $main_user > 0) return FALSE;

		$CI->session->set_userdata('main_user', self::$user->id());
		$CI->session->set_userdata('user_id', $user_id);
		if ($CI->session->userdata('user_id') == $user_id) return TRUE;
		else return FALSE;
	}
	
	public static function can($seek_permission) {
		
		$CI =& get_instance();
		$CI->load->library('session');
		
		if (!$user_id = $CI->session->userdata('user_id')) {
			return FALSE;
		}
		

		$given_permissions = array();		

		// get user group and check if permission is assigned

		$group = self::user()->getGroup();

		foreach($group->getPermissions() as $rp){
			$p = strtolower(trim($rp->getName()));
			$given_permissions[$p] = TRUE;
		}
		//if user Account Type is sub user check for user permission
		// if($user->getAccType()->getType() == "SUB_USER"){
		// 	foreach ($user->getPermissions() as $perm) {
		// 		$p = strtolower(trim($perm->getName()));
		// 		$given_permissions[$p] = TRUE;
		// 	}
		// }else{
		// 	$group = self::user()->getGroup();
		// 	foreach($group->getPermissions() as $rp){
		// 		$p = strtolower(trim($rp->getName()));
		// 		$given_permissions[$p] = TRUE;
		// 	}
		// }
		
		if (is_array($seek_permission)) {
			
			foreach ($seek_permission as $p) {
				
				if (isset($given_permissions[strtolower(trim($p))])) return TRUE;	
				
			}	
			
		} else {
			
			if (isset($given_permissions[strtolower(trim($seek_permission))])) return TRUE;		
			
		}
		
		return FALSE;
		

	}

	public function isAlreadyLogged($user_id=NULL){
		
		if (!isset($user_id)) $user_id = self::user()->id();
		
		$CI =& get_instance();
		$CI->load->library('session');

		$CI->load->database();

		$sesstimeout = time() - $CI->config->item('sess_time_to_update');
		$sessID = $CI->session->userdata('session_id');
		$delim_str = 's:7:"user_id";i:'.$user_id.';';
		$ip = $CI->input->ip_address();
		$ua = $CI->input->user_agent();
		
		$query = $this->db->query(
			" SELECT session_id, ip_address, user_agent FROM f1_sessions 
			WHERE last_activity > $sesstimeout 
			AND user_data LIKE '%".$delim_str."%' 
			AND session_id != '$sessID' " 
			);
		
		if ($query->num_rows() > 0) {
			
			$result = $query->row_array();
			
			if ($result['ip_address'] == $ip && $result['user_agent'] == trim(substr($ua, 0, 50))) {
				
				$this->db->where('session_id', $result['session_id']);
				$this->db->update($CI->config->item('sess_table_name'), array('user_data' => ''));
				return FALSE;
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function setUser($user)
	{
		self::$user = $user;
	}
	
	public static function isSuperUser(){
		if(self::nonClient()) return false;
		return (self::user()->id() == 1 ||  self::user()->getGroup()->id() ==  Group::SUPER_ADMIN);
		
	}

	/**
	 * checks if currentUser can access targetUser based on group herirarchy and user level 
	 * @param $targetUser | userID or userObject
	 * @return bool 
	 */
	public static function canActOn($targetUser = NULL){
		
		if (! $targetUser or ! self::$user) return FALSE;
		
		if ( is_numeric($targetUser) ) {
			$targetUser = CI::$APP->doctrine->em->find('models\User', $targetUser);
		}
		
		if (! is_object($targetUser)) return FALSE;
		
		$targetUserGroup  = $targetUser->getGroup()->id();
		$currentUserGroup = self::$user->getGroup()->id();
		
		if ($currentUserGroup == Group::SUPER_ADMIN) return TRUE;
		if ($targetUserGroup  == Group::SUPER_ADMIN) return FALSE;
		if ($targetUserLevel  == User::USERLEVEL_MTO 
			and $targetUserGroup == Group::SUPER_AGENT_ADMIN) return FALSE;

			if (self::isMTOAdmin()) return TRUE;
		
		if ($targetUserLevel == User::USERLEVEL_MTO) return FALSE;
		
		if (self::isMTOUser()) return TRUE;
		
		if ($targetUserGroup == Group::SUPER_AGENT_ADMIN) return FALSE;
		
		$targetAgent =  $targetUser->getAgent();
		$currentAgent =  self::$user->getAgent();
		
		if ($currentUserGroup == Group::SUPER_AGENT_ADMIN) {
			
			$targetAgentID = $targetAgent->id();
			if ($targetAgent = $targetAgent->getParentAgent()) 
				$targetAgentID = $targetAgent->id();
			
			$currentAgentID = $currentAgent->id();
			if ($currentAgent = $currentAgent->getParentAgent()) 
				$currentAgentID = $currentAgent->id();
			
			return ($currentAgentID == $targetAgentID);
		}
		
		if ($targetUserGroup == Group::SUB_AGENT_ADMIN) return FALSE;
		
		if ($currentUserGroup == Group::SUB_AGENT_ADMIN) {
			return ($currentAgent->id() == $targetAgent->id());
		}
		
		return FALSE;
	}
	
	public static function isPrivilegedUser(){
		return ( self::isMTOUser() or self::isSuperUser());  
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	
}
