<?php

function adjustInstallmentAmount($installment, $amount, $startDate,$emiStartDate, $totalMonths, $rate)
{
	$outstandingPrincipal = $amount;
	$dueInterest = 0;
	$dueDate = clone $startDate;
	$years = $totalMonths/12;

	for($i = 1; $i <= $totalMonths; $i++) {

		if($i==1){
			$thisDate = clone $emiStartDate;
		}
		else{
			$thisDate = clone $dueDate;

			$thisDate->add(new \DateInterval("P1M"));

		}

		$interval = $dueDate->diff($thisDate, true)->days;
		$dueDate = $thisDate;

		

		

		$actualInterest = (($outstandingPrincipal * $rate)/365 ) * $interval;
		$dueInterest += $actualInterest;

		$payment = $installment;

		if($i % 12 == 0) {
			$payment += round(0.04 * $amount, 2);
		}

		if($i == $totalMonths) {
			$payment += round((1 - (0.04 * $years )) * $amount,2);
		}

		$principal=0;
		if($payment < $dueInterest) {
			$dueInterest -= $payment;
			$interestRecovered = $payment;
		}else{
			$principal = $payment - $dueInterest;
			$outstandingPrincipal -= $principal;
			$dueInterest = 0;
			$interestRecovered = $actualInterest;
		}
		$installments[]=array(
			'installment_number'=>$i,
			'due_date'=>$dueDate->format("Y-m-d"),
			'principal'=>round($principal,2),
			'outstanding_principal'=>round($outstandingPrincipal,2),
			'actual_interest'=>round($actualInterest,2),
			'interest_recovered'=>round($interestRecovered,2),
			'installment'=>round($payment,2),
			'outstanding_interest'=>round($dueInterest,2),
			);
	}

	return $installments;
}



?>