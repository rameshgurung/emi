<?php
$groupRepo = $this->doctrine->em->getRepository('models\User\Group');
$groups = $groupRepo->getGroupList();
$selected_group = isset($filters['group'])? $filters['group'] : '';
$selected_status = isset($filters['status'])? $filters['status'] : '';
$typed_f_name = isset($filters['first_name'])? $filters['first_name'] : '';
$typed_l_name = isset($filters['last_name'])? $filters['last_name'] : '';
$typed_u_name = isset($filters['user_name'])? $filters['user_name'] : '';
$typed_acc_no = isset($filters['acc_no'])? $filters['acc_no'] : '';

?>

<div class="grid_12">
	<div class="section">
	<?php if (user_access(array('add user'))) { ?>	
		<div class="controls">
			<a href="<?php echo base_url()."user/add" ?>" class="button">Add Admin</a>
		</div>
	<?php } ?>	
		<form name="user_filter" method="get" action="">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th colspan="8" style="text-align:left;">Filter Users By</th>
				</tr>
				<tr>	
					<td>Group</td>
					<td>Status</td>
					<td>Username</td>
				</tr>
				<tr>	
					<td>
						<select name="group" id="group">
							<?php if(count($groups)>1){?><option value="">--Select Group--</option><?php }?>
							<?php foreach ($groups as $g){?>
							<?php $selected = ($selected_group == $g['group_id']) ? 'selected="selected"' : '' ?>
							<option <?php echo $selected; ?> value="<?php echo $g['group_id'];?>"><?php echo $g['name'];?></option>
							<?php }?>
						</select>
					</td>
					<td>
						<?php 
						$s = array(''=>'--Select Status--');
						$status = \models\User::$status_types;
						$status = array_merge($s,$status);
				//echo form_dropdown('status', $status, isset($filters['status'])? $status : '', 'id="status"');
						echo form_dropdown('status', $status, $selected_status, 'id="status"');
						?>

					</td>
					<td><input name="user_name" type="text" placeholder="Username" id="user_name" value="<?php echo $typed_u_name?>"/></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Account No.</td>
				</tr>
				<tr>
					<td><input name="first_name" type="text" placeholder="First Name" size="12" id="first_name" value="<?php echo $typed_f_name?>"/></td>
					<td><input name="last_name" type="text" placeholder="Last	 Name" id="last_name" size="12" value="<?php echo $typed_l_name?>"/></td>
					<td><input name="acc_no" type="text" placeholder="Account No" id="acc_no" value="<?php echo $typed_acc_no?>"/></td>
				</tr>
				<tr>
					<td colspan="7">
						<input type="submit" value="Filter" name="filter_btn"/>
						<input type="reset" value="Clear" name="clear_btn" id="clear"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<div class="grid_12">
	<h2>Listing Clients / Gov. / Reg. Admin </h2>
	<div class="section">
		<table border=0 cellspacing=1 cellpadding=1>
			<tr>
				<th>S.N.</th>
				<th>Username</th>
				<th>Name</th>
				<th>Account Name</th>
				<th>Phone</th>
				<th>Group</th>
				<th>Account Type</th>
				<th>Account Number</th>
				<th>Status</th>
				<th>Created By</th>
				<th>Created At</th>
				<th>Actions</th>
			</tr>
			<?php
			$count = isset($offset)?$offset+1:1; 
			foreach($users as $u){?>
			<tr 
			<?php echo $u->getStatus()== \models\User::STATUS_BLOCK?"class='blocked'":''; ?>
			<?php echo (user_access('administer user') && $u->id()==Current_User::user()->id())?"class='current_user'":''; ?>
			>
			<td><?php echo $count; ?></td>
			<td><?php echo $u->getUsername(); ?></td>
			<td><?php echo $u->getName(); ?></td> 
			<td><?php echo $u->getAccName(); ?></td>
			<td><?php echo $u->getPhone(); ?></td>
			<td><?php echo ($u->getGroup())?$u->getGroup()->getName():'--'; ?></td>
			<td><?php echo $u->getAccType() ? $u->getAccType()->getDisplayName() : '--'; ?></td>
			<td><?php echo $u->getAccNumber()?:"--"; ?></td>
			<td><?php echo models\User::$status_types[$u->getStatus()];?></td>
			<td><?php echo ($u->getCreatedBy()) ? $u->getCreatedBy()->getUsername() : '--'; ?></td>
			<td><?php echo $u->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
			<td class="actions">
				<?php 
				$title = "----";
				?>
					<?php 
					if(user_access('administer user') && $u->id() != 1 && $u->getAccType()->getType() != "SUB_USER"){
     					//ensure that the user is not sub user
						if($u->id()==Current_User::user()->id()){
							echo "This is You";
							continue;
						}
						echo action_button('view','user/viewDetail/'.$u->getUsername(),array('title'  =>  'View Details '.$u->getUsername()));
						switch($u->getStatus()){
							case models\User::STATUS_PENDING:{
								/*if(user_access('edit user'))
									echo action_button('edit','user/SubUser/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));					
								*/
/*								if(user_access('approve user'))
									echo action_button('approve','user/approve/'.$u->getUsername(),array('title'	=>	'Approve '.$u->getUsername()));
*/								break;	
							}	
							case models\User::STATUS_ACTIVE:{
								if(user_access('edit user'))
									echo action_button('edit','user/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
								if(user_access('block user'))
									echo action_button('block','user/block/'.$u->getUsername(),array('title'	=>	'Block '.$u->getUsername()));
								if(Current_User::user()->id() == 1){
									//only superadmin will have access to swtich user
									echo action_button('person','auth/switchuser/'.$u->getUsername(),array('title'  =>  'Run CIBN as '.$u->getUsername()));
								}
								break;	
							}
							case models\User::STATUS_BLOCK:{
								if(user_access('edit user'))
									echo action_button('edit','user/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
								/*if(user_access('unblock user'))
									echo action_button('unblock','user/unblock/'.$u->getUsername(),array('title'  =>  'Unblock '.$u->getUsername()));*/
								break;	
							}
						}
						if(user_access('delete user'))
							echo action_button('delete','user/delete/'.$u->getUsername(),array('title'  =>  'Delete '.$u->getUsername()));
					}
					else
					{
						if($u->id()==Current_User::user()->id()){
							echo action_button('edit','user/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
						}
					}
					?>
					</td>
		</tr>
	<?php $count++;}?>
</table>
<?php if(isset($pagination)):?>
	<div class="tablefooter">
		<div class="actions">
			&nbsp;
		</div>
		<div class="pagination">
			<?php echo $pagination; ?>
		</div>
		<div class="clear"></div>
	</div>
<?php endif;?>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#clear').click(function(){
			$('#first_name,#last_name,#user_name,#acc_no').attr("value",'');
			$('#status,#group').val('');
			return false;
		});
	});
</script>


