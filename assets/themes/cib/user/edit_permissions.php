<div class="grid_12">
	<h2>Edit Permissions [ <?php echo $user->getName();?> ]</h2>
	<div class="section">
		<form name="transborder_group_permissions" method="post" action="" id="permission_form">
		<table border="0px" cellspacing=1 cellpadding=1 >
        	<tr>
        		<th width="3%"><input type="checkbox" id="select_all" name="select_all" title="select all" /></th>
        		<th>Permission</th>
        		<th>Description</th>
        	</tr>
			
			<?php 
			// show_pre($group_permissions_array);
			foreach($group_permissions as $gpk=>$gp){
					$checked = "";
					foreach($user_permissions as $up){ 
						if($up->id() == $gp->id())$checked = "checked='checked'";
					}
				?>

				<tr>
					<td><input <?php echo $checked; ?> type="checkbox" class="permissions" id="perm[<?php echo $gp->id() ?>]" name="perm[<?php echo $gp->id() ?>]"></td>
					<td><?php echo $gp->getName();?></td>
					<td><?php echo ($gp->getDescription() != 'Array') ? $gp->getDescription() : getDescriptionFromPermissionArray($all_permissions,$gp); ?></td>
				</tr>
			<?php } ?>
			
			
        </table>
        <div class="tablefooter">
        	<div class="actions">
        		<input type="submit" class="button small save-perms" value="Save Permissions" name="save_perms"/>
        		<input type="reset" class="button small" value="Reset" />
				<a href="<?php echo site_url('user/group')?>" class="button">Cancel</a>
			</div>
        			
			<div class="clear"></div>
        </div>
        </form>


        <?php 
/*        show_pre($all_permissions);
        foreach($group_permissions as $k=>$permission_name)
        {
        	echo "<li>".$k.'----'.$permission_name;
        	foreach($all_permissions as $permissions)
	        	echo $chk = (in_array($permission_name,$permissions)) ? "show" : "";
        	echo "</li>";
        } 
*/        ?>




	</div>
</div>

<script>

$(document).ready(function(e){
	$("#select_all").click(function(e){
		var select_all = $(this).attr("checked");
		
		if(select_all){
			$(".permissions").each(function(){
				$(this).attr("checked","checked");
			});
		}else{
			$(".permissions").each(function(){
				$(this).removeAttr("checked");
			});
		}
	});
});
</script>