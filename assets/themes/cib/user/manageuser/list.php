<div class="grid_12">
	
	<h2>Manage Sub Users</h2>
	
	<?php if(Current_User::user()->getGroup()->id()!=1) {?>
		<div class="controls">
			<a href="<?php echo base_url()."user/SubUser/add" ?>" class="button">Add User</a>
		</div>
		<?php } ?>

		<div class="section">
			<table border=0 cellspacing=1 cellpadding=1>
				<tr>
					<th>S.No</th>
					<th>Username</th>
					<th>Name</th>
					<th>Group / Subgroup</th>
					<th>Account Type</th>
					<th>Phone</th>
					<th>Account Number</th>
					<th>Status </th>
					<th>Created By</th>
					<th>Created At</th>
					<th>Actions</th>
				</tr>

				<?php
				$count=isset($offset)?$offset:0;
				foreach($users as $u):$count++
					?>
				<tr 
				<?php echo $u->getStatus()== \models\User::STATUS_BLOCK?"class='blocked'":''; ?>
				<?php echo (user_access('manage sub users') && $u->isActive() && Current_User::user()->id()==$u->id())?"class='current_user'":''; ?>
				>
				<td><?php echo $count; ?></td>
				<td><?php echo $u->getUsername(); ?></td>
				<td><?php echo $u->getName(); ?></td>
				<td><?php echo $u->getGroup()?$u->getGroup()->getName():''; ?></td>
				<td><?php echo $u->getAccType()->getDisplayName(); ?></td>
				<td><?php echo $u->getPhone(); ?></td>
				<td><?php echo $u->getAccNumber()?:'--'; ?></td>
				<td><?php echo \models\User::$status_types[$u->getStatus()]; ?></td>
				<td><?php echo $u->getCreatedBy() ? $u->getCreatedBy()->getName() : '---'; ?></td>
				<td><?php echo $u->getCreatedAt()->format('Y-m-d H:i:s'); ?></td>
				<td class="actions">
					<?php 
					$title = "----";
					?>
					<?php 
					if( user_access('manage sub users')){	
						if(Current_User::user()->id()==1 and Current_User::user()->id()==$u->id()){
							//superadmin case
							echo "This is You";
						}
						else if(Current_User::user()->id()==$u->id()){
							echo action_button('edit','user/SubUser/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
							echo "This Is You";
						}
						else{
							switch($u->getStatus()){
								case models\User::STATUS_PENDING:{
									echo action_button('edit','user/SubUser/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
									break;	
								}	
								case models\User::STATUS_ACTIVE:{
									echo action_button('edit','user/SubUser/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
									echo action_button('block','user/SubUser/block/'.$u->getUsername(),array('title'	=>	'Block '.$u->getUsername()));
									break;	
								}
								case models\User::STATUS_BLOCK:{
									echo action_button('edit','user/SubUser/edit/'.$u->getUsername(),array('title'	=>	'Edit '.$u->getUsername()));
									break;	
								}
								case models\User::STATUS_DELETE:{
									break;	
								}
							}
							echo action_button('delete','user/SubUser/delete/'.$u->getUsername(),array('title'  =>  'Delete '.$u->getUsername()));							
						}
					}
					?>
				</td>
			</tr>

		<?php endforeach;?>
	</table>
	<?php if(isset($pagination)):?>
		<div class="tablefooter">
			<div class="actions">
				&nbsp;
			</div>
			<div class="pagination">
				<?php echo $pagination; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php endif;?>
</table>
</div>
</div>
