<div class="grid_12">
        <?php if(!$parentGroup){ ?>
        <h2>Add a group</h2>
        <?php }else{ ?>
        <h2>Add a sub group for :: <?php if($parentGroup) echo $parentGroup->getName()  ?></h2>
        <?php } ?>
        <form class="validate" action="" method="post">
                <fieldset>
                        <p class="element">
                                <label>Group Name</label>
                                <input type="text" name="name" class="required" />
                        </p>
                        <p class="element">
                                <label>Group Description</label>
                                <textarea cols=40 rows=4 name="description" class="required"></textarea>
                        </p>
                        <p>
                                <label>&nbsp;</label>
                                <input type="submit" value="Add" />
                                <a href="<?php echo site_url('user/group')?>" class="button">Cancel</a>
                        </p>
                </fieldset>
        </form>	
</div>