<div class="grid_12">
	<h2>Edit a Group [ <?php echo $group->getName();?> ]</h2>
        <fieldset>        			<form class="validate" action="" method="post">
            <p class="element">
                   <label>Group Name</label>
                   <input type="text" name="name" class="required" value="<?php echo $group->getName();?>" />
           </p>
           <p class="element">
                   <label>Group Description</label>
                   <textarea cols=40 rows=4 name="description" class="required"><?php echo $group->getDescription();?></textarea>
           </p>
           <p class="element">
                   <label>Check Balance at Login</label>
                   <input type="checkbox" name="check_balance" 
                   <?php echo $group->getCheckBalance()?'checked':'';?>>
           </p>
           <p>
                   <label>&nbsp;</label>
                   <input type="submit" value="Save" />
                   <a href="<?php echo site_url('user/group')?>" class="button">Cancel</a>
           </p>
   </form>	
</fieldset>
</div>