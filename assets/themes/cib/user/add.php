<?php 
use models\User;
use models\User\Group;
?>

<form class="validate" action="" method="post" name="addUser">
	<div class="grid_12">
		<h2>Create a User</h2>
		<div class="grid_6">
			<fieldset id="general">
				<legend>General</legend>
				<p class="element">
					<label> Account Name</label> <input type="text" name="acc_name" autocomplete="off"
					class="required" value="<?php echo set_value('acc_name');?>" />
				</p>
				<p class="element">
					<label> Country </label> 
					<select name="country" id="country" class="required">
						<?php foreach(getOperatingCountries() as $k=>$v){ ?>
						<option value="<?php echo $k ?>"><?php echo $v ?></opption>
							<?php } ?>
						</select>
					</p>
					<p class="element">
						<label> District </label> 
						<select name="district" id="district" class="required">
						</select>
					</p>


					<p class="element">
						<label> VDC/MCP/MP </label> 
						<input name="local_gov" id="local_gov" class="required" placeholder="Enter your vdc/mcp/mp or village or town" value="<?php echo set_value('local_gov');?>">
						<label class="info">Enter your vdc/mcp/mp or village or town</label>
					</p>
					<p class="element">
						<label> Address </label> 
						<textarea rows="5" cols="25" name="address" id="address" class="required" 
						placeholder="Enter your house number, tole name, ward number & vdc/mcp/mp"><?php echo set_value('address');?></textarea>
						<label class="info">Enter Your house number, tole name, ward number</label>
					</p>
					
					<p class="element">
						<label> Account Type </label> 
						<select name="acc_type" class="required">
							<?php foreach (getAccountTypes() as $at) {?>
							<?php if($at['type'] == "SUB_USER" or $at['type'] == "CLIENT" or $at['type'] == "NON_CLIENT"or $at['type'] == "GOVERNMENT_ENTITY") continue;; ?>
							<?php $selected = ($this->input->post("acc_type") == $at['id'])?"selected='selected'":""; ?>
							<option <?php echo $selected; ?> value="<?php echo $at['id']; ?>"> <?php echo $at['display_name']; ?> </option>
							<?php } ?>
						</select>
					</p>
					<p class="element">
						<label> Group </label> 
						<select name="group" class="required">
							<?php foreach($groups as $group){ if($group['name'] == 'superadmin' OR $group['name'] == 'Client' OR $group['name'] == 'Non Client')continue; ?>
							<?php $selected = ($this->input->post("group") == $group['group_id'])?"selected='selected'":""; ?>
							<option <?php echo $selected; ?>  value="<?php echo $group['group_id'] ?>"><?php echo $group['name'] ?></option>
							<?php } ?>
						</select>
					</p>
				</fieldset>
			</div>


			<div class="grid_6">
				<fieldset id="security_administrator">
					<legend>Security Administrator</legend>
					<div class="section">
						<p class="element">
							<label>Primary Name</label> <input type="text" name="first_name" autocomplete="off"
							class="required" value="<?php echo set_value('first_name');?>" />
						</p>
						<p class="element">
							<label>Second Name</label> <input type="text" name="middle_name" autocomplete="off"
							class="" value="<?php echo set_value('middle_name');?>" />
						</p>
						<p class="element">
							<label>Surname</label> <input type="text" name="last_name" autocomplete="off"
							class="required" value="<?php echo set_value('last_name');?>" />
						</p>
						<p class="element">
							<label>Telephone/Mobile</label> <input type="text" name="phone" autocomplete="off"
							class="required" value="<?php echo set_value('phone');?>" />
						</p>
						<p class="element">
							<label>Fax</label> <input type="text" name="fax" autocomplete="off"
							class="required" value="<?php echo set_value('fax');?>" />
						</p>
						<p class="element">
							<label>Email</label> <input type="email" name="email" autocomplete="off"
							class="required" value="<?php echo set_value('email');?>" />
						</p>
						<p class="element">
							<label>Login User ID</label> <input type="text" name="username" autocomplete="off"
							class="required" value="<?php echo set_value('username');?>" />
						</p>
						<p class="element">
							<label>Password</label> <input type="password" name="password" autocomplete="off"
							class="required" value="" />
						</p>
						<p class="element">
							<label>Confirm Password</label> <input type="password" name="confirmpassword" autocomplete="off"
							class="required" value="" />
						</p>
					</div>
				</fieldset>
			</div>

			<div class="clear"></div>
			<p class="element">
				<label>&nbsp;</label> <input type="submit" value="Create" />
				<a href="<?php echo Site_url('user')?>" class="button">Cancel</a>
			</p>

		</div>
	</form>

	<script type="text/javascript">
		$(function() {
			var country = $("#country").val();

			if(country) doActionCountryChanged(country);

			$("country").change(function(){

				var country = $(this).val();

				doActionCountryChanged(country);

			});

			function doActionCountryChanged(country){
				var country = country;
				var selected_city = "<?php echo ($this->input->post('district'))?:'' ?>";
				$.ajax({
					type : 'GET',
					url : Transborder.config.base_url+'country/ajax/getCitiesByCountry/'+country+"/"+selected_city,
					success : function(data){ $('#district').html(data); }
				});
			}
		});
	</script>
	<style type="text/css">
		span.error {
			text-align: left;
			color: red;
			font-size: 13px;
			width: 240px;
			display: block;
			margin-left: 34%;
		}
	</style>