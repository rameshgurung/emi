$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

jQuery.widget('f1soft.customeradd',{
	options: {
		title: "Add a customer",
		msgConfirmUse: "Are you sure?",
		width: 500,
		height: 550,
		preloadData:{},
		fields:[],
		onSelect: null,
		country: null,
		remote: '',
		receiver: false,
		agent: null,
	},
	
	_create: function(){
		this._createWrapper();
		var self = this;
		this.element.bind('click',function(e){
			e.preventDefault();
			self.dialog.dialog('open');
			
		});
	},
	
	_getTemplate: function(){
		var self = this,
			template = this.options.receiver ? 'add-customer-beneficiary':'add-customer';
		
		this.form.load(Transborder.config.base_url+'template/get/'+template+'/'+this.options.agent,function(){
			self._initDialog();
			
			if( self.options.receiver )
				self.form.find('.f1tb_c_add_state').remoteChained(self.form.find('.f1tb_c_add_country'), Transborder.config.base_url+'country/ajax/chainProcessor');
		
				self.form.find('.f1tb_c_add_city').remoteChained(self.form.find('.f1tb_c_add_state'), Transborder.config.base_url+'country/ajax/chainProcessor');
			
			self.form.find('.birthdatepicker').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
			    changeYear: true,
			    yearRange: '1940:-18y'
			});
			
			self.addButton = $("<button>")
				.text("Add")
				.addClass("search-button")
				.appendTo(self.form)
				.attr('id','save-customer-'+self.element.attr('id'))
				.button(); 
				
			self.addButton.click(function(e){
				e.preventDefault();
				var valid = self.form.validate({errorElement: 'span'}).form();
				if(valid)
					self._addCustomer();
			});
		});
	},
	
	_createWrapper: function(){
		var self = this;
		
		this.wrapper = $('<div>')
			.addClass('f1-add-customer-widget-wrapper')
			.attr({
				title: this.options.title,
				id: "dialog-"+this.element.attr('id')
			});
		
		this.remoteResponse = $('<p>')
			.addClass('error')
			.appendTo(this.wrapper).hide();
		
		this.form = $("<form>")
			.appendTo(this.wrapper);
		
		this.anyWayContinue = $('<input>').attr({ type : "hidden", name : "continue", value : "Y"})
		
		this._getTemplate();
	},
	
	_addCustomer: function(data){
		var self = this,
			_d = data ? data:this.form.serialize();
		
		this.wrapper.mask('Adding Customer..');
		
		jQuery.post(this.options.remote, _d,function(d){
			self._handleResponse(d);
		},'json');
	},
	
	_handleResponse: function(data){
		if(data.status == true){
			this.options.onAdd.call(self, data);
			this.addButton.text("Add");
			this.anyWayContinue.remove();
			this.remoteResponse.html('').hide();
			this.close();
		}else{
			this.remoteResponse.html(data.responseMessage).show();
			this.addButton.text("Continue Any Way");
			this.anyWayContinue.appendTo(this.form);
			this.wrapper.unmask();
		}
	},
	
	_initDialog: function(){
		var self = this;
		this.dialog = this.wrapper.dialog({
			autoOpen: false,
		    height: "auto",
		    resizable: false,
		    width: self.options.width,
		    modal: true,
		    create: function(){
		    	$(this).css("maxHeight", self.options.height);
		    },
		    close: function(e, ui){
		    	self._cleanup();		    	
		    }
		});
	},
	
	close: function(){
		this.remoteResponse.html('').hide();
		this.wrapper.unmask();
		this.dialog.dialog('close');
	},
	
	_cleanup: function(){
		this.form.find('input').not('input[readonly=readonly]').val('');
	}
});