	<p class="element">
		<label>First Name<em class="required">*</em>
		</label><input type="text" name="c_firstname"
			class="text ui-widget-content required">
	</p>
	
	<p class="element">
		<label>Middle Name
		</label><input type="text" name="c_middlename"
			class="text ui-widget-content">
	</p>
	
	<p class="element">
		<label>Last Name<em class="required">*</em>
		</label><input type="text" name="c_lastname"
			class="text ui-widget-content required">
	</p>
	
	<p class="element">
		<label>Phone
		</label><input type="text" name="c_phone"
			class="text ui-widget-content ">
	</p>
	<p class="element">
		<label>Mobile<em class="required">*</em></label>
		<input type="hidden"  style="width:50px;" readonly="readonly" name="c_country_code"  value="<?php echo '+'.$country_code; ?>"/>
		<input type="text" name="c_mobile"
			class="text ui-widget-content required">
	</p>
	<p class="element">
		<label>Email</label><input type="text" name="c_email"
			class="text ui-widget-content">
	</p>
	
		<p class="element">
		<label>Occupation</label><input type="text" name="c_occupation"
			class="text ui-widget-content">
	</p>
	<p class="element">
		<label>Address<em class="required">*</em>
		</label>
		<textarea name="c_address" class="text ui-widget-content required"
			cols="40" rows="3"></textarea>
	</p>
	<p class="element">
		<label for="c_dob">Date of Birth</label>
		<input type="text" name="c_dob" class="text ui-widget-content birthdatepicker" /> <span>(YYYY-MM-DD)</span>
	</p>
	<p class="element">
		<label>Country<em class="required">*</em>
		</label>
			<input type="text" name="f1tb_c_add_country"
			class="text ui-widget-content required"  readonly="readonly"  value="<?php echo $country;?>">
	</p>
	
	<p class="element">
		<label>State<em class="required">*</em>
		</label><select name="c_state" data-chain-ref="f1tb_c_add_state"
			class="text ui-widget-content required f1tb_c_add_state required">
			<option value="">--</option>
			<?php 
				if(count($states)):
					foreach($states as $k => $v):
			?>
				<option value="<?php echo $k;?>"><?php echo $v;?></option>
			<?php endforeach;
				endif;
			?>
		</select>
	</p>
	<p class="element">
		<label>City<em class="required">*</em>
		</label><select name="c_city" data-chain-ref="f1tb_c_add_city"
			class="text ui-widget-content required f1tb_c_add_city"><option value="">--</option>
		</select>
	</p>
