<?php 
/**
 * @var $customer models\Customer
 */

if (!user_access(array('edit customer'))){
	$readOnly = 'readonly="readonly"';
	$note = 'You can not edit Name and DOB. If you want to edit Name and DOB , call customer service.';
}else{
	$readOnly = '';
	$note = '';
}

?>		
		<p><?php echo $note;?></p>
		
		<p class="element">
		<label>Customer ID<em class="required">*</em>
		</label><input type="text" name="c_fname" readonly="readonly"
			class="text ui-widget-content" value="<?php echo $customer->getGuid(); ?>">
	</p>
	
	<p class="element">
		<label>First Name<em class="required">*</em>
		</label><input type="text" name="c_firstname" <?php echo $readOnly;?>
			class="text ui-widget-content required" value="<?php echo $customer->getfirstName(); ?>">
	</p>
	<p class="element">
		<label>Middle Name
		</label><input type="text" name="c_middlename" <?php echo $readOnly;?>
			class="text ui-widget-content " value="<?php echo $customer->getmiddleName(); ?>">
	</p>
	<p class="element">
		<label>Last Name<em class="required">*</em>
		</label><input type="text" name="c_lastname" <?php echo $readOnly;?>
			class="text ui-widget-content required" value="<?php echo $customer->getlastName(); ?>">
	</p>
	<p class="element">
		<label>Phone<em class="required">*</em>
		</label><input type="text" name="c_phone"
			class="text ui-widget-content required" value="<?php echo $customer->getPhone(); ?>">
	</p>
	<p class="element">
		<label>Mobile</label><input type="text" name="c_mobile"
			class="text ui-widget-content"value="<?php echo $customer->getMobile(); ?>">
	</p>
	<p class="element">
		<label>Email</label><input type="text" name="c_email"
			class="text ui-widget-content" value="<?php echo $customer->getEmail(); ?>">
	</p>
		<p class="element">
		<label>Occupation</label><input type="text" name="c_occupation"
			class="text ui-widget-content" value="<?php echo $customer->getOccupation(); ?>">
	</p>
		
	<p class="element">
		<label>Address<em class="required">*</em>
		</label>
		<textarea name="c_address" class="text ui-widget-content required"
			cols="40" rows="3"><?php echo $customer->getStreet(); ?></textarea>
	</p>
	<?php 
		
		$class = "text ui-widget-content tbdatepicker";
		$dob = '';
		
		if(	$customer->getDob() )
		{
			$class = "text ui-widget-content";
			$dob = $customer->getDob()->format('Y-m-d');
		}
	?>
	
	<p class="element">
		<label for="c_dob">Date of Birth</label>
		<input type="text" name="c_dob" <?php echo $readOnly;?> class="<?php echo $class ?>" value="<?php echo $dob ?>" /> <span>(YYYY-MM-DD)</span>
	</p>
	
	<p class="element">
		<label>Country<em class="required">*</em>
		</label><select name="c_country" data-chain-ref="f1tb_c_edit_country"
			class="text ui-widget-content required f1tb_c_edit_country" readonly="readonly">
			<option value="">--</option>
			<?php 
				$allCountries = getDestinationCountries($agent);
		 		foreach($allCountries as $ac): 
		 	?>
			<option value="<?php echo $ac->id() ?>" 
			<?php echo ($customer->getCity()->getState()->getCountry()->id() == $ac->id()) ? " selected='selected'":'';?>>
			<?php echo $ac->getName() ?></option>
			<?php endforeach; ?>
		</select>
	</p>
	<p class="element">
		<label>State<em class="required">*</em>
		</label><select name="c_state" data-chain-ref="f1tb_c_edit_state" readonly="readonly"
			class="text ui-widget-content f1tb_c_edit_state required">
			<option value="">--</option>
			<?php 
			$states = getStatesByCountry($customer->getCity()->getState()->getCountry()->id());
			foreach($states as $s):
			?>
			<option value="<?php echo $s->id() ?>" 
			<?php echo ($customer->getCity()->getState()->id() == $s->id()) ? " selected='selected'":'';?>>
			<?php echo $s->getName() ?></option>
			<?php endforeach;?>
		</select>
	</p>
	<p class="element">
		<label>City<em class="required">*</em>
		</label><select name="c_city" data-chain-ref="f1tb_c_edit_city" readonly="readonly"
			class="text ui-widget-content required f1tb_c_edit_city"><option value="">--</option>
			
			<?php 
			$cities = getCitiesByState($customer->getCity()->getState()->id());
			foreach($cities as $city):
			?>
			<option value="<?php echo $city->id() ?>" 
			<?php echo ($customer->getCity()->id() == $city->id()) ? " selected='selected'":'';?>>
			<?php echo $city->getName() ?></option>
			<?php endforeach;?>
		</select>
	</p>
	