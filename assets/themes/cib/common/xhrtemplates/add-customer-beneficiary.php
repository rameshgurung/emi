	<p class="element">
		<label>First Name<em class="required">*</em>
		</label><input type="text" name="c_firstname"
			class="text ui-widget-content required">
	</p>
	
		<p class="element">
		<label>Middle Name
		</label><input type="text" name="c_middlename"
			class="text ui-widget-content">
	</p>
	
	
	<p class="element">
		<label>Last Name<em class="required">*</em>
		</label><input type="text" name="c_lastname"
			class="text ui-widget-content required">
	</p>

	<p class="element">
		<label>Phone
		</label><input type="text" name="c_phone"
			class="text ui-widget-content ">
	</p>
	<p class="element">
		<label>Mobile</label><em class="required">*</em>
			<input type="hidden"  style="width:50px;"  name="c_country_code"  value="<?php echo '+'.$country_code; ?>"/>
		<input type="text" name="c_mobile"
			class="text ui-widget-content required">
	</p>
	<p class="element">
		<label>Email</label><input type="text" name="c_email"
			class="text ui-widget-content">
	</p>
			<p class="element">
		<label>Occupation</label><input type="text" name="c_occupation"
			class="text ui-widget-content">
	</p>
	<p class="element">
		<label>Address<em class="required">*</em>
		</label>
		<textarea name="c_address" class="text ui-widget-content required"
			cols="40" rows="3"></textarea>
	</p>
	<p class="element">
		<label for="c_dob">Date of Birth</label>
		<input type="text" name="c_dob"  class="text ui-widget-content birthdatepicker" /> <span>(YYYY-MM-DD)</span>
	</p>

	<p class="element">
		<label>Country<em class="required">*</em>
		</label><select name="c_country" data-chain-ref="f1tb_c_add_country"
			class="text ui-widget-content required f1tb_c_add_country"><option value="">--</option>
			<?php 
				$allCountries = getDestinationCountries($agent);
		 		foreach($allCountries as $ac): 
		 	?>
			<option value="<?php echo $ac->id() ?>"><?php echo $ac->getName() ?></option>
			<?php endforeach; ?>
		</select>
	</p>
	<p class="element">
		<label>State<em class="required">*</em>
		</label><select name="c_state" data-chain-ref="f1tb_c_add_state"
			class="text ui-widget-content required f1tb_c_add_state">
			<option value="">--</option>
		</select>
	</p>
	<p class="element">
		<label>City<em class="required">*</em>
		</label><select name="c_city" data-chain-ref="f1tb_c_add_city"
			class="text ui-widget-content required f1tb_c_add_city"><option value="">--</option>
		</select>
	</p>
