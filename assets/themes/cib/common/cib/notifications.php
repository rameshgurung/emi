<!-- alerts  -->
        <div class="alerts">
        <?php 
          if(isset($critical_alerts)){
            foreach($critical_alerts as $type => $msg){
              echo '<div class="response alert">';
              echo '<p>'.$msg.'</p>';
              echo '</div>';
            }
          }
          
          if(isset($feedback)){
            foreach($feedback as $type => $messages){
              echo '<div class="response '.$type.'">';
              
              foreach($messages as $msg)
                echo '<p>'.$msg.'</p>';
              
              echo '</div>';
            }
          }
          
        ?>
         <?php
          if($validation_errors = validation_errors('<p>','</p>'))
            echo '<div class="response error">'.$validation_errors.'</div>';
        ?>
      </div>