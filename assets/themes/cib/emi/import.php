<div class="grid_12">
	<h2>Import Cities </h2>
	
		<div class="section">
		<a href="<?php echo site_url('country/city/excelsample')?>" class="button" style="float:right;">View Sample</a>
        	<div>
					<form class="validate" action="" method="post" enctype="multipart/form-data">
						<p class="element">
							<label>Choose File</label>
							<input type="file" name="city_file" class=""/>
							<span>Only '.xls' file</span>
						</p>
						
						<p class="element">
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="Import" class="button" />
				       		<input type="reset" name="reset" value="Reset" class="button" />
						</p>
					</form>
			 </div>
        </div>
</div>