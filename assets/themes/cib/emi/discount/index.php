 <div class="grid_12">
  <h2> Discounts </h2>
  <table>
   <thead>
     <tr>
       <th>S. No.</th>
       <th>Name</th>
       <th>Payment Discount</th>
       <th>Corporate Discount</th>
       <!-- <th>Actions</th> -->
     </tr>
   </thead>
   <tbody>
     <?php if($discounts) { ?>
     <?php $c=0; foreach($discounts as $discount){ $c++;?>
     <tr>
      <td><?php echo $c;?></td>
      <td><?php echo $discount->getName();?></td>
      <td>
        <?php
        echo $discount->getPaymentDiscount();
        ?>
      </td>
      <td> <?php echo $discount->getCorporateDiscount();?></td>
     <!--  <td>
        <?php 
        echo anchor("emi/discount/edit/{$discount->id()}", 'Edit');
        echo " / "; 
        echo anchor("emi/discount/delete/{$discount->id()}", 'Delete');
        ?>
      </td> -->
    </tr>
    <?php } } else {?>
    <tr><td colspan="4">no data</td></tr>         
    <?php } ?>         
  </tbody>
</table>
</div>


<style>
  tbody tr td{
    background: white;
    color: black;
  }
  tr.last_month td{
    background-color: teal;
    color: white;
  }
</style>