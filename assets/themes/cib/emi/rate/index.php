 <div class="grid_12">
   <h2> Emi Rates </h2>
   <table>
     <thead>
       <tr>
         <th>S. No.</th>
         <th>Age</th>
         <th>Year</th>
         <th>Rate</th>
         <!-- <th>Actions</th> -->
       </tr>
     </thead>
     <tbody>
       <?php if($rates) { ?>
       <?php $c=0; foreach($rates as $rate){ $c++;?>
       <tr>
        <td><?php echo $c;?></td>
        <td><?php echo $rate->getAge();?></td>
        <td><?php echo $rate->getYear();?></td>
        <td><?php echo $rate->getRate();?></td>
        <!-- <td>
          <?php 
          echo anchor("emi/rate/edit/{$rate->id()}", 'Edit');
          echo " / "; 
          echo anchor("emi/rate/delete/{$rate->id()}", 'Delete');
          ?>
        </td> -->
      </tr>
      <?php } } else {?>
      <tr><td colspan="4">no data</td></tr>         
      <?php } ?>         
    </tbody>
  </table>
</div>


<style>
  tbody tr td{
    background: white;
    color: black;
  }
  tr.last_month td{
    background-color: teal;
    color: white;
  }
</style>