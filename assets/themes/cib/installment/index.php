 <div class="grid_12">
  <h2> Installments </h2>
  <table>
   <thead>
     <tr>
       <th>S.No.</th>
       <th>Created At</th>
       <th>T24 Username</th>
       <th>BranchCode</th>
       <th>Actions</th>
     </tr>
   </thead>
   <tbody>
     <?php if($installments) { ?>
     <?php $c=0; foreach($installments as $installment){ $c++;?>
     <tr>
      <td><?php echo $c;?></td>
      <td><?php echo $installment->getCreatedAt()->format('m-d-Y');?></td>
      <td><?php echo $installment->getT24UserName();?></td>
      <td><?php echo $installment->getBranchCode();?></td>
      <td><?php echo anchor("installment/view/".$installment->getId(), 'View Detail', '');?></td>
    </tr>
    <?php } } else {?>
    <tr><td colspan="5">no data</td></tr>         
    <?php } ?>         
  </tbody>
</table>
</div>


<style>
  tbody tr td{
    background: white;
    color: black;
  }
  tr.last_month td{
    background-color: teal;
    color: white;
  }
</style>