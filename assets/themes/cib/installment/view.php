 <div class="grid_12">
  <?php if(isset($installmentDetails) && $installmentDetails) { ?>
  <div class="grid_8">
   <h3>Loan Details: </h3><hr>
   <p class="element"><label>Principal</label> : <?php echo $installmentDetails->getAmount()?:'N/A';?>    </p>
   <p class="element"><label>Interest Rate</label> : <?php echo $installmentDetails->getInterest()?:'N/A';?> %    </p>
   <p class="element"><label>Term</label> : <?php echo $installmentDetails->getTenture()?:'N/A';?> years    </p>
   <p class="element"><label>Age</label> : <?php echo $installmentDetails->getAge()?:'N/A';?> years old    </p>
   <p class="element"><label>Premium Rate</label> : <?php echo $installmentDetails->getPremiumRate()?:'N/A';?>    </p>


   <p class="element"><label>Loan Disbursement Date</label> : <?php echo $installmentDetails->getLoanDispbursementDate()->format('Y-m-d')?:'N/A';?>    </p>
   <p class="element"><label>Emi Start Date</label> : <?php echo $installmentDetails->getEmiStarDate()->format('Y-m-d')?:'N/A';?>    </p>
   <p class="element"><label>Premium Payment Modet</label> : <b><?php echo $installmentDetails->getPremiumPaymentMode()->getRow()?:'N/A';?></b>    </p>
   <p class="element"><label>ADB (Accidental Death Benefit)</label> :  <?php echo $installmentDetails->getAccidentalDeathBenefit()?'Yes':'No';?></p>
   <p class="element"><label>TPDPW (Total Permanent Disability Benefit)</label> :  
     <?php echo $installmentDetails->getTotalPermanentDisabilityBenefit()?'Yes':'No';?>
   </p>
   <br>
 </div>


 <div class="grid_8">
   <h3>Insurance Premium: </h3><hr>
   <p class="element"><label>Large Sum Discount</label> : -<?php echo $installmentDetails->getLargeSumDiscount()?:'N/A';?>    </p>
   <p class="element"><label>After Discount</label> : <?php echo $installmentDetails->getAfterDiscount()?:'N/A';?>    </p>
   <p class="element"><label>Annual Premium</label> : <?php echo $installmentDetails->getAnnualPremium()?:'N/A';?></p>
   <p class="element"><label>Dis. on Annual payment</label> : <?php echo $installmentDetails->getDiscountOnAnnualPayment()?:'N/A';?></p>
   <p class="element"><label>Prem. After Discount</label> :  <?php echo $installmentDetails->getPermiumAfterDiscount()?:'N/A';?>     </p>
   <p class="element"><label>Less Corporate Discount</label> : <?php echo $installmentDetails->getLessCorporateDiscount()?:'N/A';?></p>
   <p class="element"><label>ADB (Accidental Death Benefit)</label> :  <?php echo $installmentDetails->getAdbAmount()?:'N/A';?></p>
   <p class="element"><label>TPDPW (Total Permanent Disability Benefit)</label> :  <?php echo $installmentDetails->getTpdpwAmount()?:'N/A';?></p>
   <p class="element"><label>Monthly Premium Amount (MPA)</label> : <?php echo $installmentDetails->getMonthlyPremiumAmount()?:'N/A';?></p>
   <br>
 </div>

 <div class="grid_8">
   <h3>Loan Generation Parameter: </h3><hr>
   <p class="element"><label>T24 Username</label> : <?php echo $installmentDetails->getT24Username()?:'N/A';?></p>
   <p class="element"><label>Branch Code</label> : <?php echo $installmentDetails->getBranchCode()?:'N/A';?></p>
   <p class="element"><label>Customer Identification Number</label> : <?php echo $installmentDetails->getCustomerIdentificationNumber()?:'N/A';?></p>
   <p class="element"><label>Loan Disbursment Account</label> : <?php echo $installmentDetails->getLoanDisbursementAccount()?:'N/A';?></p>
   <p class="element"><label>Principal Liquidation Account</label> : <?php echo $installmentDetails->getPrincipalLiquidationAccount()?:'N/A';?></p>           
   <p class="element"><label>Interest Liquidation Account</label> : <?php echo $installmentDetails->getInterestLiquidationAccount()?:'N/A';?></p>          
   <p class="element"><label>Cutomer Loan Account</label> : <b><?php echo $installmentDetails->getApiCustomerNo()?:'N/A';?></b></p>          
   <br>
 </div>

 <div class="grid_8">
   <h3>Installments: </h3><hr>
   <table>
     <thead>
       <tr>
         <th>S.No.</th>
         <th>Date</th>
         <th>Installment</th>
         <th>Premium</th>
         <th>Principal</th>
         <th>Interest</th>
       </tr>
     </thead>
     <tbody>
       <?php 
       $installments=unserialize($installmentDetails->getInstallments());
       if(isset($installments) && $installments) { $mpa=$installmentDetails->getMonthlyPremiumAmount();?>
       <?php foreach ($installments as $installment) { $m=$installment['installment_number'];?>
       <tr <?php echo $m%12==0?'class="last_month"':'';?>>
         <td><?php echo $m;?></td>
         <td><?php echo $installment['due_date'];?></td>
         <td>
           <?php 
           echo number_format($installment['installment'],'2'); 
           ?>
         </td>
         <td><?php echo isset($mpa)?number_format($mpa,2):'N/A'; ?></td>
         <td><?php echo number_format($installment['principal'],2)?></td>
         <td>
           <?php 
           if($m%12==0){
            $principal=$installment['installment']-$installment['principal'];
            echo number_format($principal,2);
          }else{
            echo number_format($installment['interest_recovered'],2);
          }
          ?>
        </td>
        <?php } } else {?>
        <tr><td colspan="5">no data</td></tr>         
        <?php } ?>         
      </tbody>
    </table>   
    <br>
  </div>

  <div class="grid_12">
    <h3>Api Details: </h3><hr>
    <p class="element"><label>Request Data</label> : 
      <textarea class="grid_12" name="" id="" cols="110" rows="10"><?php echo unserialize($installmentDetails->getApiRequestData())?:'N/A';?></textarea>
    </p>
    <p class="element"><label>Response Data</label> : 
      <textarea class="grid_12" name="" id="" cols="110"  rows="10"><?php echo $installmentDetails->getApiResponseData()?:'N/A';?></textarea>
    </p>
    <br>
  </div>

  <?php } ?>



</div>


<style>
  tbody tr td{
    background: white;
    color: black;
  }
  tr.last_month td{
    background-color: teal;
    color: white;
  }
</style>