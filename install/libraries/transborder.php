<?php

use models\Common\Currency;

use models\User;

use models\Ledger;

use models\Common\City;

use models\Common\State;

use models\Agent;

use models\Common\ReportGroup;

use models\Meta\AgentMetaEntry,
	models\Meta\TransactionMetaEntry,
	models\Meta\UserMetaEntry;

class Transborder
{
	var $ci;
	
	public function __construct(){
		$this->ci = & get_instance();
	}
	
	public function install($data){
		$schemas = array(	
				$this->ci->doctrine->em->getClassMetadata('models\Agent')
				,$this->ci->doctrine->em->getClassMetadata('models\Agent\AgentLog')
				,$this->ci->doctrine->em->getClassMetadata('models\User')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\IdentificationDocument')
				,$this->ci->doctrine->em->getClassMetadata('models\Customer')
				,$this->ci->doctrine->em->getClassMetadata('models\User\Group')
				,$this->ci->doctrine->em->getClassMetadata('models\User\Permission')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\Bank')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\BankBranch')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\Country')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\Currency')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\TimeZone')
				,$this->ci->doctrine->em->getClassMetadata('models\Transaction')
				,$this->ci->doctrine->em->getClassMetadata('models\Transaction\TransactionNote')
				,$this->ci->doctrine->em->getClassMetadata('models\Transaction\TransactionLog')
				,$this->ci->doctrine->em->getClassMetadata('models\Transaction\TransactionSearchLog')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\ExchangeRateBatch')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\ExchangeRate')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\State')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\ReportGroup')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\Report')
				,$this->ci->doctrine->em->getClassMetadata('models\Common\City')
				,$this->ci->doctrine->em->getClassMetadata('models\Ledger')
				,$this->ci->doctrine->em->getClassMetadata('models\LedgerPosting')
				,$this->ci->doctrine->em->getClassMetadata('models\CountryTDS')
				,$this->ci->doctrine->em->getClassMetadata('models\StateTDS')
				,$this->ci->doctrine->em->getClassMetadata('models\RemitCommission')
				,$this->ci->doctrine->em->getClassMetadata('models\RemitCommissionIndex')
				,$this->ci->doctrine->em->getClassMetadata('models\AML')
				,$this->ci->doctrine->em->getClassMetadata('models\AMLAddress')
				,$this->ci->doctrine->em->getClassMetadata('models\AMLAlias')
				,$this->ci->doctrine->em->getClassMetadata('models\AMLDocument')
				,$this->ci->doctrine->em->getClassMetadata('models\CustomerIDocuments')
				,$this->ci->doctrine->em->getClassMetadata('models\Meta\AgentMetaEntry')
				,$this->ci->doctrine->em->getClassMetadata('models\Meta\TransactionMetaEntry')
				,$this->ci->doctrine->em->getClassMetadata('models\Meta\UserMetaEntry')
		);
			 
		 
// 		$sql = $this->ci->doctrine->tool->getCreateSchemaSql($schemas,TRUE);
		$schema = $this->ci->doctrine->em->getMetadataFactory()->getAllMetadata();
		$sql = $this->ci->doctrine->tool->getCreateSchemaSql($schemas,TRUE);
		$sql[] = $this->getOptionsTableCreateSQL();
		$sql[] = $this->getSessionsTableCreateSQL();
		$sql[] = $this->getCountriesCreateSQL();
		$sql[] = $this->getStatesSQL();
		$sql[] = $this->getCitiesSQL();
		$sql[] = $this->getBanksSQL();
		$sql[] = $this->getBankBranchesSQL();
		$sql[] = $this->getPermissionsSQL();
		$sql[] = $this->getGroupSQL();
		$sql[] = $this->getGroupPermissionSQL();
		$sql[] = $this->getTimezoneSQL();
		$sql[] = $this->getReportGroupSQL();
		$sql[] = $this->getReportSQL();
		$sql[] = $this->getDocumentTypeSQL();
		
		$sql[] = $this->setOption('config_adminemail', $data['admin_contact']);
		$sql[] = $this->setOption('config_technicalemail', $data['tech_contact']);
		$sql[] = $this->setOption('config_aml_enabled', 0);
		 
		$count = count($sql);
		 
		foreach($sql as $statment)
		{
			$this->ci->doctrine->em->getConnection()->exec( $statment );
		}
		
		// Write the database file
		if ( ! $this->write_db_file($data['database']) )
		{
			return array(
					'status'	=> FALSE,
					'message'	=> '',
					'code'		=> 105
			);
		}
		
		// Write the config file.
		if ( ! $this->write_config_file() )
		{
			return array(
					'status'	=> FALSE,
					'message'	=> '',
					'code'		=> 106
			);
		}
		return array('status' => TRUE);
	}
	
	
	function getCountriesCreateSQL(){
		return file_get_contents('./assets/config/countries.sql');
	}
	
	function getStatesSQL()
	{
		return file_get_contents('./assets/config/states.sql');
	}
	
	function getCitiesSQL()
	{
		return file_get_contents('./assets/config/cities.sql');
	}
	
	function getBanksSQL()
	{
		return file_get_contents('./assets/config/banks.sql');
	}
	
	function getBankBranchesSQL()
	{
		return file_get_contents('./assets/config/bankbranches.sql');
	}
	
	function getPermissionsSQL(){
		return file_get_contents('./assets/config/permissions.sql');
	}
	
	function getGroupSQL(){
		return file_get_contents('./assets/config/groups.sql');
	}
	
	function getGroupPermissionSQL(){
		return file_get_contents('./assets/config/grouppermission.sql');
	}
	
	function getTimezoneSQL(){
		return file_get_contents('./assets/config/timezone.sql');
	}
	
	function getReportSQL(){
		return file_get_contents('./assets/config/report.sql');
	}
	
	function getDocumentTypeSQL(){
		return file_get_contents('./assets/config/documents.sql');
	}
	
	function getReportGroupSQL(){
		return file_get_contents('./assets/config/reportGroup.sql');
	}
	
	function write_db_file($database)
	{
		// First retrieve all the required data from the session and the $database variable
		$server 	= $this->ci->session->userdata('hostname');
		$username 	= $this->ci->session->userdata('username');
		$password 	= $this->ci->session->userdata('password');
		$port		= $this->ci->session->userdata('port');

		// Open the template file
		$template 	= file_get_contents('./assets/config/database.php');

		$replace = array(
			'__HOSTNAME__' 	=> $server,
			'__USERNAME__' 	=> $username,
			'__PASSWORD__' 	=> $password,
			'__DATABASE__' 	=> $database,
			'__PORT__' 		=> $port ? $port : 3306
		);

		// Replace the __ variables with the data specified by the user
		$new_file  	= str_replace(array_keys($replace), $replace, $template);

		// Open the database.php file, show an error message in case this returns false
		$handle 	= @fopen('../application/config/production/database.php.inc','w+');

		// Validate the handle results
		if ($handle !== FALSE)
		{
			$status = @fwrite($handle, $new_file);
			
			if($status !== FALSE){
				fclose($handle);
				return rename('../application/config/production/database.php.inc',
								'../application/config/production/database.php');
			}
			return FALSE;
		}

		return FALSE;
	}
	
	
	function write_config_file()
	{
		// Open the template
		$template = file_get_contents('./assets/config/config.php');
		
		$replace = array(
				'__BASEURL__' 	=> str_replace('install/', '', base_url()),
				'__PROXY_IPS__'	=>	isset($data['proxy_ip']) ? $data['proxy_ip']:''
		);
		
		// Replace the __ variables with the data specified by the user
		$new_file  	= str_replace(array_keys($replace), $replace, $template);

		// Open the database.php file, show an error message in case this returns false
		$handle = @fopen('../application/config/production/config.php.inc','w+');
		
		// Validate the handle results
		if ($handle !== FALSE)
		{
			$status = @fwrite($handle, $new_file);
				
			if($status !== FALSE){
				fclose($handle);
				return rename('../application/config/production/config.php.inc',
						'../application/config/production/config.php');
			}
			return FALSE;
// 			return fwrite($handle, $new_file);
		}
	
		return FALSE;
	}
	
	public function setOption($key,$value){
		return "INSERT INTO f1_options (`option_name`,`option_value`) VALUES ('{$key}','{$value}')";
	}
	
	
	public function setupAgent($data){
		$agent_country = $this->ci->doctrine->em->find('models\Common\Country',$data['agent_country']);
		$idrepo = $this->ci->doctrine->em->getRepository('models\Common\IdentificationDocument');
		$id_types = $idrepo->findAll();
		
		$remitDest = array();
		$remitDest[] = $agent_country->id();
		
		$cityRepo = $this->ci->doctrine->em->getRepository('models\Common\City');
		$city = $cityRepo->findOneBy(array('name' => $data['city']));
		
		if(!$city)
		{
			$state = new State();
			$state->setCountry($agent_country);
			$state->setOwn(TRUE);
			$state->setName($data['state']);
			$this->ci->doctrine->em->persist($state);
				
			$city = new City();
			$city->setName($data['city']);
			$city->setOwn(TRUE);
			$city->setState($state);
			$this->ci->doctrine->em->persist($city);
		}
		
		$currency = new Currency();
		$currency->setCountry($agent_country);
		$currency->setName($data['currency_name']);
		$currency->setIsoCode($data['currency_iso']);
		$currency->setSymbol($data['currency_symbol']);
		$this->ci->doctrine->em->persist($currency);
		
		$agent_country->setCurrency($currency);
		foreach($id_types as $id){
			$agent_country->addIdType($id);
		}
		
		$this->ci->doctrine->em->persist($agent_country);
		$timezone = $this->ci->doctrine->em->find('models\Common\TimeZone',$data['timezone']);
		
		$agent = new Agent();
		$agent->setAddress($data['address']);
		$agent->setBranchCode($data['branch_code']);
		$agent->setCity($city);
		$agent->setContactPerson($data['contact_person']);
		$agent->setEmail($data['contact_email']);
		$agent->setRemitDest($remitDest);
		$agent->setTimezone($timezone);
		
		
		if(isset($data['phone']))
			$agent->setPhone($data['phone']);
		
		if(isset($data['fax']))
			$agent->setFax($data['fax']);
		
		$agent->setDailyTxnLimit($data['daily_limit']);
		$agent->setMonthlyTxnLimit($data['monthly_limit']);
		$agent->setPerTxnLimit($data['txn_limit']);
		$agent->setName($data['company_name']);
		$agent->setRegNumber($data['company_reg']);
		$agent->activate();
		$agent->setDescription('');
		$agent->markAsNotTaxable(FALSE);
		
		$account = new Ledger();
		$account->setAccountNumber($data['account_no']);
		$account->setAccountHead($data['account_head']);
		$account->setBalance($data['balance']);
		$agent->setAccount($account);
		$this->ci->doctrine->em->persist($account);
		
		if($data['com_account_no'] !== '' && $data['com_account_no'] !== $data['account_no']){
			$comaccount = new Ledger();
			$comaccount->setAccountNumber($data['com_account_no']);
			$comaccount->setAccountHead($data['com_account_head']);
			$comaccount->setBalance($data['com_account_balance']);
		
			$agent->setComAccount($comaccount);
			$this->ci->doctrine->em->persist($comaccount);
		}else{
			$agent->setComAccount($account);
		}
		
		$sa = clone $agent;
		$sa->setParentAgent($agent);
		$this->ci->doctrine->em->persist($agent);
		$this->ci->doctrine->em->persist($sa);
		
		$this->ci->doctrine->em->flush();
		if($agent->id()){
			$poolAccount = $this->setOption('config_pool_account', $agent->getAccount()->id());
			$commissionPoolAccount = $this->setOption('config_commission_pool_account', $agent->getComAccount()->id());
			$pstatus = $this->ci->doctrine->em->getConnection()->exec($poolAccount);
			$cpstatus = $this->ci->doctrine->em->getConnection()->exec($commissionPoolAccount);
			return TRUE;
		}
		return FALSE;
		
	}
	
	
	
	public function setupSuperadmin($data){
		$user = new User();
		$firstname = $data["first_name"];
		$middlename = $data["middle_name"];
		$lastname = $data["last_name"];
		
		$user->setFirstname($firstname);
		$user->setMiddlename($middlename);
		$user->setLastname($lastname);
		$user->setUsercode($data["user_code"]);
		
		$user->setAddress($data["address"]);
		$user->setIdNumber("N/A");
		$user->setEmail($data["email"]);
		$user->setMobile($data["mobile"]);
		
		$groups = $this->ci->doctrine->em->find('\models\User\Group',1);
		$user->setGroup($groups);
		
		
		$agent = $this->ci->doctrine->em->find('\models\Agent',1);
		$user->setAgent($agent);
		
		$user->setUsername($data["username"]);
		$user->setPassword(md5($data["password"]));
		$user->setCity($agent->getCity());
		$user->activate();
		
		$uRepo = $this->ci->doctrine->em->getRepository('models\User');
		$api_key = $uRepo->generateApiKey();
		$user->setApiKey($api_key);
		
		$user->unmarkFirstLogin();
		$user->setPwdLastChangedOn();
		$user->setLastLogged();
		
		$this->ci->doctrine->em->persist($user);
		$this->ci->doctrine->em->flush();
		
		if($user->id()){
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function getOptionsTableCreateSQL(){
		return "CREATE TABLE `f1_options` (
		`id` INT(9) NOT NULL AUTO_INCREMENT,
		`option_name` VARCHAR(100) NOT NULL,
		`option_value` TEXT NOT NULL,
		`autoload` TINYINT(1) NOT NULL DEFAULT '1',
		PRIMARY KEY (`id`, `option_name`),
		UNIQUE INDEX `IDX_option_name` (`option_name`)
		)
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB
		ROW_FORMAT=DEFAULT";
	}
	
	
	public function getSessionsTableCreateSQL(){
		return "CREATE TABLE `f1_sessions` (
		`session_id` VARCHAR(40) NOT NULL DEFAULT '0',
		`ip_address` VARCHAR(16) NOT NULL DEFAULT '0',
		`user_agent` VARCHAR(255) NOT NULL,
		`last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0',
		`user_data` TEXT NOT NULL,
		PRIMARY KEY (`session_id`)
		)
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB
		ROW_FORMAT=DEFAULT";
	}
}