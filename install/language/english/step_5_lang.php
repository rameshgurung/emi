<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// labels
$lang['header']			=	'Step 4: Create Database';
$lang['intro_text']		=	'Complete the form below and hit the button labelled "Install" to install Transborder. Be sure to install Transborder into the right database since all existing changes will be lost!';

$lang['db_settings']	=	'Database Settings';
$lang['db_create']		=	'Create Database';
$lang['db_notice']		=	'You might need to do this yourself';
$lang['default_user']	=	'Default User';
$lang['database']		=	'Database';
$lang['site_settings']	= 	'Site Settings';
$lang['site_ref']		=	'Site Ref';
$lang['admin_contact']	= 	'Admin Contact Email Address';
$lang['tech_contact']	= 	'Technical Contact Email Address';
$lang['last_name']		=	'Last name';
$lang['email']			=	'Email';
$lang['password']		= 	'Password';
$lang['conf_password']	=	'Confirm Password';
$lang['finish']			=	'Install';