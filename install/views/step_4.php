<section class="title">
	<h3><?php echo lang('header'); ?></h3>
</section>

<section class="item">
	<p><?php echo lang('intro_text'); ?></p>
</section>

<?php echo form_open(uri_string(), 'id="install_frm"'); ?>
	
	<section class="title">
		<h3><?php echo lang('db_settings'); ?></h3>
	</section>
	
	<section class="item">

	<div class="input">
		<label for="database"><?php echo lang('database'); ?><em class="required">*</em></label>
		<input type="text" id="database" class="input_text" name="database" value="<?php echo set_value('database'); ?>" />
	</div>

	<br>

		
	</section>
	
	<section class="title">
		<h3>General Settings</h3>
	</section>
	<section class="item">
		<div class="input">
		<label for="proxy_ip">Proxy Ips</label>
		<?php
			echo form_input(array(
				'id' => 'proxy_ip',
				'name' => 'proxy_ip',
				'value' => set_value('proxy_ip')
			));
		?>
		<p>Enter your proxy server IP if you are running Transborder behind a proxy.</p>
	</div>
	</section>
	
	<section class="title">
		<h3>Contact Details</h3>
	</section>
	<section class="item">
		<div class="input">
		<label for="admin_contact"><?php echo lang('admin_contact'); ?><em class="required">*</em></label>
		<?php
			echo form_input(array(
				'id' => 'admin_contact',
				'name' => 'admin_contact',
				'value' => set_value('admin_contact')
			));
		?>
	</div>

	<div class="input">
		<label for="tech_contact"><?php echo lang('tech_contact'); ?><em class="required">*</em></label>
		<?php
			echo form_input(array(
				'id' => 'tech_contact',
				'name' => 'tech_contact',
				'value' => set_value('tech_contact')
			));
		?>
	</div>
	
	<input type="hidden" id="site_ref" name="site_ref" value="default" />
		<input class="btn orange" id="next_step" type="submit" id="submit" value="<?php echo lang('finish'); ?>" />
	</section>
	

	

<?php echo form_close(); ?>
