<section class="title">
	<h3><?php echo lang('header'); ?></h3>
</section>

<section class="item">
	<p><?php echo lang('intro_text'); ?></p>
</section>

<!-- Recommended -->
<section class="title">
	<h3><?php echo lang('mandatory'); ?></h3>
</section>

<section class="item">
<ul class="check">

	<!-- Server -->
	
	<!-- PHP -->
	<li>
		<h5><?php echo lang('php_settings'); ?></h5>
		<p><?php echo sprintf(lang('php_required'), $php_min_version); ?></p>

		<p class="result <?php echo ($php_acceptable) ? 'pass' : 'fail'; ?>">
			<?php echo lang('php_version'); ?> <strong><?php echo $php_version; ?></strong>.
			<?php if (!$php_acceptable): ?>
				<?php echo sprintf(lang('php_fail'), $php_min_version); ?>
			<?php endif; ?>
		</p>

	</li>

	<!-- MySQL -->
	<li>
		<h5><?php echo lang('mysql_settings'); ?></h5>
		<p><?php echo lang('mysql_required'); ?></p>

		<!-- Server -->
		<p class="result <?php echo ($mysql->server_version_acceptable) ? 'pass' : 'fail'; ?>">
			<?php echo lang('mysql_version1'); ?> <strong><?php echo $mysql->server_version; ?></strong>.
			<?php echo ($mysql->server_version_acceptable) ? '' : lang('mysql_fail'); ?>
		</p>

		<!-- Client -->
		<p class="result <?php echo ($mysql->client_version_acceptable) ? 'pass' : 'fail'; ?>">
			<?php echo lang('mysql_version2'); ?> <strong><?php echo $mysql->client_version; ?></strong>.
			<?php echo ($mysql->client_version_acceptable) ? '' : lang('mysql_fail') ; ?>
		</p>

	</li>

</ul>

</section>

<!-- Summary -->
<section class="title">
	<h3><?php echo lang('summary'); ?></h3>
</section>

<section class="item">
<?php if($step_passed === TRUE): ?>

	<p class="success">
		<?php echo lang('summary_success'); ?>
	</p>

	<a class="btn orange" id="next_step" href="<?php echo site_url('installer/step_3'); ?>" title="<?php echo lang('next_step'); ?>"><?php echo lang('step3'); ?></a>

<?php elseif($step_passed == 'partial'): ?>

	<p class="partial">
		<?php echo lang('summary_partial'); ?>
	</p>

	<a class="btn orange" id="next_step" href="<?php echo site_url('installer/step_3'); ?>" title="<?php echo lang('next_step'); ?>"><?php echo lang('step3'); ?></a>

<?php else: ?>

	<p class="failure">
		<?php echo lang('summary_failure'); ?>
	</p>

	<a class="btn orange" id="next_step" href="<?php echo site_url('installer/step_2'); ?>"><?php echo lang('retry'); ?></a>
<?php endif; ?>
</section>