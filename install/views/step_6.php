<section class="title">
	<h3>Super Admin Setup</h3>
</section>

<section class="item">
	<p>Now lets setup your company. Please fill in the details below so we configure Transborder for you.</p>
</section>

<?php echo form_open(uri_string(), 'id="superadmin_frm"'); ?>

<section class="title">
	<h3>Super Admin Detail</h3>
</section>


<section class="item">

	<div class="input">
		<label for="first_name">First Name<em class="required">*</em></label>
		<input type="text" id="first_name" class="input_text" name="first_name" value="<?php echo set_value('first_name'); ?>" />
	</div>
	
	<div class="input">
		<label for="middle_name">Middle Name</label>
		<input type="text" id="middle_name" class="input_text" name="middle_name" value="<?php echo set_value('middle_name'); ?>" />
	</div>
	
	<div class="input">
		<label for="last_name">Last Name<em class="required">*</em></label>
		<input type="text" id="last_name" class="input_text" name="last_name" value="<?php echo set_value('last_name'); ?>" />
	</div>
	
	<div class="input">
		<label for="user_code">User Code<em class="required">*</em></label>
		<input type="text" id="user_code" class="input_text" name="user_code" value="<?php echo set_value('user_code'); ?>" />
	</div>
	
	<div class="input">
		<label for="email">Email<em class="required">*</em></label>
		<input type="text" id="email" class="input_text" name="email" value="<?php echo set_value('email'); ?>" />
	</div>
	
	<div class="input">
		<label for="mobile">Mobile<em class="required">*</em></label>
		<input type="text" id="mobile" class="input_text" name="mobile" value="<?php echo set_value('mobile'); ?>" />
	</div>
	
	<div class="input address_textarea">
		<label for="address">Address<em class="required">*</em></label>
		<textarea id="address" class="input_text" name="address"><?php echo set_value('address'); ?></textarea>
	</div>
	
	

</section>


<section class="title">
	<h3>Super Admin Log in Detail</h3>
</section>

<section class="item">
	<div class="input">
		<label for="username">Username<em class="required">*</em></label>
		<input type="text" id="username" class="input_text" name="username" value="<?php echo set_value('username'); ?>" />
	</div>

	<div class="input">
		<label for="password">Password<em class="required">*</em></label>
		<input type="password" id="password" class="input_text" name="password" value="" />
	</div>
	
	<div class="input">
		<label for="password_confirm">Confirm Password<em class="required">*</em></label>
		<input type="password" id="password_confirm" class="input_text" name="password_confirm" value="" />
	</div>
	
	<input type="hidden" id="site_ref" name="site_ref" value="default" />
		<input class="btn orange" id="next_step" type="submit" id="submit" value="Proceed" />
		<br/>
</section>

<?php echo form_close(); ?>