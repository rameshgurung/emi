<section class="title">
	<h3>Company Setup</h3>
</section>

<section class="item">
	<p>Now lets setup your company. Please fill in the details below so we configure Transborder for you.</p>
</section>

<?php echo form_open(uri_string(), 'id="company_frm"'); ?>
	
	<section class="title">
		<h3>Company Detail</h3>
	</section>
	
	<section class="item">

	<div class="input">
		<label for="company_name">Company Name<em class="required">*</em></label>
		<input type="text" id="company_name" class="input_text" name="company_name" value="<?php echo set_value('company_name'); ?>" />
	</div>
	
	<div class="input">
		<label for="company_reg">Company Registration Number<em class="required">*</em></label>
		<input type="text" id="company_reg" class="input_text" name="company_reg" value="<?php echo set_value('company_reg'); ?>" />
	</div>

	
	<div class="input">
		<label for="operating_countries">Operating Countries<em class="required">*</em></label>
		{countries_multiselect}
	</div>
	
	<br>
		
	</section>
	
	<section class="title">
		<h3>The main agent</h3>
	</section>
	<section class="item">
		
		<div class="input">
			<label for="branch_code">Branch Code<em class="required">*</em></label>
			<input type="text" id="branch_code" class="input_text" name="branch_code" value="<?php echo set_value('branch_code'); ?>" />
		</div>
		
		
		<div class="input">
			<label for="phone">Phone Number<em class="required">*</em></label>
			<input type="text" id="phone" class="input_text" name="phone" value="<?php echo set_value('phone'); ?>" />
		</div>
		
		<div class="input">
			<label for="fax">Fax</label>
			<input type="text" id="fax" class="input_text" name="fax" value="<?php echo set_value('fax'); ?>" />
		</div>
		
		<div class="input">
			<label for="contact_person">Contact Person<em class="required">*</em></label>
			<input type="text" id="contact_person" class="input_text" name="contact_person" value="<?php echo set_value('contact_person'); ?>" />
		</div>
		
		<div class="input">
			<label for="contact_email">Contact Email<em class="required">*</em></label>
			<input type="text" id="contact_email" class="input_text" name="contact_email" value="<?php echo set_value('contact_email'); ?>" />
		</div>
		
		<div class="input">
			<label for="contact_mobile">Contact Mobile<em class="required">*</em></label>
			<input type="text" id="contact_mobile" class="input_text" name="contact_mobile" value="<?php echo set_value('contact_mobile'); ?>" />
		</div>
	
	
		<div class="input">
			<label for="address">Address<em class="required">*</em></label>
			<input type="text" id="address" class="input_text" name="address" value="<?php echo set_value('address'); ?>" />
		</div>
		<div class="input">
			<label for="city">City<em class="required">*</em></label>
			<input type="text" id="city" class="input_text" name="city" value="<?php echo set_value('city'); ?>" />
		</div>
		
		<div class="input">
			<label for="state">State<em class="required">*</em></label>
			<input type="text" id="state" class="input_text" name="state" value="<?php echo set_value('state'); ?>" />
		</div>
	
		
		<div class="input">
			<label for="agent_country">Country<em class="required">*</em></label>
			{agent_country}
		</div>
		
		<div class="input">
			<label for="timezone">Time Zone<em class="required">*</em></label>
			{timezone}
		</div>
	
	<br/>
	</section>
	
	<section class="title">
		<h3>Currency</h3>
	</section>
	<section class="item">
	
		<div class="input">
			<label for="currency_name">Currency Name<em class="required">*</em></label>
			<input type="text" id="currency_name" class="input_text" name="currency_name" value="<?php echo set_value('currency_name')?>" />
		</div>
		
		<div class="input">
			<label for="currency_iso">Currency ISO Code<em class="required">*</em></label>
			<input type="text" id="currency_iso" class="input_text" name="currency_iso" value="<?php echo set_value('currency_iso')?>" />
		</div>
		
		<div class="input">
			<label for="currency_symbol">Currency Symbol</label>
			<input type="text" id="currency_symbol" class="input_text" name="currency_symbol" value="<?php echo set_value('currency_symbol')?>" />
		</div>
	
	</section>
	
	<section class="title">
		<h3>Accounting &amp; Transaction Limits</h3>
	</section>
	<section class="item">
		<div class="input">
			<label for="account_no">Payable/Receivable Account Number<em class="required">*</em></label>
			<input type="text" id="account_no" class="input_text" name="account_no" value="<?php echo set_value('account_no'); ?>" />
		</div>
		
		<div class="input">
			<label for="account_head">Payable/Receivable Account Name<em class="required">*</em></label>
			<input type="text" id="account_head" class="input_text" name="account_head" value="<?php echo set_value('account_head'); ?>" />
		</div>
		
		<div class="input">
			<label for="balance">Payable/Receivable Account Balance</label>
			<input type="text" id="balance" class="input_text" name="balance" value="<?php echo set_value('balance'); ?>" />
		</div>
		
		<div class="input">
			<label for="com_account_no">Commission Account Number</label>
			<input type="text" id="com_account_no" class="input_text" name="com_account_no" value="<?php echo set_value('com_account_no'); ?>" />
			<small>Leave blank if same as payble/receivable account and commission account are same</small>
		</div>
		<div class="input">
			<label for="com_account_head">Commission Account Name</label>
			<input type="text" id="com_account_head" class="input_text" name="com_account_head" value="<?php echo set_value('com_account_head'); ?>" />
			<small>Leave blank if same as payble/receivable account and commission account are same</small>
		</div>
		
		<div class="input">
			<label for="com_account_balance">Commission Account Balance</label>
			<input type="text" id="com_account_balance" class="input_text" name="com_account_balance" value="<?php echo set_value('com_account_balance'); ?>" />
			<small>Leave blank if same as payble/receivable account and commission account are same</small>
		</div>
	
		<div class="input">
			<label for="txn_limit">Per Transaction Limit<em class="required">*</em></label>
			<input type="text" id="txn_limit" class="input_text" name="txn_limit" value="<?php echo set_value('txn_limit'); ?>" />
		</div>
		
		<div class="input">
			<label for="daily_limit">Daily Limit<em class="required">*</em></label>
			<input type="text" id="daily_limit" class="input_text" name="daily_limit" value="<?php echo set_value('daily_limit'); ?>" />
		</div>
		
		<div class="input">
			<label for="monthly_limit">Monthly Limit<em class="required">*</em></label>
			<input type="text" id="monthly_limit" class="input_text" name="monthly_limit" value="<?php echo set_value('monthly_limit'); ?>" />
		</div>
		
		<input type="hidden" id="site_ref" name="site_ref" value="default" />
		<input class="btn orange" id="next_step" type="submit" id="submit" value="Update Company" />
		<br/>
	</section>
	
<?php echo form_close(); ?>