# loancalculator
1. clone repo
	1. cd emi
	2. ~~composer update~~ (command)
	3. copy vendor 
	4. create db/migrations
2. create databse f1_sunrise
3. go to directory
4. give full permission to 
	1. bin/console folder
	2. assets/theme folder
5. create tables
	* php bin/console db:init ( to create session and option tables).
	* ###or###
	* run tables.sql file ( which will create 4 tables f1_installments, f1_rate, f1_dicounts).
6. run data.sql file ( to insert values to tables).
7. set .htaccess file if needed
